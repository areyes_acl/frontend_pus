/**
 * 
 */
package resources;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.acl.model.filter.FiltroUsuario;
import com.acl.service.RolService;
import com.acl.service.UsuarioService;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class UsuarioServiceTest {

    private ApplicationContext context;
    private UsuarioService usuarioService;
    private RolService rolService;

    @Before
    public void setUp() throws Exception {
	System.out.println("Configurando....");
	context = new ClassPathXmlApplicationContext("Spring-Context-Test.xml");

	for (String nombre : context.getBeanDefinitionNames()) {
	    System.out.println(nombre);
	}

	usuarioService = (UsuarioService) context.getBean("usuarioService");
	rolService = (RolService) context.getBean("rolService");
	System.out.println("Configuracion Finalizada!.");
    }

    @Test
    @Ignore
    public void testGestUsuarios() {
	FiltroUsuario filter = new FiltroUsuario();
	filter.setRut(17815618);
	filter.setDv("7");
//	filter.setEstado(true);
	System.out.println(usuarioService.buscarUsuariosPorFiltro(filter));

    }

    @Test
    // @Ignore
    public void testSaveUsuario() {
	//
	// // USUARIO
	// Usuario usuario = new Usuario();
	// usuario.setRut(8084020);
	// usuario.setDv("9");
	// usuario.setNombres("Usuario");
	// usuario.setApellidoPaterno("paterno");
	// usuario.setApellidoMaterno("materno");
	// usuario.setEmail("test@test.com");
	// usuario.setFono("");
	// usuario.setEstado(Boolean.TRUE.booleanValue());
	// usuario.setEsSuperUsuario(Boolean.TRUE.booleanValue());
	//
	// // CONTRASENA
	// Contrasena contrasena = new Contrasena();
	// contrasena.setPassword("aA12r$s");
	// contrasena.getEstado().setSid(Long.valueOf(3));
	// contrasena.setFechaVencimiento(Calendar.getInstance().getTime());
	// usuario.setContrasena(contrasena);

	// ROLES
//	List<Rol> listaRoles = rolService.obtenerTodos();
	//
	// usuarioService.guardar(usuario, listaRoles);
	// System.out.println("FIN PRUEBA");
    }

    @Test
    @Ignore
    public void testCallSP() {
    }

}
