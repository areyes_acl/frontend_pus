/**
 * 
 */
package resources;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.acl.model.Pregunta;
import com.acl.service.PreguntaService;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class PreguntasSeguridadServiceTest {

    private ApplicationContext context;
    private PreguntaService preguntaService;

    @Before
    public void setUp() throws Exception {
	System.out.println("Configurando....");
	context = new ClassPathXmlApplicationContext("Spring-Context.xml");

	for (String nombre : context.getBeanDefinitionNames()) {
	    System.out.println(nombre);
	}

	preguntaService = (PreguntaService) context.getBean("preguntaService");
	System.out.println("Configuracion Finalizada!.");
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    @Test
    @Ignore
    public void testObtenerTodasLasPreguntas() {
	System.out.println("Inicio de Metodo");
	List<Pregunta> listaPreguntas = preguntaService.buscarPreguntas(null);
	System.out.println(listaPreguntas);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    @Test
    @Ignore
    public void testCrearPreguntas() {
	System.out.println("Inicio de Metodo");
	Pregunta pregunta = new Pregunta();
	pregunta.setPregunta("Se ha ingresado una pregunta test ? ");
	pregunta.getLog().setSidCreacion(Long.valueOf(1));
//	pregunta.setEstado(Boolean.TRUE);

	System.out.println("Fin de metodo");
    }

    /**
     *
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     *
     * @since 1.X
     */
    @Test
    @Ignore
    public void testModificarPreguntas() {

	Pregunta pregunta = preguntaService.buscarPreguntas(null).get(0);
	pregunta.setPregunta("nueva pregunta");
//	pregunta.setEstado(Boolean.FALSE);

	System.out.println("Fin de metodo");
    }

}
