//Alfanumérico, espacios y guión
var alphaNumberSet1 = /^[a-zA-Z0-9 -]*$/;

var alphaAndAccentMark = /^[a-zA-ZÑñáéíóúÁÉÍÓÚ ]*$/;

// Alfanumérico
var alphaNumberSet2 = /^[0-9a-zA-Z]*$/;

// Solo números
var onlyNumberSet = /^[0-9]*$/;

//Solo números y K
var alphaAndKSetCharacter = /^[0-9Kk]*$/;

// Sólo letras sin espacio
var alphaSetWithoutSpace = /^[a-zA-Z]*$/;

// Sólo letras con espacio
var alphaSetWithSpace = /^[a-zA-Z ]*$/;

var alphaNumberSetCharacter1 = /^[0-9a-zA-Z,.]*$/;

var alphaNumberSetCharacter2 = /^[0-9a-zA-Z]*$/;

var alphaNumberSetCharacter3 = /^[A-Z 0-9 ,.]*$/;

var alphaNumberSetCharacter4 = /^[a-zA-Z ]*$/;

var varNumberSetCharacter5 = /^[0-9]*$/;

var alphaNumberSetCharacter6 = /^[0-9a-zA-Z,. ]*$/;

var varNumberDecimalSetCharacter6 = /^[0-9\.]*$/;

var alphaNumberSetCharacter7 = /^[0-9a-zA-Z ]*$/;

var alphaSetCharacter8 = /^[a-zA-Z]*$/;

var onlyNumberAndPoint = /^[0-9:]*$/;

var onlyNumberSN = /^([0-9]*||([sS]{1,1}[nN]{0,1}))$/;

var numberSetDecimalRestrict1 = /^\d{0,30}(\.\d{0,2})?$/;

// 8 enteros y dos decimales
var numberSetDecimalRestrict2 = /^\d{0,8}(\.\d{0,2})?$/;

// 6 enteros y dos decimales
var numberSetDecimalRestrict3 = /^\d{0,6}(\.\d{0,2})?$/;

// 13 enteros y dos decimales
var numberSetDecimalRestrict4 = /^\d{0,13}(\.\d{0,2})?$/;

/**
 * 
 * @param restriction
 * @param obj
 * @param noUpperFlag
 * @returns {Boolean}
 */
function restrictInputs(restriction, obj, noUpperFlag) {

	var eventObj = window.event;
	var code = 0;
	if (eventObj.keyCode) {
		code = eventObj.keyCode;
	} else if (eventObj.which) {
		code = eventObj.which;
	}

	// Verifica si el codigo Key ingresado es ESC
	if (code == 27) {
		this.blur();
		return false;
	}

	// ASR: Se incluye validación de maxlength
	if (typeof obj != 'undefined' && (obj.value.length + 1) > obj.maxLength) {
		return false;
	}
	
	if (!eventObj.ctrlKey && code != 9 && code != 8 && code != 36 && code != 37
			&& code != 38 && (code != 39)
			&& code != 40) {

		var pst = undefined;
		var pnd = undefined;
		var string_start = undefined;
		var string_end = undefined;

		if ($.browser.chrome) { // chrome
			pst = eventObj.currentTarget.selectionStart;
			pnd = eventObj.currentTarget.selectionEnd;
			var selectionlength = pnd - pst;

			if (selectionlength > 0) {
				string_start = eventObj.currentTarget.value.substring(0, pst);
				string_end = eventObj.currentTarget.value.substring(pnd,
						eventObj.currentTarget.value.length);
			} else {
				string_start = eventObj.currentTarget.value.substring(0, pst);
				string_end = eventObj.currentTarget.value.substring(pst,
						eventObj.currentTarget.value.length);
			}
		} else { // IE8
			pst = getCaret(obj);
			string_start = obj.value.substring(0, pst);
			string_end = obj.value.substring(pst, obj.value.length);
		}

		var valueAll = string_start + String.fromCharCode(code) + string_end;

		if (restriction.test(valueAll)) {
			if ($.browser.chrome) { // chrome

				if (typeof noUpperFlag !== undefined && noUpperFlag) {
					eventObj.currentTarget.value = eventObj.currentTarget.value;
				} else {
					eventObj.currentTarget.value = eventObj.currentTarget.value
							.toUpperCase();
				}

				eventObj.currentTarget.selectionStart = pst + 1;
				eventObj.currentTarget.selectionEnd = pst + 1;
				// eventObj.currentTarget.blur();
				eventObj.currentTarget.focus();
				eventObj.stopPropagation();

				return true;
			} else { // IE8
				if (typeof noUpperFlag !== undefined && noUpperFlag) {
					window.event.keyCode = window.event.keyCode;
				} else {
					if ((window.event.keyCode > 0x60)
							&& (window.event.keyCode < 0x7B)) {
						window.event.keyCode = window.event.keyCode - 0x20;
					}
				}
				return true;
			}
		} else {
			return false;
		}

	} else {
		return false;
	}

}

/**
 * 
 * @param el
 * @returns
 */
function getCaret(el) {
	if (el.selectionStart) {
		return el.selectionStart;
	} else if (document.selection) {
		el.focus();

		var r = document.selection.createRange();
		if (r == null) {
			return 0;
		}

		var re = el.createTextRange(), rc = re.duplicate();
		re.moveToBookmark(r.getBookmark());
		rc.setEndPoint('EndToStart', re);

		return rc.text.length;
	}
	return 0;
}

/**
 * 
 * @param element
 */
function restrictPhone(element) {

	var $th = $(element);
	$th.val($th.val().replace(/[^0-9+#]/g, function(str) {
		return '';
	}).toUpperCase());
};

/**
 * 
 * @param element
 */
function restrictEmail(element) {

	var $th = $(element);
	$th.val($th.val().replace(/^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$/g, function(str) {
		return '';
	}).toUpperCase());
};


/**
 * 
 * @param idElement
 */
function setFocusById(idElement) {
	var idVarliable = PrimeFaces.escapeClientId(idElement);
	if (!($(idVarliable).is(':disabled'))) {
		$(idVarliable).select();
		$(idVarliable).focus();
	}
}
