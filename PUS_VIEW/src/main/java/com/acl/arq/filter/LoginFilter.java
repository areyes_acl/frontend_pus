package com.acl.arq.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.acl.web.managedbeans.LoginSessionMB;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * Filter checks if LoginBean has loginIn property set to true. If it is not set
 * then request is being redirected to the login.xhml page.
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class LoginFilter implements Filter {

    private static final Logger logger = Logger
	    .getLogger(LoginFilter.class);
    private static String REDIRECT_TO_LOGIN = "/login.jsf";

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
     *      javax.servlet.ServletResponse, javax.servlet.FilterChain)
     * @since 1.X
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
	    FilterChain chain) throws IOException, ServletException {

	logger.debug("Chequea el login");

	// Obtiene el bean de sesion con el nombre definido en el propio bean
	LoginSessionMB loginBean = (LoginSessionMB) ((HttpServletRequest) request)
		.getSession().getAttribute("loginSession");

	logger.debug("loginBean.isLoggedIn()" + loginBean);

	if (loginBean == null || !loginBean.isLoggedIn()) {
	    String contextPath = ((HttpServletRequest) request)
		    .getContextPath();
	    ((HttpServletResponse) response).sendRedirect(contextPath
		    + REDIRECT_TO_LOGIN);

	}

	chain.doFilter(request, response);

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param config
     * @throws ServletException
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     * @since 1.X
     */
    @Override
    public void init(FilterConfig config) throws ServletException {
	// Nothing to do here!
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @see javax.servlet.Filter#destroy()
     * @since 1.X
     */
    @Override
    public void destroy() {
	// Nothing to do here!
    }

}