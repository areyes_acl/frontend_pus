/**
 * 
 */
package com.acl.arq.logger;

import org.apache.log4j.Logger;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * Clase para poder escribir log en la aplicacion
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class PoliticasFileLoggerImpl implements PoliticasLogger {
    private static Logger logger = Logger
	    .getLogger(PoliticasFileLoggerImpl.class);

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private PoliticasFileLoggerImpl() {
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo sobreescrito que permite escribi sysos en la aplicacion
     * 
     * @param clase
     * @param mensaje
     * @see com.acl.arq.logger.LoggerInt#addLog(java.lang.Class,
     *      java.lang.String)
     * @since 1.X
     */
    @Override
    public void addLog(Class<?> clase, String mensaje) {
	logger.info("<<<<< " + clase.getName() + " >>>>> : " + mensaje);
    }

}
