/**
 * 
 */
package com.acl.arq.logger;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * Clase para poder escribir log en la aplicacion
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class PoliticasConsoleLoggerImpl implements PoliticasLogger {
    static PoliticasLogger log;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private PoliticasConsoleLoggerImpl() {
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo sobreescrito que permite escribi sysos en la aplicacion
     * 
     * @param clase
     * @param mensaje
     * @see com.acl.arq.logger.LoggerInt#addLog(java.lang.Class,
     *      java.lang.String)
     * @since 1.X
     */
    @Override
    public void addLog(Class<?> clase, String mensaje) {
	System.out.println("<<<<< " + clase.getName() + " >>>>> : " + mensaje);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public static PoliticasLogger getinstance() {
	if (log == null) {
	    log = new PoliticasConsoleLoggerImpl();
	    return log;
	} else {
	    return log;
	}
    }

}
