package com.acl.arq.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.acl.arq.config.PropertiesConfiguration;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class AplicationInitListener implements ServletContextListener {
    /**
     * Constante de log
     */
    private static final Logger logger = Logger
	    .getLogger(AplicationInitListener.class);

    @Override
    public void contextInitialized(ServletContextEvent arg0) {

	ServletContext context = arg0.getServletContext();
	PropertiesConfiguration.loadApplicationProperties(context);
	logger.info("==============>SE INICIO APP POLITICAS SGIC abcdin CORRECTAMENTE!!!<==============");

    }

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
	logger.info("==============>SE DETUVO POLITICAS SGIC ABCDCIN CORRECTAMENTE!!!<==============");

    }
}
