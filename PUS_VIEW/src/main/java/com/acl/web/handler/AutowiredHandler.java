/**
 * 
 */
package com.acl.web.handler;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public abstract class AutowiredHandler {

	protected AutowireCapableBeanFactory ctx;

	@PostConstruct
	protected void init() {
		ctx = WebApplicationContextUtils.getWebApplicationContext(
				(ServletContext) FacesContext.getCurrentInstance()
						.getExternalContext().getContext())
				.getAutowireCapableBeanFactory();
		ctx.autowireBean(this);
	}

}
