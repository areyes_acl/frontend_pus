package com.acl.web.utils;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * Clase utilitaria para manejar la session de un usuario
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class SessionUtils {

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo utilizado para obtener una session
     * 
     * @return
     * @since 1.X
     */
    public static HttpSession getSession() {

	return (HttpSession) FacesContext.getCurrentInstance()
		.getExternalContext().getSession(false);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo utilizado para obtener un request
     * 
     * @return
     * @since 1.X
     */
    public static HttpServletRequest getRequest() {
	return (HttpServletRequest) FacesContext.getCurrentInstance()
		.getExternalContext().getRequest();
    }

}
