/**
 * 
 */
package com.acl.web.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.acl.arq.logger.PoliticasConsoleLoggerImpl;
import com.acl.arq.logger.PoliticasLogger;
import com.acl.model.Parametros;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@FacesValidator("custom.maxValueParamValidator")
public class MaximoValorParametroValidator implements Validator {
    PoliticasLogger logger = PoliticasConsoleLoggerImpl.getinstance();
    private static final String MAX_VALUE_EXCEPTION = "El campo :nombreCampo, permite un valor máximo igual a :valorMax .";
    private static final String MIN_VALUE_EXCEPTION = "El campo :nombreCampo, permite un valor mínimo  mayor o igual a :valorMin .";

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param context
     * @param component
     * @param value
     * @throws ValidatorException
     * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
     *      javax.faces.component.UIComponent, java.lang.Object)
     * @since 1.X
     */
    @Override
    public void validate(FacesContext context, UIComponent component,
	    Object value) throws ValidatorException {
	String valorActualizado = String.valueOf(value);
	if (valorActualizado != null && !valorActualizado.trim().isEmpty()) {
	    Parametros parametro = (Parametros) component.getAttributes().get(
		    "parametro");
	    parametro.setValor((String) value);

	    // parametro.setNewValor(String.valueOf(value));

	    validarMaximo(component, parametro);
	    validarMinimo(component,parametro);
	}

	// context.getCurrentInstance().getPartialViewContext().getRenderIds().add("tablaParametros");

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param largoMaximo
     * 
     * @param component
     * @param parametro
     * @since 1.X
     */
    private void validarMaximo(UIComponent component, Parametros parametro) {

	Integer valor = Integer.valueOf(parametro.getValor());

	if (valor > parametro.getParametro().getValorMaximo()) {
	    String mensajeError = MAX_VALUE_EXCEPTION.replace(
		    ":nombreCampo",
		    component.getAttributes().get("nameComponentView")
			    .toString()).replace(":valorMax",
		    parametro.getParametro().getValorMaximo().toString());
	    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
		    "Error:", mensajeError);
	    parametro.setTestValidatorFail(true);
	    throw new ValidatorException(msg);
	}

	parametro.setTestValidatorFail(false);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param component
     * @param parametro
     * @since 1.X
     */
    private void validarMinimo(UIComponent component, Parametros parametro) {

	Integer valor = Integer.valueOf(parametro.getValor());

	if (valor < parametro.getParametro().getValorMinimo()) {
	    
	    String mensajeError = MIN_VALUE_EXCEPTION.replace(
		    ":nombreCampo",
		    component.getAttributes().get("nameComponentView")
			    .toString()).replace(":valorMin",
		    parametro.getParametro().getValorMinimo().toString());
	    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
		    "Error:", mensajeError);
	    parametro.setTestValidatorFail(true);
	    throw new ValidatorException(msg);
	}

	parametro.setTestValidatorFail(false);
    }
}
