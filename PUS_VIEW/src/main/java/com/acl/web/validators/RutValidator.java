/**
 * 
 */
package com.acl.web.validators;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.validate.ClientValidator;

import com.acl.model.utils.ValidationUtils;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@FacesValidator("custom.rutValidator")
public class RutValidator implements Validator, ClientValidator {

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public RutValidator() {

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see org.primefaces.validate.ClientValidator#getMetadata()
     * @since 1.X
     */
    @Override
    public Map<String, Object> getMetadata() {
	return null;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see org.primefaces.validate.ClientValidator#getValidatorId()
     * @since 1.X
     */
    @Override
    public String getValidatorId() {
	return "custom.rutValidator";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param context
     * @param component
     * @param value
     * @throws ValidatorException
     * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
     *      javax.faces.component.UIComponent, java.lang.Object)
     * @since 1.X
     */
    @Override
    public void validate(FacesContext context, UIComponent component,
	    Object value) throws ValidatorException {

	// Obtiene el componente rutId pasado como parametros
	UIInput startComponent = (UIInput) component.getAttributes().get(
		"rutId");

	if (value == null || startComponent == null) {
	    return;
	}

	Object rutNumeric = startComponent.getValue();

	if (!ValidationUtils
		.validarRut(rutNumeric.toString(), value.toString())) {
	    startComponent.setValid(Boolean.FALSE);
	    throw new ValidatorException(new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Error de validación rut",
		    "El rut ingresado : " + rutNumeric + "-" + value
			    + " no es un rut válido"));
	}

    }
}
