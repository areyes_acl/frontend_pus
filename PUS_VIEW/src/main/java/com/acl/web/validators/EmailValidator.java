/**
 * 
 */
package com.acl.web.validators;

import java.util.Map;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.validate.ClientValidator;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@FacesValidator("custom.emailValidator")
public class EmailValidator implements Validator, ClientValidator {

    private Pattern pattern;
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public EmailValidator() {
	pattern = Pattern.compile(EMAIL_PATTERN);
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see org.primefaces.validate.ClientValidator#getMetadata()
     * @since 1.X
     */
    @Override
    public Map<String, Object> getMetadata() {
	return null;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see org.primefaces.validate.ClientValidator#getValidatorId()
     * @since 1.X
     */
    @Override
    public String getValidatorId() {
	return "custom.emailValidator";
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>	
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param context
     * @param component
     * @param value
     * @throws ValidatorException
     * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
     *      javax.faces.component.UIComponent, java.lang.Object)
     * @since 1.X
     */
    @Override
    public void validate(FacesContext context, UIComponent component,
	    Object value) throws ValidatorException {
			
	if (value == null) {
	    return;
	}

	if (!pattern.matcher(value.toString()).matches()) {
	    throw new ValidatorException(new FacesMessage(
		    FacesMessage.SEVERITY_ERROR, "Error de validación email",
		    value + " ingresado no es un email válido"));
	}

    }
}
