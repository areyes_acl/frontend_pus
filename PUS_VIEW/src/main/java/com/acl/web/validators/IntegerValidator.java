/**
 * 
 */
package com.acl.web.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.acl.model.Parametros;
import com.acl.model.types.ParametrosType;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@FacesValidator("custom.integerValidator")
public class IntegerValidator implements Validator {

    private static final String PATTERN_INTEGER = "\\d+";
    private static final String EXCEPTION_INVALID_CHARACTER = "El campo :nombreCampo, sólo permite valores numéricos.";

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param context
     * @param component
     * @param value
     * @throws ValidatorException
     * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext,
     *      javax.faces.component.UIComponent, java.lang.Object)
     * @since 1.X
     */
    @Override
    public void validate(FacesContext context, UIComponent component,
	    Object value) throws ValidatorException {

//	Parametros parametro = (Parametros) component.getAttributes().get(
//		"parametro");

	validacionDeTipoDeDatoEntero(component, value);
//	validacionDeCamposLargosMaximos(component, parametro);
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param parametro
     * 
     * @since 1.X
     */
    private void validacionDeCamposLargosMaximos(UIComponent component,
	    Parametros parametro) {

	switch (parametro.getParametro()) {
	case LARGO_MAXIMO:
	    validarMaximo(ParametrosType.LARGO_MAXIMO, component, parametro);

	    break;
	case LARGO_MINIMO:
	    validarMaximo(ParametrosType.LARGO_MINIMO, component, parametro);
	    break;
	case VIGENCIA_CONTRASENA:
	    validarMaximo(ParametrosType.VIGENCIA_CONTRASENA, component, parametro);
	    break;
	case NRO_RECHAZO_RE_USO:
	    validarMaximo(ParametrosType.NRO_RECHAZO_RE_USO, component, parametro);
	    break;
	case NRO_CARACTERES_CONSECUTIVOS:
	    validarMaximo(ParametrosType.NRO_CARACTERES_CONSECUTIVOS, component, parametro);
	    break;
	case NRO_CARACT_IGUALES_CONSECUTIVOS:
	    validarMaximo(ParametrosType.NRO_CARACT_IGUALES_CONSECUTIVOS, component, parametro);
	    break;
	case NRO_DE_MAYUSCULAS_OBLIGATORIAS:
	    validarMaximo(ParametrosType.NRO_DE_MAYUSCULAS_OBLIGATORIAS, component, parametro);
	    break;
	case NRO_REINTENTOS_BLOQUEAR_CUENTA:
	    validarMaximo(ParametrosType.NRO_REINTENTOS_BLOQUEAR_CUENTA, component, parametro);
	default:
	    break;
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param largoMaximo
     * 
     * @param component
     * @param parametro
     * @since 1.X
     */
    private void validarMaximo(ParametrosType tipoParametro,
	    UIComponent component, Parametros parametro) {
	
	if (tipoParametro.getValorMaximo() < Integer.valueOf(parametro
		.getValor())) {
	    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
		    "Error:", EXCEPTION_INVALID_CHARACTER.replace(
			    ":nombreCampo",
			    component.getAttributes().get("nameComponentView")
				    .toString()));
	    parametro.setTestValidatorFail(true);
	    throw new ValidatorException(msg);
	}
	   parametro.setTestValidatorFail(false);
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param component
     * @param value
     * @since 1.X
     */
    private void validacionDeTipoDeDatoEntero(UIComponent component,
	    Object value) {
	Pattern pat = Pattern.compile(PATTERN_INTEGER);
	Matcher mat = pat.matcher(value.toString());

	if (!mat.matches()) {
	    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
		    "Error:", EXCEPTION_INVALID_CHARACTER.replace(
			    ":nombreCampo",
			    component.getAttributes().get("nameComponentView")
				    .toString()));
	 
	    throw new ValidatorException(msg);
	}
    }
}
