package com.acl.web.converters;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.NoneScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.acl.model.utils.CommonUtils;

/**
 * Converter para crear una lista de Strings separados por coma a partir de un
 * campo en particular de cada objeto de una lista
 * 
 * REQUIERE se use en conjunto con un f:attribute de name="fieldName" que
 * especifique el campo a utilizar de cada objeto en la lista
 * 
 * 
 * 
 */
@ManagedBean(name = "fieldOfObjListConverter")
@NoneScoped
public class FieldOfObjListConverter implements Converter {
    private static final String ERROR_STR = "ERROR.FieldOfObjListConverter";
    private static final String SEPARATOR = ", ";
    private static final Logger logger = LoggerFactory
	    .getLogger(FieldOfObjListConverter.class);

    @SuppressWarnings("unchecked")
    @Override
    public String getAsString(FacesContext context, UIComponent component,
	    Object value) {

	String getterName = "get"
		+ WordUtils.capitalize((String) component.getAttributes().get(
			"fieldName"));

	List<Object> listObj = (List<Object>) value;
	StringBuffer listStr = new StringBuffer();

	try {
	    for (Object obj : listObj) {

		String strFieldValue = parserFieldValue(obj, getterName);
		if (!CommonUtils.isEmpty(strFieldValue)) {
		    listStr.append(strFieldValue.concat(SEPARATOR));
		}
	    }
	    return listStr.length() > 0 ? listStr.substring(0,
		    listStr.length() - 2) : null;
	} catch (SecurityException e) {
	    logger.error(ERROR_STR, e);
	    return ERROR_STR;
	} catch (IllegalArgumentException e) {
	    logger.error(ERROR_STR, e);
	    return ERROR_STR;
	} catch (NoSuchMethodException e) {
	    logger.error(ERROR_STR, e);
	    return ERROR_STR;
	} catch (IllegalAccessException e) {
	    logger.error(ERROR_STR, e);
	    return ERROR_STR;
	} catch (InvocationTargetException e) {
	    logger.error(ERROR_STR, e);
	    return ERROR_STR;
	}
    }

    private String parserFieldValue(Object obj, String getterName)
	    throws SecurityException, IllegalArgumentException,
	    IllegalAccessException, InvocationTargetException,
	    NoSuchMethodException {
	Method method = null;
	try {
	    method = obj.getClass().getDeclaredMethod(getterName, new Class[0]);
	} catch (NoSuchMethodException e) {
	    logger.error(ERROR_STR, e.getMessage());
	}
	if (null == method) {
	    method = obj.getClass().getSuperclass()
		    .getDeclaredMethod(getterName, new Class[0]);
	}

	Object fieldValue = method.invoke(obj, new Object[0]);
	String strFieldValue = fieldValue != null ? fieldValue.toString()
		: null;
	return strFieldValue;

    }

    /*
     * if ( strFieldValue != null && !strFieldValue.isEmpty() ) {
     * listStr.append( fieldValue ); listStr.append( ", " ); }
     */
    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
	return null;
    }
}
