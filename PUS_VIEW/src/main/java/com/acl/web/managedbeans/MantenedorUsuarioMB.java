package com.acl.web.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.acl.model.Log;
import com.acl.model.Rol;
import com.acl.model.Usuario;
import com.acl.model.commons.GlobalConstants;
import com.acl.model.exception.ServiceException;
import com.acl.model.filter.FiltroUsuario;
import com.acl.model.types.EstadoCambioType;
import com.acl.model.types.EstadoRolType;
import com.acl.model.types.EstadoUsuarioType;
import com.acl.model.utils.CommonUtils;
import com.acl.model.utils.MessageUtils;
import com.acl.model.utils.PasswordUtils;
import com.acl.model.utils.ValidationUtils;
import com.acl.service.RolService;
import com.acl.service.UsuarioService;
import com.acl.web.handler.AutowiredHandler;

@Component
@ManagedBean(name = "mantenedorUsuario")
@ViewScoped
public class MantenedorUsuarioMB extends AutowiredHandler implements
	Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * Constante de logger
     */
    private static final Logger logger = Logger
	    .getLogger(MantenedorUsuarioMB.class);

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private RolService rolService;

    private Usuario usuario;

    private Usuario usuarioSeleccionado;

    private List<Usuario> listaUsuarios;

    private List<Rol> roles;

    private List<Rol> listaRolesToSave;

    private FiltroUsuario filtro;

    public FiltroUsuario getFiltro() {
	return filtro;
    }

    public void setFiltro(FiltroUsuario filtro) {
	this.filtro = filtro;
    }

    public void setUsuarioService(UsuarioService usuarioService) {
	this.usuarioService = usuarioService;
    }

    public void setRolService(RolService rolService) {
	this.rolService = rolService;
    }

    public List<Usuario> getListaUsuarios() {
	return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
	this.listaUsuarios = listaUsuarios;
    }

    public Usuario getUsuario() {
	return usuario;
    }

    public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
    }

    public List<Rol> getRoles() {
	return roles;
    }

    public void setRoles(List<Rol> roles) {
	this.roles = roles;
    }

    public EstadoUsuarioType[] getEstados() {
	return EstadoUsuarioType.values();
    }

    public Usuario getUsuarioSeleccionado() {
	return usuarioSeleccionado;
    }

    public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
	this.usuarioSeleccionado = usuarioSeleccionado;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @see com.acl.web.handler.AutowiredHandler#init()
     * @since 1.X
     */
    @PostConstruct
    @Override
    protected void init() {
	super.init();
	usuario = new Usuario();
	roles = rolService.buscarRoles(null);
	usuario = new Usuario();
	filtro = new FiltroUsuario();
	usuarioSeleccionado = new Usuario();
	listaRolesToSave = new ArrayList<Rol>();
	listaUsuarios = new ArrayList<Usuario>();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void buscarUsuarios() {
	logger.info("Busca usuarios por filtro: " + filtro);
	listaUsuarios.clear();

	if (filtro.getRut() == null
		&& !GlobalConstants.EMPTY.equalsIgnoreCase(filtro.getDv())) {
	    MessageUtils.addWarnMessage("Rut incorrecto",
		    "El rut ingresado para realizar la busqueda es incorrecto");
	    return;
	} else if (filtro.getRut() != null
		&& GlobalConstants.EMPTY.equalsIgnoreCase(filtro.getDv())) {
	    MessageUtils.addWarnMessage("Rut incorrecto",
		    "El rut ingresado para realizar la busqueda es incorrecto");
	    return;
	} else if (filtro.getRut() != null
		&& !GlobalConstants.EMPTY.equalsIgnoreCase(filtro.getDv())
		&& !ValidationUtils.validarRut(filtro.getRut().toString(),
			filtro.getDv())) {
	    MessageUtils.addWarnMessage("Rut incorrecto",
		    "El rut ingresado para realizar la busqueda es incorrecto");
	    return;
	}

	listaUsuarios = this.usuarioService.buscarUsuariosPorFiltro(filtro);

	for (Usuario usuario : listaUsuarios) {
	    usuario.setListaRoles(eliminarRolesInactivos(usuario
		    .getListaRoles()));
	}
	logger.info(listaUsuarios);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void guardarUsuario() {

	try {
	    // Valida que el nombre ingresado no sean solo espacios
	    Log log = CommonUtils.createLog();
	    validarCamposEnBlanco(usuario);
	    usuario.setLog(log);

	    usuario.setListaRoles(llenarFechaRolLog(usuario.getListaRoles(),
		    log));
	    usuarioService.guardar(usuario);
	    MessageUtils.addInfoMessage("Usuario guardado Exitosamente", "");
	    buscarUsuarios();
	} catch (ServiceException e) {
	    logger.error(e.getMessage(), e);
	    MessageUtils.addErrorMessage(e.getMessage());
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param listaRoles
     * @since 1.X
     */
    private List<Rol> llenarFechaRolLog(List<Rol> listaRoles, Log log) {

	if (listaRoles != null && !listaRoles.isEmpty()) {
	    for (Rol rol : listaRoles) {
		rol.setLog(log);
	    }
	}

	return listaRoles;

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws ServiceException
     * 
     * @since 1.X
     */
    private void validarCamposEnBlanco(Usuario userEval)
	    throws ServiceException {

	if (userEval.getNombres().trim().isEmpty()
		|| userEval.getApellidoMaterno().trim().isEmpty()
		|| userEval.getApellidoPaterno().trim().isEmpty()
		|| userEval.getEmail().trim().isEmpty()) {
	    throw new ServiceException(
		    "Existen campos que no han sido ingresados correctamente");

	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void editarUsuario() {

	try {
	    if (seRealizaronCambiosCambios()) {
		Log log = CommonUtils.createLog();
		validarCamposEnBlanco(usuarioSeleccionado);
		usuarioSeleccionado.setLog(log);
		// Procesar Roles de usuario
		procesarRoles();
		
		usuarioSeleccionado.setListaRoles(llenarFechaRolLog(listaRolesToSave, log));
		usuarioService.actualizarUsuario(usuarioSeleccionado);
		MessageUtils.addInfoMessage("Usuario editado Exitosamente", "");
		buscarUsuarios();

	    } else {
		MessageUtils.addWarnMessage("Warning: No hay cambios",
			"No se ha actualizado la información del usuario.");
	    }

	} catch (ServiceException e) {
	    logger.error(e.getMessage(), e);
	    MessageUtils.addErrorMessage(e.getMessage());
	} catch (Exception e) {
	    logger.error(e.getMessage(), e);
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private Boolean seRealizaronCambiosCambios() throws ServiceException {

	// Ordena las listas base y seleccionadas
	Collections.sort(usuario.getListaRoles());
	Collections.sort(usuarioSeleccionado.getListaRoles());
	String newPass = usuarioSeleccionado.getContrasena().getPassword();
	boolean cambioPass = false;

	// Si ingresa contrasena
	if (newPass != null
		&& !GlobalConstants.EMPTY.equalsIgnoreCase(newPass.trim())) {
	    newPass = PasswordUtils.convertirPasswordSha256(newPass);
	    cambioPass = true;
	}

	logger.info(usuario);
	logger.info(usuarioSeleccionado);

	// Info Basica
	if ((!usuario.getNombres().equalsIgnoreCase(
		usuarioSeleccionado.getNombres()))
		|| (!usuario.getApellidoMaterno().equalsIgnoreCase(
			usuarioSeleccionado.getApellidoMaterno()))
		|| (!usuario.getApellidoPaterno().equalsIgnoreCase(
			usuarioSeleccionado.getApellidoPaterno()))
		|| (!usuario.getEmail().equalsIgnoreCase(
			usuarioSeleccionado.getEmail()))
		|| (!usuario.getFono().equalsIgnoreCase(
			usuarioSeleccionado.getFono()))
		|| (!usuario.getEstado()
			.equals(usuarioSeleccionado.getEstado()))
		|| (!usuario.getEsSuperUsuario().equals(
			usuarioSeleccionado.getEsSuperUsuario()))
		|| (!listasSonIguales(usuarioSeleccionado.getListaRoles(),
			usuario.getListaRoles()))
		|| (cambioPass && !(newPass.equalsIgnoreCase(usuario
			.getContrasena().getPassword())))) {

	    logger.info("Hay cambios");
	    return true;

	}
	logger.info("No Hay cambios");
	return false;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param listaRoles
     * @param listaRoles2
     * @return
     * @since 1.X
     */
    private boolean listasSonIguales(List<Rol> rolesSelected,
	    List<Rol> rolesBases) {
	List<Rol> rolesAUX = new ArrayList<Rol>();

	for (Rol rol : rolesBases) {
	    if (!rol.getEstado().equals(EstadoRolType.INACTIVO)) {
		rolesAUX.add(rol);
	    }
	}

	Collections.sort(rolesAUX);
	return rolesSelected.equals(rolesAUX);
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private void procesarRoles() {
	List<Rol> rolesSeleccionados = usuarioSeleccionado.getListaRoles();
	Log log = CommonUtils.createLog();

	// Roles que se deseleccionaron se deben desactivar
	for (Rol rolBase : listaRolesToSave) {
	    if (!rolesSeleccionados.contains(rolBase)) {
		rolBase.setEstadoCambio(EstadoCambioType.DELETE);
		rolBase.setLog(log);
		rolBase.setEstado(EstadoRolType.INACTIVO);
	    }
	}

	// Roles que se activaron
	for (Rol rolNew : rolesSeleccionados) {
	    // Roles que existen en la tabla relacion pero se encuentran con
	    // estado inactivo
	    if (rolNew.getEstadoCambio().equals(EstadoCambioType.NEW)
		    && listaRolesToSave.contains(rolNew)
		    && listaRolesToSave.get(listaRolesToSave.indexOf(rolNew))
			    .getEstado().equals(EstadoRolType.INACTIVO)) {
		rolNew = listaRolesToSave.get(listaRolesToSave.indexOf(rolNew));
		listaRolesToSave.remove(rolNew);
		rolNew.setEstadoCambio(EstadoCambioType.UPDATE);
		rolNew.setEstado(EstadoRolType.ACTIVO);
		rolNew.setLog(log);
		listaRolesToSave.add(rolNew);
	    }

	    // Roles que no existen en la relacion en Base de datos
	    if (rolNew.getEstadoCambio().equals(EstadoCambioType.NEW)
		    && !listaRolesToSave.contains(rolNew)) {
		rolNew.setLog(log);
		listaRolesToSave.add(rolNew);
	    }

	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param userToEdit
     * @since 1.X
     */
    public void loadPopUpEdicionUsuario(ActionEvent event) {
	usuario = (Usuario) event.getComponent().getAttributes().get("usuario");
	listaRolesToSave.clear();

	try {
	    FiltroUsuario nuevoFiltro = new FiltroUsuario(usuario.getRut(),
		    usuario.getDv());
	    usuarioSeleccionado = usuarioService.buscarUsuariosPorFiltro(
		    nuevoFiltro).get(0);
	    usuario.getContrasena().setPassword(
		    usuarioSeleccionado.getContrasena().getPassword());
	    usuarioSeleccionado.getContrasena().setPassword(
		    GlobalConstants.EMPTY);

	    // Eliminar roles que no estan activos
	    usuarioSeleccionado
		    .setListaRoles(eliminarRolesInactivos(usuarioSeleccionado
			    .getListaRoles()));

	    // Se agregan todos los roles "base" a la lista a guardar
	    listaRolesToSave.addAll(usuarioSeleccionado.getListaRoles());

	} catch (Exception e) {
	    MessageUtils.addErrorMessage(e.getMessage());
	    logger.error(e.getMessage(), e);
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private List<Rol> eliminarRolesInactivos(List<Rol> listaRoles) {
	List<Rol> listaRolesFinal = new ArrayList<Rol>();

	if (listaRoles != null) {
	    for (Rol rol : listaRoles) {
		if (EstadoRolType.ACTIVO.equals(rol.getEstado())) {
		    listaRolesFinal.add(rol);
		} else {
		    listaRolesToSave.add(rol);
		}
	    }
	}
	return listaRolesFinal;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void cleanUsuario() {
	usuario = new Usuario();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void cleanRoles() {
	usuario.setListaRoles(new ArrayList<Rol>());
    }

}