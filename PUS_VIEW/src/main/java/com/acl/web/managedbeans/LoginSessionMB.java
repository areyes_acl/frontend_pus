/**
 * 
 */
package com.acl.web.managedbeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.acl.model.Usuario;
import com.acl.model.exception.UsuarioServiceException;
import com.acl.model.utils.MessageUtils;
import com.acl.service.UsuarioService;
import com.acl.web.handler.AutowiredHandler;
import com.acl.web.utils.SessionUtils;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2017, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@ManagedBean(name = "loginSession")
@SessionScoped
public class LoginSessionMB extends AutowiredHandler implements Serializable {
    private static final Logger logger = Logger.getLogger(LoginSessionMB.class);
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Usuario usuario;
    private Integer rut;
    private String dv;
    private String password;
    private String loginUser;
    private String messageForceLogout;
    private boolean loggedIn;
    private boolean showLogout;
    private static final String INDEX_VIEW = "/private/index.jsf?faces-redirect=true";	
    private static final String LOGIN_VIEW_JSF = "/login.jsf";
    private static final String LOGIN_VIEW = "/login.xhtml";

    @Autowired
    UsuarioService usuarioService;

    @ManagedProperty(value = "#{utils}")
    private UtilsMB utils;

    public void setUtils(UtilsMB utils) {
	this.utils = utils;
    }

    public String getLoginUser() {
	return loginUser;
    }

    public void setLoginUser(String loginUser) {
	this.loginUser = loginUser;
    }

    public boolean isLoggedIn() {
	return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
	this.loggedIn = loggedIn;
    }

    public String getMessageForceLogout() {
	return messageForceLogout;
    }

    public void setMessageForceLogout(String messageForceLogout) {
	this.messageForceLogout = messageForceLogout;
    }

    public static long getSerialversionuid() {
	return serialVersionUID;
    }

    public static String getIndexView() {
	return INDEX_VIEW;
    }

    public UtilsMB getUtils() {
	return utils;
    }

    public Integer getRut() {
	return rut;
    }

    public void setRut(Integer rut) {
	this.rut = rut;
    }

    public String getDv() {
	return dv;
    }

    public void setDv(String dv) {
	this.dv = dv;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public void setUsuarioService(UsuarioService usuarioService) {
	this.usuarioService = usuarioService;
    }

    public Usuario getUsuario() {
	return usuario;
    }

    public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
    }

    public boolean isShowLogout() {
	return showLogout;
    }

    public void setShowLogout(boolean showLogout) {
	this.showLogout = showLogout;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @see com.acl.web.handler.AutowiredHandler#init()
     * @since 1.X
     */
    @PostConstruct
    @Override
    protected void init() {
	super.init();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que realiza la autenticación del usuario en el sistema con el rut
     * y la contrasena
     * 
     * @return
     * @since 1.X
     */
    public String doLogin() {

	// Envia los datos a la logica de negocio que realiza las
	// validaciones
	try {
	    logger.info("=== Login de Usuario ===");
	    usuario = usuarioService.login(password, rut, dv);
	    MessageUtils.addInfoMessage("Login Exitoso",
		    "Usuario se ha logueado correctamente");
	    HttpSession hs = SessionUtils.getSession();
	    hs.setAttribute("usuario", usuario);
	    loggedIn = true;
	    logger.info("Usuario Logueado correctamente");
	    return INDEX_VIEW;
	} catch (UsuarioServiceException e) {
	    MessageUtils.addErrorMessage(e.getMessage());
	    logger.error(e.getMessage(), e);
	    HttpSession hs = SessionUtils.getSession();
	    hs.removeAttribute("usuario");
	    loggedIn = false;
	    hs.invalidate();
	} catch (Exception e) {
	    MessageUtils.addErrorMessage(e.getMessage());
	    logger.error(e.getMessage(), e);
	    HttpSession hs = SessionUtils.getSession();
	    hs.removeAttribute("usuario");
	    loggedIn = false;
	    hs.invalidate();
	}

	return LOGIN_VIEW_JSF;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * 
     * OJO!, Se debe validar si es PostBack, para evitar que el metodo se
     * ejecute en todos los request (incluidos Ajax) Solo se debe ejecutar en el
     * Request Inicial de la vista
     * 
     * @since 1.X
     */
    public void validateIsLogin() {

	logger.info("Valida si el usuario està logueado en el resquest inicial");
	if (FacesContext.getCurrentInstance().isPostback()) {
	    return;
	}
	String currentView = FacesContext.getCurrentInstance().getViewRoot()
		.getViewId();

	if (currentView.equals(LOGIN_VIEW) && loggedIn) {
	    utils.redirect(INDEX_VIEW);
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void doLogout() {
	logger.debug("Se realiza el proceso de loguot");
	HttpSession session = SessionUtils.getSession();
	session.invalidate();
	loggedIn = false;
	logger.debug("logged: " + loggedIn);
	utils.redirect(LOGIN_VIEW_JSF);

    }

}
