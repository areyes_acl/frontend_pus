package com.acl.web.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.acl.model.Parametros;
import com.acl.model.filter.FiltroParametro;
import com.acl.model.types.ContrasenaType;
import com.acl.model.types.EstadoParametroType;
import com.acl.model.types.ParametrosType;
import com.acl.service.ParametrosService;
import com.acl.web.handler.AutowiredHandler;

@Component
@ManagedBean(name = "mantenedorParametros")
@ViewScoped
public class MantenedorParametrosMB extends AutowiredHandler implements
	Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -652688932033005861L;
    private static Logger logger = Logger
	    .getLogger(MantenedorParametrosMB.class);
    private static final String TIPO_CAMPO_SWITCH = "inputSwitch";

    @Autowired
    private ParametrosService parametrosService;
    private List<Parametros> listaParametros;
    private FiltroParametro filtro;
    private List<String> tipoClave;
//    private Boolean error;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public MantenedorParametrosMB() {
	super();
	tipoClave = new ArrayList<String>();
	tipoClave.add(ContrasenaType.ALFABETICA.getLabel());
	tipoClave.add(ContrasenaType.ALFANUMERICA.getLabel());
	tipoClave.add(ContrasenaType.NUMERICA.getLabel());

    }

    public List<String> getTipoClave() {
	return tipoClave;
    }

    public void setTipoClave(List<String> tipoClave) {
	this.tipoClave = tipoClave;
    }

    public void setParametrosService(ParametrosService parametrosService) {
	this.parametrosService = parametrosService;
    }

    public List<Parametros> getListaParametros() {
	return converterListParameterBolean(this.listaParametros);
    }

    public void setListaParametros(List<Parametros> listaParametros) {
	this.listaParametros = listaParametros;
    }

    public FiltroParametro getFiltro() {
	return filtro;
    }

    public void setFiltro(FiltroParametro filtro) {
	this.filtro = filtro;
    }
//
//    public Boolean getError() {
//	return error;
//    }
//
//    public void setError(Boolean error) {
//	this.error = error;
//    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param event
     * @since 1.X
     */
    public void update(ActionEvent event) {
	Parametros parametro = (Parametros) event.getComponent()
		.getAttributes().get("parametro");

	// Validar que el parametro que viene de vista sea minimo o maximo y
	// compararlo con el parametro de la lista de parametros que es con
	// quien se interactua

	if (esValidoLargoMinimoYMaximo(parametro)) {
//	    error = false;
	    parametro.setTestValidatorFail(false);
	    converterListParameter(parametro);
	    if (parametrosService.update(parametro) > 0) {

		parametro.setNewValor(parametro.getValor());
		parametro.changedValor();

		FacesContext.getCurrentInstance().addMessage(
			null,
			new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
				"Su Parametro "
					+ parametro.getParametro().getView()
					+ " Fue actualizado correctamente."));
	    } else {
		FacesContext.getCurrentInstance().addMessage(
			null,
			new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!",
				"Su Parametro "
					+ parametro.getParametro().getView()
					+ " No pudo ser actualizado."));

	    }
	} else {
//	    error = true;
	    parametro.setTestValidatorFail(true);
	    FacesContext
		    .getCurrentInstance()
		    .addMessage(
			    null,
			    new FacesMessage(FacesMessage.SEVERITY_ERROR,
				    "Error!",
				    "El campo largo mínimo no puede ser mayor al campo largo máximo"));
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param parametro
     * @since 1.X
     */
    private Boolean esValidoLargoMinimoYMaximo(Parametros parametro) {
	Parametros parametroAux = null;
	Boolean esValido = Boolean.TRUE;

	
	if (ParametrosType.LARGO_MAXIMO.equals(parametro.getParametro())) {
	    // obtener el largo minimo
	    parametroAux = obtenerParametroPorTipo(ParametrosType.LARGO_MINIMO);
	    if (parametroAux != null
		    && Integer.parseInt(parametroAux.getValor()) > Integer
			    .parseInt(parametro.getValor())) {
		
		esValido = Boolean.FALSE;
	    }

	} else if (ParametrosType.LARGO_MINIMO.equals(parametro.getParametro())) {
	    // obtener el largo maximo
	    parametroAux = obtenerParametroPorTipo(ParametrosType.LARGO_MAXIMO);
	    
	    if (parametroAux != null
		    && Integer.parseInt(parametroAux.getValor()) < Integer
			    .parseInt(parametro.getValor())) {
	
		esValido = Boolean.FALSE;
	    }

	}

	return esValido;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private Parametros obtenerParametroPorTipo(ParametrosType tipoParametro) {
	for (Parametros parametro : listaParametros) {
	    if (tipoParametro.equals(parametro.getParametro())) {
		return parametro;
	    }
	}
	return null;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param listaParametros
     * @return
     * @since 1.X
     */
    private List<Parametros> converterListParameterBolean(
	    List<Parametros> listaParametros) {
	for (Parametros x : listaParametros) {
	    if (TIPO_CAMPO_SWITCH.equals(x.getParametro().getObje())) {
		if (x.getValor().equals("1")) {
		    x.setValor("true");
		} else if (x.getValor().equals("0")) {
		    x.setValor("false");
		}
	    }
	}
	return listaParametros;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param x
     * @return
     * @since 1.X
     */
    private Parametros converterListParameter(Parametros x) {

	if (TIPO_CAMPO_SWITCH.equals(x.getParametro().getObje())) {
	    if (x.getValor().equals("true")) {
		x.setValor("1");
	    } else {
		x.setValor("0");
	    }
	}

	return x;
    }
    
//    public void cleanError(){
//	error = Boolean.FALSE;
//    }
    

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @see com.acl.web.handler.AutowiredHandler#init()
     * @since 1.X
     */
    @PostConstruct
    @Override
    protected void init() {
	super.init();
	filtro = new FiltroParametro(EstadoParametroType.ACTIVO);
	listaParametros = parametrosService.buscarPorFiltro(filtro);
	logger.info(listaParametros);
    }

}