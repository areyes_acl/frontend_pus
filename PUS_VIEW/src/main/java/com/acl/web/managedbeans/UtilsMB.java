/**
 * 
 */
package com.acl.web.managedbeans;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.NoneScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.acl.model.types.IType;
import com.acl.model.utils.CommonUtils;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@NoneScoped
@ManagedBean(name = "utils")
public class UtilsMB implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -1271614041174670489L;
    /**
     * Constante de logger
     */
    private static final Logger logger = Logger.getLogger(UtilsMB.class);

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param url
     * @since 1.X
     */
    public void redirect(String url) {

	ExternalContext ec = FacesContext.getCurrentInstance()
		.getExternalContext();
	try {
	    if (!ec.isResponseCommitted()) {
		ec.redirect(ec.getRequestContextPath() + url);
	    }
	} catch (IOException e) {
	    logger.error("Error al redirigir..." + e);
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param p_type
     * @return
     * @since 1.X
     */
    public List<IType> genericType(String p_type) {

	return CommonUtils.getGenericType("com.acl.model.types" + p_type);

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2014, (Everis Chile) - versión inicial
     * </ul>
     * <p>
     * 
     * @param document
     * @since 1.X
     */
    public void postProcessXLS(Object document) {

	final HSSFWorkbook wb = (HSSFWorkbook) document;
	final HSSFSheet sheet = wb.getSheetAt(0);
	final HSSFRow header = sheet.getRow(0);
	final HSSFCellStyle cellStyle = wb.createCellStyle();
	cellStyle.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
	cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	int colums = header.getPhysicalNumberOfCells();
	for (int i = 0; i < colums; i++) {
	    ((HSSFCell) header.getCell(i)).setCellStyle(cellStyle);
	}

	// / se comprime la salida en formato zip
	String contentType = "application/zip";
	DateFormat dateFormat = new SimpleDateFormat("yyyymmddHHmmss");
	Date date = new Date();
	FacesContext fc = FacesContext.getCurrentInstance();
	HttpServletResponse response = (HttpServletResponse) fc
		.getExternalContext().getResponse();
	response.reset();
	/* TODO : ver forma de pasar el nombre dinamicamente */
	String fileName = ""
		+ dateFormat.format(date).toString()
		+ "_"
		+ fc.getExternalContext().getRequestParameterMap()
			.get("fileName");
	try {

	    OutputStream output = response.getOutputStream();

	    ZipOutputStream zipOut = new ZipOutputStream(output);
	    ZipEntry entry = new ZipEntry(fileName + ".xls");
	    zipOut.putNextEntry(entry);
	    response.setHeader("Content-Disposition", "attachment; filename="
		    + fileName + ".zip");
	    response.setContentType(contentType);

	    wb.write(zipOut);
	    zipOut.closeEntry();
	    output.flush();
	    output.close();

	    zipOut.flush();
	    zipOut.close();

	    fc.responseComplete();

	} catch (IOException e1) {
	    logger.error(e1.toString());
	}
    }

}
