package com.acl.web.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.acl.model.Log;
import com.acl.model.Pregunta;
import com.acl.model.commons.GlobalConstants;
import com.acl.model.exception.PreguntaServiceException;
import com.acl.model.filter.FiltroPregunta;
import com.acl.model.types.AccionType;
import com.acl.model.types.EstadoPreguntaType;
import com.acl.model.utils.CommonUtils;
import com.acl.model.utils.MessageUtils;
import com.acl.service.PreguntaService;
import com.acl.web.handler.AutowiredHandler;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@ManagedBean(name = "mantenedorPreguntas")
@ViewScoped
public class MantenedorPreguntasMB extends AutowiredHandler implements
	Serializable {
    /**
     * Constante de logger
     */
    private static final Logger logger = Logger
	    .getLogger(MantenedorPreguntasMB.class);

    /**
     * 
     */
    private static final long serialVersionUID = -9069030038949998892L;

    @Autowired
    private PreguntaService preguntaService;
    private Pregunta pregunta;
    private Pregunta preguntaSeleccionada;
    private List<Pregunta> ltsPreguntas;
    private FiltroPregunta filtro;

    public Pregunta getPregunta() {
	return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
	this.pregunta = pregunta;
    }

    public List<Pregunta> getLtsPreguntas() {
	return ltsPreguntas;
    }

    public void setLtsPreguntas(List<Pregunta> ltsPreguntas) {
	this.ltsPreguntas = ltsPreguntas;
    }

    public FiltroPregunta getFiltro() {
	return filtro;
    }

    public void setFiltro(FiltroPregunta filtro) {
	this.filtro = filtro;
    }

    public void setPreguntaService(PreguntaService preguntaService) {
	this.preguntaService = preguntaService;
    }

    public EstadoPreguntaType[] getEstados() {
	return EstadoPreguntaType.values();
    }

    public Pregunta getPreguntaSeleccionada() {
	return preguntaSeleccionada;
    }

    public void setPreguntaSeleccionada(Pregunta preguntaSeleccionada) {
	this.preguntaSeleccionada = preguntaSeleccionada;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @see com.acl.web.handler.AutowiredHandler#init()
     * @since 1.X
     */
    @PostConstruct
    @Override
    public void init() {
	super.init();
	filtro = new FiltroPregunta();
	pregunta = new Pregunta();
	ltsPreguntas = new ArrayList<Pregunta>();
	preguntaSeleccionada = new Pregunta();

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws Exception
     * @since 1.X
     */
    public void buscarPreguntas() {
	ltsPreguntas = preguntaService.buscarPreguntas(filtro);
	logger.info(ltsPreguntas);

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void cleanPregunta() {
	pregunta = new Pregunta();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void guardarPregunta() {
	try {
	    validarPreguntasEnBlanco(AccionType.NUEVO);
	    pregunta.getLog().setSidCreacion(Long.valueOf(1));
	    preguntaService.guardar(pregunta);
	    MessageUtils.addInfoMessage("Pregunta guardada Exitosamente", "");
	    buscarPreguntas();
	} catch (PreguntaServiceException e) {
	    logger.error(e.getMessage() + ":" + e.getDescripcion(), e);
	    MessageUtils.addErrorMessage(e.getMessage(), e.getDescripcion());
	}
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws PreguntaServiceException
     * 
     * @since 1.X
     */
    private void validarPreguntasEnBlanco(AccionType accion)
	    throws PreguntaServiceException {
	Pregunta preguntaAValidar = null;
	switch (accion) {
	case NUEVO:
	    preguntaAValidar = pregunta;
	    break;

	case ACTUALIZACION:
	    preguntaAValidar = preguntaSeleccionada;
	    break;
	default:
	    break;
	}

	if (preguntaAValidar != null
		&& preguntaAValidar.getPregunta() != null
		&& GlobalConstants.EMPTY.equalsIgnoreCase(preguntaAValidar
			.getPregunta().trim())) {
	    throw new PreguntaServiceException("Error",
		    "No se pueden ingresar preguntas en blanco");
	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws PreguntaServiceException
     * 
     * @since 1.X
     */
    public void actualizarPregunta() {

	try {
	    // VALIDAR QUE REALIZA CAMBIOS
	    if (existenCambios()) {

		validarPreguntasEnBlanco(AccionType.ACTUALIZACION);

		Log log = CommonUtils.createLog();
		preguntaSeleccionada.setLog(log);
		preguntaService.actualizar(preguntaSeleccionada);
		buscarPreguntas();
		logger.info("Se actualiza pregunta");
		MessageUtils.addInfoMessage("Actualizacion Correcta!",
			"Se ha actualizado el registro correctamente.");

	    } else {
		logger.info("NO actualiza pregunta");
		MessageUtils
			.addWarnMessage("No hay cambios",
				"No se ha registrado ningun cambio a la pregunta seleccionada.");

	    }
	} catch (PreguntaServiceException e) {
	    logger.error(e.getMessage(), e);
	    MessageUtils.addErrorMessage(e.getMessage(), e.getDescripcion());
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private Boolean existenCambios() {

	if (pregunta != null
		&& pregunta.getPregunta() != null
		&& preguntaSeleccionada != null
		&& preguntaSeleccionada.getPregunta() != null
		&& !pregunta.getPregunta().equalsIgnoreCase(
			preguntaSeleccionada.getPregunta())) {
	    return true;

	}
	if (pregunta != null && pregunta.getPregunta() != null
		&& preguntaSeleccionada != null
		&& preguntaSeleccionada.getEstado() != null
		&& !(pregunta.getEstado() == preguntaSeleccionada.getEstado())) {
	    return true;
	}

	return false;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param event
     * @since 1.X
     */
    public void loadPopUpEdicion(ActionEvent event) {
	pregunta = (Pregunta) event.getComponent().getAttributes()
		.get("pregunta");
	try {
	    FiltroPregunta filter = new FiltroPregunta();
	    filter.setSid(pregunta.getSid());
	    List<Pregunta> listaPreguntasEncontradas = preguntaService
		    .buscarPreguntas(filter);
	    if (listaPreguntasEncontradas != null
		    && !listaPreguntasEncontradas.isEmpty()) {
		preguntaSeleccionada = listaPreguntasEncontradas.get(0);
	    }
	} catch (Exception e) {
	    MessageUtils.addErrorMessage(e.getMessage());
	    logger.error(e.getMessage(), e);
	}

	logger.info(preguntaSeleccionada);

    }
}
