package com.acl.web.managedbeans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.acl.model.Rol;
import com.acl.model.exception.ServiceException;
import com.acl.model.filter.FiltroRoles;
import com.acl.model.types.EstadoRolType;
import com.acl.model.utils.CommonUtils;
import com.acl.model.utils.MessageUtils;
import com.acl.service.RolService;
import com.acl.web.handler.AutowiredHandler;

@Component
@ManagedBean(name = "mantenedorRoles")
@ViewScoped
public class MantenedorRolesMB extends AutowiredHandler implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7181371429169818078L;
    /**
     * Constante de logger
     */
    private static final Logger logger = Logger
	    .getLogger(MantenedorRolesMB.class);

    @Autowired
    private RolService rolService;

    private Rol rol;

    private Rol rolSeleccionado;

    private FiltroRoles filtro;

    private List<Rol> roles;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    public EstadoRolType[] getEstados() {
	return EstadoRolType.values();
    }

    public Rol getRol() {
	return rol;
    }

    public void setRol(Rol rol) {
	this.rol = rol;
    }

    public List<Rol> getRoles() {
	return roles;
    }

    public void setRoles(List<Rol> roles) {
	this.roles = roles;
    }

    public void setRolService(RolService rolService) {
	this.rolService = rolService;
    }

    public FiltroRoles getFiltro() {
	return filtro;
    }

    public void setFiltro(FiltroRoles filtro) {
	this.filtro = filtro;
    }

    public Rol getRolSeleccionado() {
	return rolSeleccionado;
    }

    public void setRolSeleccionado(Rol rolSeleccionado) {
	this.rolSeleccionado = rolSeleccionado;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @see com.acl.web.handler.AutowiredHandler#init()
     * @since 1.X
     */
    @PostConstruct
    @Override
    protected void init() {
	super.init();
	rol = new Rol();
	filtro = new FiltroRoles();
	rolSeleccionado = new Rol();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void buscarRoles() {
	logger.debug("Busca Roles");
	roles = this.rolService.buscarRoles(filtro);
	logger.info(roles);

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void guardarRol() {
	logger.debug("Rol a guardar : " + rol);

	try {
	    rol.getLog().setSidModificacion(Long.valueOf(1));
	    rolService.guardar(rol);
	    MessageUtils.addInfoMessage("Rol guardado Exitosamente", "");

	} catch (ServiceException e) {
	    logger.error(e.getMessage(), e);
	    MessageUtils.addErrorMessage(e.getMessage());
	} finally {
	    cleanRol();
	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void editarRol() {
	logger.info("Editar Rol");

	try {
	    if (seRealizaronCambiosCambios()) {
		validarCamposEnBlanco(rolSeleccionado);
		rolSeleccionado.setLog(CommonUtils.createLog());
		rolService.actualizarRol(rolSeleccionado);
		MessageUtils.addInfoMessage("Rol editado Exitosamente", "");
		buscarRoles();

	    } else {
		MessageUtils.addWarnMessage("Warning: No hay cambios",
			"No se ha actualizado la información del usuario.");
	    }

	} catch (ServiceException e) {
	    logger.error(e.getMessage(), e);
	    MessageUtils.addErrorMessage(e.getMessage());
	} catch (Exception e) {
	    logger.error(e.getMessage(), e);
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rolSeleccionado2
     * @throws ServiceException
     * @since 1.X
     */
    private void validarCamposEnBlanco(Rol rolEval) throws ServiceException {
	if (rolEval.getNombre().trim().isEmpty()
		|| rolEval.getDescripcion().trim().isEmpty()) {
	    throw new ServiceException(
		    "Existen campos que no han sido ingresados correctamente");

	}
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @since 1.X
     */
    private Boolean seRealizaronCambiosCambios() throws ServiceException {

	// Info Basica
	if ((!rol.getNombre().equalsIgnoreCase(rolSeleccionado.getNombre()))
		|| (!rol.getDescripcion().equalsIgnoreCase(
			rolSeleccionado.getDescripcion()))
		|| (!rol.getEstado().equals(rolSeleccionado.getEstado()))) {

	    logger.info("Hay cambios");
	    return true;

	}
	logger.info("No Hay cambios");
	return false;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * Método que carga el rol que se va a editar con la informacion extraida
     * desde BD
     * 
     * @param userToEdit
     * @since 1.X
     */
    public void loadPopUpEdicionRol(ActionEvent event) {
	rol = (Rol) event.getComponent().getAttributes().get("rol");

	FiltroRoles nuevoFiltro = new FiltroRoles();
	nuevoFiltro.setSid(rol.getSid());

	List<Rol> rolesBuscados = rolService.buscarRoles(nuevoFiltro);
	rolSeleccionado = (rolesBuscados != null && !rolesBuscados.isEmpty()) ? rolesBuscados
		.get(0) : new Rol();

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void cleanRol() {
	rol = new Rol();
	rolSeleccionado = new Rol();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public void viewPermision() {

	Map<String, Object> options = new HashMap<String, Object>();
	options.put("modal", true);
	RequestContext.getCurrentInstance().openDialog(
		"menuViewRolPermisionMB", options, null);

    }
}