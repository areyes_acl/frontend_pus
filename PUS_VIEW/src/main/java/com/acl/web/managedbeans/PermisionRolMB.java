/**
 * 
 */
package com.acl.web.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.acl.model.Permiso;
import com.acl.model.Rol;
import com.acl.model.RolSubMenu;
import com.acl.model.SubMenu;
import com.acl.model.utils.MessageUtils;
import com.acl.service.PermisoRolService;
import com.acl.web.handler.AutowiredHandler;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Component
@ManagedBean(name = "permisionRolMB")
@ViewScoped
public class PermisionRolMB extends AutowiredHandler implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8803600089800123927L;
    private ArrayList<RolSubMenu> rolesPermisos;
    private Rol rolSelection = new Rol();

    @Autowired
    private PermisoRolService permisoRolService;

    public void rolListener(ActionEvent event) {

	this.rolSelection = (Rol) event.getComponent().getAttributes()
		.get("roles");
	RequestContext context = RequestContext.getCurrentInstance();
	context.execute("PF('viewPermision').show();");

    }

    public void update() {

	for (RolSubMenu x : rolesPermisos) {

	    if (x.getPermisionsListNameSelection() != null) {

		permisoRolService.updateEstate(x);

	    }
	}
	
	MessageUtils.addInfoMessage("Se han guardado/actualizado los permisos del rol correctamente","");
	RequestContext context = RequestContext.getCurrentInstance();
	context.execute("PF('viewPermision').hide();");

    }

    public ArrayList<RolSubMenu> getRolesPermisos() {
	rolesPermisos = new ArrayList<RolSubMenu>();

	List<SubMenu> listSubMenu = permisoRolService.getAllSubMenu();
	List<Permiso> listPermiso = permisoRolService.getAllPermisos();
	for (SubMenu sm : listSubMenu) {
	    RolSubMenu rp = new RolSubMenu();
	    rp.setSubmenu(sm);

	    rp.setPermisoList(listPermiso);
	    rolesPermisos.add(rp);
	}
	checkPermisoRol();
	return rolesPermisos;
    }

    public List<RolSubMenu> checkPermisoRol() {
	if (rolSelection.getSid() != null) {

	    for (RolSubMenu rolSubMenuBD : rolesPermisos) {

		rolSubMenuBD.setRol(rolSelection);
		rolSubMenuBD.setEstado(1L);
		List<Permiso> permisoBDactivate = permisoRolService
			.getAllPermisoForRol(rolSubMenuBD, "1");

		for (Permiso pActivate : permisoBDactivate) {

		    for (Permiso pview : rolSubMenuBD.getPermisoList()) {

			if (pview.getSid() == pActivate.getSid()) {
			    pview.setCheck(true);

			    rolSubMenuBD.addpermisionsListNameSelection(pview
				    .getPermiso());

			}
		    }
		}
		permisoBDactivate = null;
	    }
	}
	return rolesPermisos;

    }

    public void setRolesPermisos(ArrayList<RolSubMenu> rolesPermisos) {
	this.rolesPermisos = rolesPermisos;
    }

    public Rol getRolSelection() {
	return rolSelection;
    }

    public void setRolSelection(Rol rolSelection) {
	this.rolSelection = rolSelection;
    }

    public PermisoRolService getPermisoRolService() {
	return permisoRolService;
    }

    public void setPermisoRolService(PermisoRolService permisoRolService) {
	this.permisoRolService = permisoRolService;
    }

}
