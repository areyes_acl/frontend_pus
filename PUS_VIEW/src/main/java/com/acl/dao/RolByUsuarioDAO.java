package com.acl.dao;

import java.util.List;

import com.acl.model.Rol;
import com.acl.model.exception.RolByUserException;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface RolByUsuarioDAO {

    public void agregarRolesUsuarioBySid(Long sidUsuario, List<Rol> roles);

    public void agregarRolUsuarioBySid(Long sidUsuario, Rol rol)
	    throws RolByUserException;

    public void agregarRolesUsuarioByRut(Integer rut, List<Rol> roles);

    public void agregarRolUsuarioByRut(Integer rut, Rol rol);

    public List<Rol> obtenerRolesPorRutUsuario(Integer rut);
    
    public void actualizarRolUsuario(Long sidUsuario, Rol rol) throws RolByUserException;

}
