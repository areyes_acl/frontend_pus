package com.acl.dao;

import java.util.List;

import com.acl.model.Pregunta;
import com.acl.model.exception.PreguntaServiceException;
import com.acl.model.filter.FiltroPregunta;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface PreguntaDAO {

    public List<Pregunta> buscarPreguntas(FiltroPregunta filtro);

    public void guardar(Pregunta pregunta) throws PreguntaServiceException;
    
    public void actualizar(Pregunta pregunta) throws PreguntaServiceException;

}
