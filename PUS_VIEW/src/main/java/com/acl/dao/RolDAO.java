package com.acl.dao;

import java.util.List;

import com.acl.model.Rol;
import com.acl.model.exception.RolServiceException;
import com.acl.model.filter.FiltroRoles;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface RolDAO {

    public List<Rol> buscarRoles(FiltroRoles filtro);

    public void guardar(Rol rol) throws RolServiceException;

    public List<Rol> getAllRol();

 
}
