package com.acl.dao;

import java.util.List;

import com.acl.model.Contrasena;
import com.acl.model.types.EstadoContrasenaType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface ContrasenaByUsuarioDAO {

    public List<Contrasena> obtenerContrasenasAnterioresPorUsuario(
	    EstadoContrasenaType estado, Long sidUsuario,
	    Integer cantidadAnteriores);

}
