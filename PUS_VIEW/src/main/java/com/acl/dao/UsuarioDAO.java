package com.acl.dao;

import java.util.List;

import com.acl.model.Usuario;
import com.acl.model.exception.UsuarioServiceException;
import com.acl.model.filter.FiltroUsuario;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface UsuarioDAO {

    public void guardar(Usuario usuario) throws UsuarioServiceException;

    public void actualizar(Usuario usuario) throws UsuarioServiceException;

    public List<Usuario> buscarUsuariosPorFiltro(FiltroUsuario filtroUsuario);

}
