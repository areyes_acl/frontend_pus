/**
 * 
 */
package com.acl.dao;

import com.acl.model.Permiso;
import com.acl.model.Rol;
import com.acl.model.RolSubMenu;
import com.acl.model.RolSubMenuPermiso;
import com.acl.model.SubMenu;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface RolSubMenuPermisoDAO {
	public RolSubMenuPermiso searchRolSubMenuPermiso(RolSubMenu rolSubMenu,Permiso permisos,String estado);
    	public int updateRolSubMenuPermisoEstado(RolSubMenuPermiso rolSubMenuPermiso, String estado);
    	public int createRolSubMenuPermiso(RolSubMenuPermiso rolSubmenuPermiso);
}
