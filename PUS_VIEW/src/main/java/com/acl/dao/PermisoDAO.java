/**
 * 
 */
package com.acl.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.acl.model.Permiso;
import com.acl.model.Rol;
import com.acl.model.RolSubMenu;
import com.acl.model.RolSubMenuPermiso;


/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */

public interface PermisoDAO {
    public List<Permiso> getAllPermiso();
    public List<Permiso> getAllPermisoForRol(RolSubMenu rolSubMenu,String estado );
    public Permiso getIdPermiso(String nombre);
}
