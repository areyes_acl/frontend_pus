package com.acl.dao.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.acl.model.Usuario;
import com.acl.model.commons.GlobalConstants;
import com.acl.model.types.EstadoUsuarioType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class UsuarioMapper implements RowMapper<Usuario> {

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rs
     * @param rowNum
     * @return
     * @throws SQLException
     * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
     *      int)
     * @since 1.X
     */
    @Override
    public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
	Usuario usuario = new Usuario();
	usuario.setSid(rs.getLong("SID"));
	usuario.setRut(rs.getInt("RUT"));
	usuario.setDv(rs.getString("DV"));
	usuario.setNombres(rs.getString("NOMBRES"));
	usuario.setApellidoPaterno(rs.getString("APELLIDO_PATERNO"));
	usuario.setApellidoMaterno(rs.getString("APELLIDO_MATERNO"));
	usuario.setEmail(rs.getString("EMAIL"));
	usuario.setFono(rs.getString("FONO") != null ? rs.getString("FONO")
		: GlobalConstants.EMPTY);
	usuario.setEsSuperUsuario(rs.getBoolean("SUPERUSUARIO"));
	usuario.setEstado(EstadoUsuarioType.fromValue(rs.getInt("ESTADO")));

	return usuario;
    }

}