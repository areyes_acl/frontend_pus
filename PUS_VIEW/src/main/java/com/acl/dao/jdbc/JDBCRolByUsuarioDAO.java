/**
 * 
 */
package com.acl.dao.jdbc;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.acl.dao.RolByUsuarioDAO;
import com.acl.dao.jdbc.mappers.RolMapper;
import com.acl.model.Rol;
import com.acl.model.commons.DatabaseConstant;
import com.acl.model.exception.RolByUserException;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Repository(value = "rolByUsuarioDAO")
public class JDBCRolByUsuarioDAO implements RolByUsuarioDAO {

    /**
     * Constante de logger
     */
    private static final Logger logger = Logger
	    .getLogger(JDBCRolByUsuarioDAO.class);

    private static final String CAMPOS = "TROL.SID, TROL.NOMBRE_ROL, TROL.DESCRIPCION, TROL.FECHA_ING, TROL.SID_USR_ING,TROL.FECHA_MOD, TROL.SID_USR_MOD,TROLUS.ESTADO";
    private static final String SQL_ROL_BY_RUT = "SELECT " + CAMPOS + " FROM "
	    + DatabaseConstant.TABLA_ROL_USUARIO + " TROLUS INNER JOIN "
	    + DatabaseConstant.TABLA_USUARIO
	    + " TUSR ON TROLUS.SID_USR = TUSR.SID  "
	    + "INNER JOIN "+DatabaseConstant.TABLA_ROL+" TROL ON TROLUS.SID_ROL = TROL.SID  "
	    + "WHERE TUSR.RUT = :rut";
    private static final String SQL_INSERT_ROLES_BY_SID = "Insert into "
	    + DatabaseConstant.ESQUEMA
	    + "."
	    + DatabaseConstant.TABLA_ROL_USUARIO
	    + " (SID,SID_USR,SID_ROL,FECHA_ROL_ASIGNADO,SID_USR_ASIGNA_ROL,ESTADO) values ("
	    + DatabaseConstant.SECUENCIA_TBL_ROL_USUARIO
	    + ".nextval,:sidUsuario,:sidRol,:fechaRolAsigna,:sidUsuarioAsigna,:estado)";
    private static final String SQL_INSERT_ROLES_BY_RUT = "INSERT INTO "
	    + DatabaseConstant.ESQUEMA
	    + "."
	    + DatabaseConstant.TABLA_ROL_USUARIO
	    + " (SID,SID_USR,SID_ROL,FECHA_ROL_ASIGNADO,SID_USR_ASIGNA_ROL,ESTADO) values ("
	    + DatabaseConstant.SECUENCIA_TBL_ROL_USUARIO
	    + ".nextval,(SELECT SID FROM "
	    + DatabaseConstant.TABLA_USUARIO
	    + " TIG WHERE TIG.RUT= :rut),:sidRol,:fechaRolAsigna,:sidUsuarioAsigna,:estado)";
    private static final String SQL_UPDATE_ROL_BY_SID = "UPDATE "
	    + DatabaseConstant.ESQUEMA
	    + "."
	    + DatabaseConstant.TABLA_ROL_USUARIO
	    + " SET FECHA_ROL_ASIGNADO= :fechaAsignacion,SID_USR_ASIGNA_ROL = :usuarioAsignacion, ESTADO = :estado WHERE SID_ROL = :sidRol AND SID_USR = :sidUsuarioAsignado";

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param sidUsuario
     * @param roles
     * @see com.acl.dao.RolByUsuarioDAO#agregarRolUsuarioBySid(java.lang.Long,
     *      java.util.List)
     * @since 1.X
     */
    @Override
    public void agregarRolesUsuarioBySid(Long sidUsuario, List<Rol> roles) {
	for (Rol rol : roles) {
	    Object[] args = new Object[4];
	    args[0] = sidUsuario;
	    args[1] = rol.getSid();
	    args[2] = rol.getLog().getFechaModificacion();
	    args[3] = rol.getLog().getSidModificacion();
	    logger.debug(rol);

	    jdbcTemplate.update(SQL_INSERT_ROLES_BY_SID, args);

	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param sidUsuario
     * @param roles
     * @throws RolByUserException
     * @see com.acl.dao.RolByUsuarioDAO#agregarRolUsuarioBySid(java.lang.Long,
     *      java.util.List)
     * @since 1.X
     */
    @Override
    public void agregarRolUsuarioBySid(Long sidUsuario, Rol rol)
	    throws RolByUserException {
	try {
	    Object[] args = new Object[5];
	    args[0] = sidUsuario;
	    args[1] = rol.getSid();
	    args[2] = rol.getLog().getFechaModificacion();
	    args[3] = rol.getLog().getSidModificacion();
	    args[4] = rol.getEstado().getValue();
	    logger.info(rol);

	    jdbcTemplate.update(SQL_INSERT_ROLES_BY_SID, args);
	} catch (DataAccessException e) {
	    throw new RolByUserException(e);
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rut
     * @param roles
     * @see com.acl.dao.RolByUsuarioDAO#agregarRolUsuarioByRut(java.lang.String,
     *      java.util.List)
     * @since 1.X
     */
    @Override
    public void agregarRolesUsuarioByRut(Integer rut, List<Rol> roles) {
	for (Rol rol : roles) {
	    Object[] args = new Object[5];
	    args[0] = rut;
	    args[1] = rol.getSid();
	    args[2] = rol.getLog().getFechaModificacion();
	    args[3] = rol.getLog().getSidModificacion();
	    args[4] = rol.getEstado().getValue();
	    

	    jdbcTemplate.update(SQL_INSERT_ROLES_BY_RUT, args);

	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>"INSERT INTO "
	    + DatabaseConstant.ESQUEMA
	    + "."
	    + DatabaseConstant.TABLA_ROL_USUARIO
	    + " (SID,SID_USR,SID_ROL,FECHA_ROL_ASIGNADO,SID_USR_ASIGNA_ROL,ESTADO) values ("
	    + DatabaseConstant.SECUENCIA_TBL_ROL_USUARIO
	    + ".nextval,(SELECT SID FROM "
	    + DatabaseConstant.TABLA_USUARIO
	    + " TIG WHERE TIG.RUT= :rut),:sidRol,:fechaRolAsigna,:sidUsuarioAsigna,:estado)";
     * 
     * @param rut
     * @param roles
     * @see com.acl.dao.RolByUsuarioDAO#agregarRolUsuarioByRut(java.lang.String,
     *      java.util.List)
     * @since 1.X
     */
    @Override
    public void agregarRolUsuarioByRut(Integer rut, Rol rol) {

	Object[] args = new Object[5];
	args[0] = rut;
	args[1] = rol.getSid();
	args[2] = rol.getLog().getFechaModificacion();
	args[3] = rol.getLog().getSidModificacion();
	args[4] = rol.getEstado().getValue();

	jdbcTemplate.update(SQL_INSERT_ROLES_BY_RUT, args);
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rut
     * @return
     * @see com.acl.dao.RolByUsuarioDAO#obtenerRolesPorRutUsuario(java.lang.Integer)
     * @since 1.X
     */
    @Override
    public List<Rol> obtenerRolesPorRutUsuario(Integer rut) {
	List<Rol> usuarios = jdbcTemplate.query(SQL_ROL_BY_RUT,
		new Object[] { rut }, new RolMapper());
	return usuarios;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param sidUsuario
     * @param rol
     * @throws RolByUserException
     * @see com.acl.dao.RolByUsuarioDAO#desactivarRolUsuario(java.lang.Long,
     *      com.acl.model.Rol)
     * @since 1.X
     */
    @Override
    public void actualizarRolUsuario(Long sidUsuario, Rol rol)
	    throws RolByUserException {
	try {
	    Object[] args = new Object[5];
	    args[0] = rol.getLog().getFechaModificacion();
	    args[1] = rol.getLog().getSidModificacion();
	    args[2] = rol.getEstado().getValue();
	    args[3] = rol.getSid();
	    args[4] = sidUsuario;
	    jdbcTemplate.update(SQL_UPDATE_ROL_BY_SID, args);
	} catch (DataAccessException e) {
	    throw new RolByUserException(e);
	}
    }

}
