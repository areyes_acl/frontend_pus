/**
 * 
 */
package com.acl.dao.jdbc;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.acl.dao.ContrasenaByUsuarioDAO;
import com.acl.dao.jdbc.mappers.ContrasenaMapper;
import com.acl.model.Contrasena;
import com.acl.model.types.EstadoContrasenaType;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Repository(value = "contrasenaByUsuarioDAO")
public class JDBCContrasenaByUsuarioDAO implements ContrasenaByUsuarioDAO {

    /**
     * Constante de logger
     */
    private static final Logger logger = Logger
	    .getLogger(JDBCContrasenaByUsuarioDAO.class);

    private static final String CAMPOS = "TC.SID AS SID, TC.SID_USR AS SID_USUARIO, TC.CONTRASENA AS CONTRASENA, TC.FECHA_VENCIMIENTO AS FEC_VENC, TE.SID AS SID_ESTADO, TE.DESCRIPCION AS DESCP, TE.ESTADO AS ESTADO";
    private static final String CAMPOS_ALIAS = "SID,  SID_USUARIO, CONTRASENA, FEC_VENC, SID_ESTADO, DESCP, ESTADO";
    private static final String SQL_ALL = "SELECT "
	    + CAMPOS
	    + " FROM SIG.TBL_CONTRASENA  TC INNER JOIN TBL_CONTRASENA_ESTADO TE  ON TC.SID_CONTRASENA_ESTADO = TE.SID";

    private static final String SQL_OBTENER_ULTIMAS_N_CONTRASENAS = SQL_ALL
	    + " WHERE TC.SID_CONTRASENA_ESTADO = :estado AND ROWNUM <= :cantidad AND TC.SID_USR = :sidUsuario ORDER BY TC.FECHA_VENCIMIENTO DESC";

    private static final String SQL_OBTENER_ULTIMAS_N_PASS = "SELECT "
	    + CAMPOS_ALIAS
	    + " FROM"
	    + " (SELECT "
	    + CAMPOS
	    + " FROM SIG.TBL_CONTRASENA  TC "
	    + " INNER JOIN TBL_CONTRASENA_ESTADO TE  ON TC.SID_CONTRASENA_ESTADO = TE.SID WHERE TC.SID_CONTRASENA_ESTADO IN (:estado) AND TC.SID_USR = :sidUsuario ORDER BY TC.SID DESC)"
	    + " WHERE ROWNUM <= :cantidad";

    private static final String SP_SIG_GUARDAR_CONSTRASENA = "SP_SIG_GUARDAR_CONSTRASENA";
    private static final String ESQUEMA = "SIG";

    @Autowired
    JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
	return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * busca las ultimas n contraseñas dado un estado 1:ESTADO INACTIVO 2:
     * ESTADO ACTIVO 3:ESTADO PENDIENTE ACTIVACION
     * 
     * @param estado
     * @param sidUsuario
     * @param cantidadAnteriores
     * @return
     * @see com.acl.dao.ContrasenaByUsuarioDAO#obtenerContrasenasAnterioresPorUsuario(com.acl.model.types.EstadoContrasenaType,
     *      java.lang.Long, java.lang.Integer)
     * @since 1.X
     */
    @Override
    public List<Contrasena> obtenerContrasenasAnterioresPorUsuario(
	    EstadoContrasenaType estado, Long sidUsuario,
	    Integer cantidadAnteriores) {
	String estados = "";

	if (estado == null) {
	    estados = EstadoContrasenaType.ACTIVA.getSid() + ","
		    + EstadoContrasenaType.INACTIVA.getSid();
	} else {
	    estados = estados + estado.getSid();
	}

	String sql = SQL_OBTENER_ULTIMAS_N_PASS
		.replace(":estado", "" + estados)
		.replace(":sidUsuario", "" + sidUsuario)
		.replace(":cantidad", "" + cantidadAnteriores);
	logger.info("Contraseñas : "+ sql);

	return jdbcTemplate.query(sql, new ContrasenaMapper());
    }

}
