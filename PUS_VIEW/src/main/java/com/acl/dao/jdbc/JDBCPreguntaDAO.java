/**
 * 
 */
package com.acl.dao.jdbc;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.acl.dao.PreguntaDAO;
import com.acl.dao.jdbc.mappers.PreguntaMapper;
import com.acl.model.Pregunta;
import com.acl.model.commons.DatabaseConstant;
import com.acl.model.commons.GlobalConstants;
import com.acl.model.exception.PreguntaServiceException;
import com.acl.model.filter.FiltroPregunta;
import com.acl.model.types.AccionType;
import com.acl.model.types.EstadoPreguntaType;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX</YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Repository(value = "preguntaDAO")
public class JDBCPreguntaDAO implements PreguntaDAO, Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = 1L;
    /**
     * Constante de logger
     */
    private static final Logger logger = Logger
	    .getLogger(JDBCPreguntaDAO.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SQL_ALL = "SELECT SID, PREGUNTA, FECHA_ING, FECHA_MOD, SID_USR_ING, SID_USR_MOD, ESTADO FROM :esquema.:tabla WHERE 1=1 ";
    private static final String SQL_INSERT = "INSERT INTO :esquema.:tabla (SID, PREGUNTA, FECHA_ING, FECHA_MOD, SID_USR_ING, SID_USR_MOD, ESTADO) values (:esquema.:secuencia.nextval, ?, SYSDATE, null, ?, null, ?)";
    private static final String SQL_UPDATE = "UPDATE :esquema.:tabla SET PREGUNTA= ? , FECHA_MOD = SYSDATE, SID_USR_MOD = ?, ESTADO = ? WHERE SID = ?";

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param jdbcTemplate
     * @since 1.X
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see com.acl.dao.PreguntaDAO#buscarPreguntas()
     * @since 1.X
     */
    @Override
    public List<Pregunta> buscarPreguntas(FiltroPregunta filtro) {
	String sql = GlobalConstants.EMPTY
		+ preparaConsulta(AccionType.CONSULTA);

	try {

	    if (filtro.getSid() != null) {
		sql = sql.concat(" AND SID = ':d'");
		sql = sql.replace(":d", String.valueOf(filtro.getSid()));
	    }

	    if (filtro.getPregunta() != null
		    && !(GlobalConstants.EMPTY.equalsIgnoreCase(filtro
			    .getPregunta().trim()))) {
		sql = sql
			.concat(" AND UPPER(translate(PREGUNTA, 'áéíóúÁÉÍÓÚ', 'aeiouAEIOU')) LIKE UPPER(translate('%:d%', 'áéíóúÁÉÍÓÚ', 'aeiouAEIOU'))");

		sql = sql.replace(":d", filtro.getPregunta());
	    }
	    if (filtro.getEstado() != null
		    && !EstadoPreguntaType.TODOS.equals(filtro.getEstado())) {
		sql = sql.concat(" AND ESTADO = ':d'");
		sql = sql.replace(":d",
			String.valueOf(filtro.getEstado().getValue()));
	    }

	    logger.info(sql);
	    return jdbcTemplate.query(sql, new PreguntaMapper());
	} catch (EmptyResultDataAccessException e) {
	    return null;
	}
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que realiza el bind de las tablas y el esquema en las tablas
     * seleccionadas
     * 
     * @param consulta
     * @return
     * @since 1.X
     */
    private String preparaConsulta(AccionType consulta) {

	switch (consulta) {
	case CONSULTA:

	    return SQL_ALL.replace(":esquema", DatabaseConstant.ESQUEMA)
		    .replace(":tabla", DatabaseConstant.TABLA_PREGUNTA);

	case NUEVO:
	    return SQL_INSERT
		    .replace(":esquema", DatabaseConstant.ESQUEMA)
		    .replace(":tabla", DatabaseConstant.TABLA_PREGUNTA)
		    .replace(":secuencia",
			    DatabaseConstant.SECUENCIA_TBL_PREGUNTAS);

	case ACTUALIZACION:
	    return SQL_UPDATE.replace(":esquema", DatabaseConstant.ESQUEMA)
		    .replace(":tabla", DatabaseConstant.TABLA_PREGUNTA);

	default:
	    break;
	}
	return null;

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param pregunta
     * @throws PreguntaServiceException
     * @see com.acl.dao.PreguntaDAO#guardar(com.acl.model.Pregunta)
     * @since 1.X
     */
    @Override
    public void guardar(Pregunta pregunta) throws PreguntaServiceException {
	String query = preparaConsulta(AccionType.NUEVO);

	logger.info(query);
	try {
	    Object[] params = new Object[3];
	    params[0] = pregunta.getPregunta();
	    params[1] = pregunta.getLog().getSidCreacion();
	    params[2] = pregunta.getEstado().getValue();
	    jdbcTemplate.update(query, params);
	} catch (DataAccessException e) {
	    throw new PreguntaServiceException(
		    "Ha ocurrido un error",
		    "No se ha podido incoporar la nueva pregunta, debido a que puede estar duplicada o se produjo algun error en la base de datos, consultar logs.",
		    e);

	} catch (Exception e) {
	    throw new PreguntaServiceException(e.getMessage(), e);

	}
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * "UPDATE :esquema.:tabla SET PREGUNTA=:pregunta, FECHA_MOD = SYSDATE, SID_USR_MOD = :sidUserMod, ESTADO : estado) WHERE SID = :sid"
     * 
     * @param pregunta
     * @throws PreguntaServiceException
     * @see com.acl.dao.PreguntaDAO#actualizar(com.acl.model.Pregunta)
     * @since 1.X
     */

    @Override
    public void actualizar(Pregunta pregunta) throws PreguntaServiceException {
	String query = preparaConsulta(AccionType.ACTUALIZACION);
	// query = query
	// .replace(":pregunta", pregunta.getPregunta())
	// .replace(
	// ":fechaMod",
	// String.valueOf(pregunta.getLog().getFechaModificacion()))
	// .replace(":sidUserMod",
	// String.valueOf(pregunta.getLog().getSidModificacion()))
	// .replace(":estado",
	// String.valueOf(pregunta.getEstado().getValue()))
	// .replace(":sid", String.valueOf(pregunta.getSid()));

	logger.info(query);
	try {
	    // Object[] params = new Object[5];
	    // params[0] = pregunta.getPregunta();
	    // params[1] = pregunta.getLog().getSidModificacion();
	    // params[2] = pregunta.getEstado().getValue();
	    // params[3] = pregunta.getSid();
	    jdbcTemplate.update(query, new Object[] { pregunta.getPregunta(),
		    pregunta.getLog().getSidModificacion(),
		    pregunta.getEstado().getValue(), pregunta.getSid() });
	} catch (DataAccessException e) {
	    throw new PreguntaServiceException(
		    "Ha ocurrido un error",
		    "No se ha podido editar la nueva pregunta, debido a que puede estar duplicada o se produjo algun error en la base de datos, consultar logs.",
		    e);

	} catch (Exception e) {
	    throw new PreguntaServiceException(e.getMessage(), e);
	}
    }

}