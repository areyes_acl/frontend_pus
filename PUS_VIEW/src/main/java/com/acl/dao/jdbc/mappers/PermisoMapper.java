/**
 * 
 */
package com.acl.dao.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.acl.model.Permiso;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class PermisoMapper implements  RowMapper<Object> {

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     *   
     * @param rs
     * @param rowNum
     * @return
     * @throws SQLException
     * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
     * @since 1.X
     */
    @Override
    public ArrayList<Permiso> mapRow(ResultSet rs, int rowNum) throws SQLException {
	// TODO Auto-generated method stub
	ArrayList<Permiso> permisoList = new ArrayList<Permiso>();
	while (rs.next()) {
        	Permiso permiso = new Permiso();
        	permiso.setSid(rs.getLong("SID"));
        	permiso.setPermiso(rs.getString("TIPO_PERMISO"));
        	permiso.setDescripcion(rs.getString("DESCRIPCION"));

        	permisoList.add(permiso);
	}
	return permisoList;
    }

}
