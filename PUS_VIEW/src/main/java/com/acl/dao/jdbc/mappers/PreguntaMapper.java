package com.acl.dao.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.acl.model.Pregunta;
import com.acl.model.types.EstadoPreguntaType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class PreguntaMapper implements RowMapper<Pregunta> {

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rs
     * @param rowNum
     * @return
     * @throws SQLException
     * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
     *      int)
     * @since 1.X
     */
    @Override
    public Pregunta mapRow(ResultSet rs, int rowNum) throws SQLException {
	Pregunta pregunta = new Pregunta();
	pregunta.setSid(rs.getLong("SID"));
	pregunta.setPregunta(rs.getString("PREGUNTA"));
	pregunta.getLog().setFechaCreacion(rs.getDate("FECHA_ING"));
	pregunta.getLog().setFechaModificacion(rs.getDate("FECHA_MOD"));
	pregunta.getLog().setSidModificacion(rs.getLong("SID_USR_ING"));
	pregunta.getLog().setSidModificacion(rs.getLong("SID_USR_MOD"));
	pregunta.setEstado(EstadoPreguntaType.fromValue(rs.getInt("ESTADO")));

	return pregunta;
    }

}