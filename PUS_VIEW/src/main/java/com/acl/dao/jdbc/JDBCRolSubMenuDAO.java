/**
 * 
 */
package com.acl.dao.jdbc;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.acl.dao.RolSubMenuDAO;
import com.acl.dao.jdbc.mappers.RolMapper;
import com.acl.dao.jdbc.mappers.RolSubMenuMapper;
import com.acl.model.Permiso;
import com.acl.model.Rol;
import com.acl.model.RolSubMenu;
import com.acl.model.SubMenu;
import com.acl.model.exception.PreguntaServiceException;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Repository(value = "rolSubMenuDAO")
public class JDBCRolSubMenuDAO implements RolSubMenuDAO {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(JDBCRolSubMenuDAO.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
    
   final  String ROLSUBMENU="select SID,SID_ROL,SID_SUB_MENU, ESTADO from TBL_ROL_SUBMENU WHERE SID_ROL=?1 AND SID_SUB_MENU=?2 AND ESTADO=?3";
   final  String UPDATEESTADO="update TBL_ROL_SUBMENU set ESTADO=? WHERE SID=?";
   final  String INSERTROLSUBMENU="INSERT INTO  SIG.TBL_ROL_SUBMENU (SID, SID_ROL,SID_SUB_MENU, ESTADO) values (?, ?,?,?)";
   final String NEXTVAL="select SIG.SEQ_TBL_ROL_MENU.nextval from dual";
    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     *   busca rolSubmenu por criterio de rol y submenu id. 
     * @param rol
     * @param subMenu
     * @return
   
     * @since 1.X
     */
    @Override
    public List<RolSubMenu> searchRolSubMenu(Rol rol, SubMenu subMenu) {



	    String ROLSUBMENU2=ROLSUBMENU;

	    ROLSUBMENU2=ROLSUBMENU2.replace("?1",rol.getSid().toString());
	    ROLSUBMENU2=ROLSUBMENU2.replace("?2",subMenu.getSid().toString());
	    ROLSUBMENU2=ROLSUBMENU2.replace("?3","1");
	List<RolSubMenu> permisoList = new ArrayList<RolSubMenu>();
	
	List<Map<String, Object>> rows = getJdbcTemplate().queryForList(ROLSUBMENU2);
	
	for (Map row : rows) {
	   
	    RolSubMenu permiso = new RolSubMenu();
	   
	    	BigDecimal bg=(BigDecimal) row.get("SID");
	    	permiso.setSid(bg.longValue());
	    	Rol rol1 = new Rol();
	    	BigDecimal bg2=(BigDecimal) row.get("SID_ROL");
	    	
	    	rol1.setSid(bg2.longValue());
	    	permiso.setRol(rol1);
	    	SubMenu submenu = new SubMenu();
	    	BigDecimal bg3=(BigDecimal) row.get("SID_SUB_MENU");
	    	
	    	submenu.setSid(bg3.longValue());
	    	permiso.setSubmenu(submenu);
	    	BigDecimal bg4=(BigDecimal) row.get("ESTADO");
	    	permiso.setEstado(bg4.longValue());
	    	permisoList.add(permiso);
	}

	return permisoList;
    }
    

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     *   
     * @param idRolSubMenu
     * @param estado
     * @return
     * @see com.acl.dao.RolSubMenuDAO#updateSubmenuEstado(int, java.lang.String)
     * @since 1.X
     */
    @Override
    public int updateSubmenuEstado(RolSubMenu idRolSubMenu, String estado) {
	// TODO Auto-generated method stub
	
	  return jdbcTemplate.update(UPDATEESTADO, estado, idRolSubMenu.getSid() );
	
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     *   
     * @param rolSubmenu
     * @return
     * @see com.acl.dao.RolSubMenuDAO#createRolSubMenu(com.acl.model.RolSubMenu)
     * @since 1.X
     */
    @Override
    public Long createRolSubMenu(RolSubMenu rolSubmenu) {
	// TODO Auto-generated method stub
	
	try {
	    Integer nextval=this.jdbcTemplate.queryForObject(
		        NEXTVAL,
		        new RowMapper<Integer>() {
		            public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
		                return Integer.parseInt(rs.getString("NEXTVAL"));
		            }
		        });
	
	   
	    Object[] params = new Object[4];
	    params[0] = nextval;
	    params[1] = rolSubmenu.getRol().getSid();
	    params[2] = rolSubmenu.getSubmenu().getSid();
	    params[3] = rolSubmenu.getEstado();
	    jdbcTemplate.update(INSERTROLSUBMENU, params);
	    return nextval.longValue();
	} catch (Exception e) {
	    logger.error(e.getMessage(), e);
	   
	}
	return 0L;
    }


    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }


    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }





}
