/**
 * 
 */
package com.acl.dao.jdbc.mappers;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.acl.dao.RolSubMenuDAO;
import com.acl.model.Permiso;
import com.acl.model.Rol;
import com.acl.model.RolSubMenu;
import com.acl.model.SubMenu;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class RolSubMenuMapper implements RowMapper<RolSubMenu>{

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     *   
     * @param rs
     * @param rowNum
     * @return
     * @throws SQLException
     * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
     * @since 1.X
     */
    @Override
    public RolSubMenu mapRow(ResultSet rs, int rowNum) throws SQLException {
	// TODO Auto-generated method stub
	RolSubMenu obj = new RolSubMenu();
	Rol rol = new Rol ();
	SubMenu sub = new SubMenu();
	obj.setEstado((Long)rs.getLong("ESTADO"));
	rol.setSid((Long)rs.getLong("SID_ROL"));
	obj.setRol(rol );
	sub.setSid((Long)rs.getLong("SID_SUB_MENU"));
	obj.setSubmenu(sub);
	obj.setSid((Long)rs.getLong("SID"));
	
	return obj;

    }


 

}
