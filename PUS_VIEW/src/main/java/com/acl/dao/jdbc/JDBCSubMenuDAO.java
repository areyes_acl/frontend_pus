/**
 * 
 */
package com.acl.dao.jdbc;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.acl.dao.SubMenuDAO;
import com.acl.model.Menu;
import com.acl.model.Permiso;
import com.acl.model.Rol;
import com.acl.model.SubMenu;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Repository(value = "subMenuDAO")
public class JDBCSubMenuDAO implements SubMenuDAO , Serializable{
    
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(JDBCSubMenuDAO.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
    String SUBMENU="select TBL_SUB_MENU.SID,TBL_SUB_MENU.NOMBRE_SUB_MENU,TBL_SUB_MENU.DESCRIPCION,TBL_MENU.NOMBRE_MENU from TBL_SUB_MENU inner join TBL_MENU on TBL_SUB_MENU.SID_MENU=TBL_MENU.SID";

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     *   
     * @return
     * @see com.acl.dao.SubMenuDAO#getAllSubMenu()
     * @since 1.X
     */
    @Override
    public List<SubMenu> getAllSubMenu() {
	// TODO Auto-generated method stub
	
//	return jdbcTemplate.queryForList(SUBMENU, SubMenu.class );
	List<SubMenu> subMenuList = new ArrayList<SubMenu>();

	List<Map<String, Object>> rows = getJdbcTemplate().queryForList(SUBMENU);
	for (Map row : rows) {
	    	SubMenu subMenu = new SubMenu();
	    	Menu m = new Menu();
	    	
	    	BigDecimal bg=(BigDecimal) row.get("SID");
	    	subMenu.setSid(bg.longValue());
	    	subMenu.setNombre((String)row.get("NOMBRE_SUB_MENU"));
	    	subMenu.setDescripcion((String)row.get("DESCRIPCION"));
	    	m.setNombre((String)row.get("NOMBRE_MENU"));
	    	subMenu.setMenu(m);
	    	subMenuList.add(subMenu);
	}

	return subMenuList;
    }

    
 
    
    
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public SimpleJdbcCall getSimpleJdbcCall() {
        return simpleJdbcCall;
    }

    public void setSimpleJdbcCall(SimpleJdbcCall simpleJdbcCall) {
        this.simpleJdbcCall = simpleJdbcCall;
    }

    public String getSUBMENU() {
        return SUBMENU;
    }

    public void setSUBMENU(String sUBMENU) {
        SUBMENU = sUBMENU;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public static Logger getLogger() {
        return logger;
    }

}
