/**
 * 
 */
package com.acl.dao.jdbc;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.acl.dao.ParametrosDAO;
import com.acl.dao.jdbc.mappers.ParametrosMapper;
import com.acl.model.Parametros;
import com.acl.model.filter.FiltroParametro;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Repository(value = "parametrosDAO")
public class JDBCParametrosDAO implements ParametrosDAO, Serializable {

   
    /**
     * 
     */
    private static final long serialVersionUID = -7342827324003610303L;

    /**
     * Constante de logger
     */
    private static final Logger logger = Logger
	    .getLogger(JDBCParametrosDAO.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SQL_ALL = "SELECT SID, NOMBRE_PARAMETRO, VALOR, FECHA_INGRESO, REF_SID_USR_ING, FECHA_MODIFICACION, REF_SID_USR_MOD, ESTADO FROM SIG.TBL_POLITICAS_USUARIO_PRTS WHERE 1=1 ";
    private final String updateParametro=new String("update TBL_POLITICAS_USUARIO_PRTS SET VALOR = ? WHERE SID=?");
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see com.acl.dao.ParametrosDAO#obtenerTodos()
     * @since 1.X
     */
    @Override
    public List<Parametros> buscarPorFiltro(FiltroParametro filtro) {
	String query = SQL_ALL;
	try {
	    if (filtro != null && filtro.getNombre() != null
		    && !(filtro.getNombre().trim().equalsIgnoreCase(""))) {
		query = query.concat(" AND  NOMBRE_PARAMETRO = :nombre");
		query = query.replace(":nombre", filtro.getNombre());
	    }

	    if (filtro != null && filtro.getEstado() != null) {
		query = query.concat(" AND  ESTADO = :estado");
		query = query.replace(":estado", "" + filtro.getEstado().getValue());
	    }
	    logger.info(query);
	    return jdbcTemplate.query(query, new ParametrosMapper());
	} catch (EmptyResultDataAccessException e) {
	    return null;
	}
    }

    public int update(Parametros parametros){
	if (parametros!=null){

	    return jdbcTemplate.update(updateParametro,parametros.getValor(),parametros.getSid());
	    
	}
	return 0;
    }
}
