package com.acl.dao.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.acl.model.Contrasena;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ContrasenaMapper implements RowMapper<Contrasena> {

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rs
     * @param rowNum
     * @return
     * @throws SQLException
     * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
     *      int)
     * @since 1.X
     */
    @Override
    public Contrasena mapRow(ResultSet rs, int rowNum) throws SQLException {
	Contrasena contrasena = new Contrasena();
	contrasena.setSid(rs.getLong("SID"));
	contrasena.setPassword(rs.getString("CONTRASENA"));
	contrasena.setFechaVencimiento(rs.getDate("FEC_VENC"));
	contrasena.getEstado().setSid(rs.getLong("SID_ESTADO"));
	contrasena.getEstado().setDescripcion(rs.getString("DESCP"));
	contrasena.getEstado().setEstado(rs.getBoolean("ESTADO"));

	return contrasena;
    }

}