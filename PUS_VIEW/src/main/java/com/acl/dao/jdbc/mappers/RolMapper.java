package com.acl.dao.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.acl.model.Rol;
import com.acl.model.types.EstadoRolType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class RolMapper implements RowMapper<Rol> {

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rs
     * @param rowNum
     * @return
     * @throws SQLException
     * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
     *      int)
     * @since 1.X
     */
    @Override
    public Rol mapRow(ResultSet rs, int rowNum) throws SQLException {
	Rol rol = new Rol();

	rol.setSid(rs.getLong("SID"));
	rol.setNombre(rs.getString("NOMBRE_ROL"));
	rol.setDescripcion(rs.getString("DESCRIPCION"));
	rol.getLog().setFechaCreacion(rs.getDate("FECHA_ING"));
	rol.getLog().setSidCreacion(rs.getLong("SID_USR_ING"));
	rol.getLog().setFechaModificacion(rs.getDate("FECHA_MOD"));
	rol.getLog().setSidModificacion(rs.getLong("SID_USR_MOD"));
	rol.setEstado(EstadoRolType.fromValue(rs.getInt("ESTADO")));
	return rol;
    }

}