/**
 * 
 */
package com.acl.dao.jdbc;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.acl.dao.UsuarioDAO;
import com.acl.dao.jdbc.mappers.UsuarioMapper;
import com.acl.model.Usuario;
import com.acl.model.commons.DatabaseConstant;
import com.acl.model.exception.UsuarioServiceException;
import com.acl.model.filter.FiltroUsuario;
import com.acl.model.types.EstadoUsuarioType;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * Os <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Repository(value = "usuarioDAO")
public class JDBCUsuarioDAO implements UsuarioDAO {
    /**
     * Constante de logger
     */
    private static final Logger logger = Logger.getLogger(JDBCUsuarioDAO.class);

    /** QUERYS */
    private static final String CAMPOS = "SID AS SID, RUT AS RUT, DV AS DV, NOMBRE AS NOMBRES, APEPAT AS APELLIDO_PATERNO, APEMAT AS APELLIDO_MATERNO, EMAIL AS EMAIL, FONO AS FONO, SU AS SUPERUSUARIO, ESTADO AS ESTADO";
    private static final String SQL_ALL_USERS = "SELECT " + CAMPOS
	    + " FROM SIG."+DatabaseConstant.TABLA_USUARIO+" WHERE 1=1 ";
    private static final String SP_GESTIONAR_USUARIO = "SP_SIG_GESTIONAR_USUARIO";
    private static final String SCHEMA = "SIG";

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo encargado de llamar a un SP que guardar al usuario
     * 
     * @param usuario
     * @throws UserServiceException
     * @see com.acl.dao.UsuarioDAO#guardar(com.acl.model.Usuario)
     * @since 1.X
     */
    @Override
    public void guardar(Usuario usuario) throws UsuarioServiceException {
	
	logger.info(usuario);

	try {
	    SimpleJdbcCall scall = new SimpleJdbcCall(jdbcTemplate);
	    scall.withSchemaName(SCHEMA);
	    scall.withProcedureName(SP_GESTIONAR_USUARIO);

	    Map<String, Object> inParamMap = new HashMap<String, Object>();
	    inParamMap.put("p_sid", usuario.getSid());
	    inParamMap.put("p_rut", usuario.getRut());
	    inParamMap.put("p_dv", usuario.getDv());
	    inParamMap.put("p_nombre", usuario.getNombres());
	    inParamMap.put("p_apellido_paterno", usuario.getApellidoPaterno());
	    inParamMap.put("p_apellido_materno", usuario.getApellidoMaterno());
	    inParamMap.put("p_email", usuario.getEmail());
	    inParamMap.put("p_fono", usuario.getFono());
	    inParamMap.put("p_super_usuario", usuario.getEsSuperUsuario());
	    inParamMap.put("p_estado", usuario.getEstado().getValue());
	    inParamMap.put("p_contrasena", usuario.getContrasena()
		    .getPassword());
	    inParamMap.put("p_sid_usr_creacion", usuario.getLog()
		    .getSidCreacion());

	    SqlParameterSource in = new MapSqlParameterSource(inParamMap);

	    Map<String, Object> simpleJdbcCallResult = scall.execute(in);

	    Integer codigoError = ((BigDecimal) simpleJdbcCallResult
		    .get("COD_ERROR")).intValue();
	    String warning = (String) simpleJdbcCallResult.get("WARNING");

	    logger.info(simpleJdbcCallResult);
	    if (codigoError != 0) {
		throw new UsuarioServiceException(warning);
	    }
	} catch (Exception e) {
	    throw new UsuarioServiceException(
		    "Ha ocurrido un error al intentar guardar el usuario.", e);
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param filtroUsuario
     * @return
     * @see com.acl.dao.UsuarioDAO#buscarUsuariosPorFiltro(com.acl.model.filter.UserFilter)
     * @since 1.X
     */
    @Override
    public List<Usuario> buscarUsuariosPorFiltro(FiltroUsuario filtroUsuario) {

	String query = SQL_ALL_USERS;

	if (filtroUsuario != null && filtroUsuario.getDv() != null
		&& filtroUsuario.getRut() != null
		&& !"K".equalsIgnoreCase(filtroUsuario.getDv())) {
	    query = query.concat(" AND  RUT = :rut AND DV = :dv");
	    query = query.replace(":rut",
		    String.valueOf(filtroUsuario.getRut()));
	    query = query.replace(":dv", String.valueOf(filtroUsuario.getDv()));

	} else if (filtroUsuario != null && filtroUsuario.getDv() != null
		&& filtroUsuario.getRut() != null
		&& "K".equalsIgnoreCase(filtroUsuario.getDv())) {
	    query = query.concat(" AND  RUT = :rut AND DV = ':dv'");
	    query = query.replace(":rut",
		    String.valueOf(filtroUsuario.getRut()));
	    query = query.replace(":dv", String.valueOf(filtroUsuario.getDv().toUpperCase()));
	}

	if (filtroUsuario != null && filtroUsuario.getEstado() != null
		&& !EstadoUsuarioType.TODOS.equals(filtroUsuario.getEstado())) {
	    query = query.concat(" AND  ESTADO = :estado");
	    query = query.replace(":estado",
		    String.valueOf(filtroUsuario.getEstado().getValue()));
	}
	logger.info(query);
	return jdbcTemplate.query(query, new UsuarioMapper());

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param usuario
     * @throws UsuarioServiceException
     * @see com.acl.dao.UsuarioDAO#actualizar(com.acl.model.Usuario)
     * @since 1.X
     */
    @Override
    public void actualizar(Usuario usuario) throws UsuarioServiceException {
	SimpleJdbcCall scall = new SimpleJdbcCall(jdbcTemplate);
	scall.withSchemaName(SCHEMA);
	scall.withProcedureName(SP_GESTIONAR_USUARIO);

	Map<String, Object> inParamMap = new HashMap<String, Object>();
	inParamMap.put("p_sid", usuario.getSid());
	inParamMap.put("p_rut", usuario.getRut());
	inParamMap.put("p_dv", usuario.getDv());
	inParamMap.put("p_nombre", usuario.getNombres());
	inParamMap.put("p_apellido_paterno", usuario.getApellidoPaterno());
	inParamMap.put("p_apellido_materno", usuario.getApellidoMaterno());
	inParamMap.put("p_email", usuario.getEmail());
	inParamMap.put("p_fono", usuario.getFono());
	inParamMap.put("p_super_usuario", usuario.getEsSuperUsuario());
	inParamMap.put("p_estado", usuario.getEstado().getValue());
	inParamMap.put("p_contrasena", usuario.getContrasena().getPassword());
	inParamMap.put("p_sid_usr_creacion", usuario.getLog()
		.getSidModificacion());

	SqlParameterSource in = new MapSqlParameterSource(inParamMap);

	Map<String, Object> simpleJdbcCallResult = scall.execute(in);
	Integer codigoError = ((BigDecimal) simpleJdbcCallResult
		.get("COD_ERROR")).intValue();
	String warning = (String) simpleJdbcCallResult.get("WARNING");

	logger.info(simpleJdbcCallResult);
	if (codigoError != 0) {
	    throw new UsuarioServiceException(warning);
	}

    }

}
