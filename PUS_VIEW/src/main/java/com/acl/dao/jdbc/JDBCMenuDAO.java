/**
 * 
 */
package com.acl.dao.jdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.acl.dao.MenuDAO;
import com.acl.dao.jdbc.mappers.MenuMapper;
import com.acl.model.Menu;
import com.acl.model.commons.DatabaseConstant;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Repository(value = "menuDAO")
public class JDBCMenuDAO implements MenuDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SQL_ALL = "SELECT SID, NOMBRE_MENU, DESCRIPCION, POSICION FROM "
	    .concat(DatabaseConstant.ESQUEMA).concat(".")
	    .concat(DatabaseConstant.TABLA_MENU);

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see com.acl.dao.MenuDAO#obtenerTodosLosMenus()
     * @since 1.X
     */
    @Override
    public List<Menu> obtenerTodosLosMenus() {
	try {
	    return jdbcTemplate.query(SQL_ALL, new MenuMapper());
	} catch (EmptyResultDataAccessException e) {
	    return null;
	}
    }

}
