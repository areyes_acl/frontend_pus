/**
 * 
 */
package com.acl.dao.jdbc;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.acl.dao.RolSubMenuDAO;
import com.acl.dao.RolSubMenuPermisoDAO;
import com.acl.dao.jdbc.mappers.RolSubMenuMapper;
import com.acl.dao.jdbc.mappers.RolSubMenuPermisoMapper;
import com.acl.model.Permiso;
import com.acl.model.RolSubMenu;
import com.acl.model.RolSubMenuPermiso;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Repository(value = "rolSubMenuPermisoDAO")
public class JDBCRolSubMenuPermisoDAO implements RolSubMenuPermisoDAO {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger
	    .getLogger(JDBCRolSubMenuPermisoDAO.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

    final String SUBMENUPERMISO = "select SID, SID_ROL_SUB_MENU,SID_TIPO_PERMISO, ESTADO from TBL_ROL_MENU_TIPO_PERMISO WHERE SID_ROL_SUB_MENU=?1 AND SID_TIPO_PERMISO=?2 AND ESTADO=?3";
    final String UPDATEESTADO = "update TBL_ROL_MENU_TIPO_PERMISO set ESTADO=? WHERE SID=?";
    final String INSERTSUBMENUPERMISO = "INSERT INTO SIG.TBL_ROL_MENU_TIPO_PERMISO (SID, SID_ROL_SUB_MENU,SID_TIPO_PERMISO, ESTADO) values (SIG.SEQ_TBL_ROL_MENU_TIPO_PERMISO.nextval, ?,?,?)";

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rolSubMenu
     * @param permisos
     * @return
     * @see com.acl.dao.RolSubMenuPermisoDAO#searchRolSubMenuPermiso(com.acl.dao.RolSubMenuDAO,
     *      com.acl.model.Permiso)
     * @since 1.X
     */
    @Override
    public RolSubMenuPermiso searchRolSubMenuPermiso(RolSubMenu rolSubMenu,
	    Permiso permisos, String estado) {
	// TODO Auto-generated method stub
	try {
	    String sql = SUBMENUPERMISO;
	    sql = sql.replace("?1", rolSubMenu.getSid().toString());
	    sql = sql.replace("?2", permisos.getSid().toString());
	    sql = sql.replace("?3", estado);
	    logger.debug(sql);
	    return (RolSubMenuPermiso) jdbcTemplate.queryForObject(sql,
		    new RolSubMenuPermisoMapper());
	} catch (EmptyResultDataAccessException e) {
	    return null;
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rolSubMenuPermiso
     * @param estado
     * @return
     * @see com.acl.dao.RolSubMenuPermisoDAO#updateRolSubMenuPermisoEstado(int,
     *      java.lang.String)
     * @since 1.X
     */
    @Override
    public int updateRolSubMenuPermisoEstado(
	    RolSubMenuPermiso rolSubMenuPermiso, String estado) {
	// TODO Auto-generated method stub
	return jdbcTemplate.update(UPDATEESTADO, estado,
		rolSubMenuPermiso.getSid());
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rolSubmenuPermiso
     * @return
     * @see com.acl.dao.RolSubMenuPermisoDAO#createRolSubMenuPermiso(com.acl.dao.RolSubMenuPermisoDAO)
     * @since 1.X
     */
    @Override
    public int createRolSubMenuPermiso(RolSubMenuPermiso rolSubmenuPermiso) {
	// TODO Auto-generated method stub

	try {
	    Object[] params = new Object[3];
	    params[0] = rolSubmenuPermiso.getRolSubMenu().getSid();
	    params[1] = rolSubmenuPermiso.getPermiso().getSid();
	    params[2] = rolSubmenuPermiso.getEstado();

	    return jdbcTemplate.update(INSERTSUBMENUPERMISO, params);
	} catch (Exception e) {
	    logger.error(e.getMessage(), e);

	}
	return 0;
    }

}
