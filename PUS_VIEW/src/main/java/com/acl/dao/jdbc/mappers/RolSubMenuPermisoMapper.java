/**
 * 
 */
package com.acl.dao.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.acl.model.Permiso;
import com.acl.model.Rol;
import com.acl.model.RolSubMenu;
import com.acl.model.RolSubMenuPermiso;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class RolSubMenuPermisoMapper implements RowMapper<RolSubMenuPermiso>{

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     *   
     * @param rs
     * @param rowNum
     * @return
     * @throws SQLException
     * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
     * @since 1.X
     */
    @Override
    public RolSubMenuPermiso mapRow(ResultSet rs, int rowNum)
	    throws SQLException {
	RolSubMenuPermiso obj = new RolSubMenuPermiso();
	Permiso p = new Permiso();
	p.setSid(rs.getLong("SID_TIPO_PERMISO"));
	obj.setPermiso(p);
	obj.setEstado(rs.getLong("ESTADO"));
	RolSubMenu r = new RolSubMenu();
	r.setSid(rs.getLong("SID_ROL_SUB_MENU"));
	obj.setRolSubMenu(r);
	obj.setSid(rs.getLong("SID"));
	return obj;

    }

}
