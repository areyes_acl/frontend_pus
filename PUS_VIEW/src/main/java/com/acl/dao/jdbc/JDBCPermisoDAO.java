/**
 * 
 */
package com.acl.dao.jdbc;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.acl.dao.PermisoDAO;
import com.acl.dao.jdbc.mappers.PermisoMapper;
import com.acl.dao.jdbc.mappers.RolMapper;
import com.acl.model.Permiso;
import com.acl.model.Rol;
import com.acl.model.RolSubMenu;
import com.acl.model.RolSubMenuPermiso;
import com.acl.model.SubMenu;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Repository(value = "permisoDAO")
public class JDBCPermisoDAO implements PermisoDAO {
    
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(JDBCPermisoDAO.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
    
    final String ALLPERMISO="select SID,TIPO_PERMISO,DESCRIPCION from TBL_TIPO_PERMISO" ;
    final String IDPERMISO="select  SID,TIPO_PERMISO,DESCRIPCION from TBL_TIPO_PERMISO where TIPO_PERMISO=?" ;
    final String  ALLPERMISOFORROL="select  TBL_TIPO_PERMISO.SID,TBL_TIPO_PERMISO.TIPO_PERMISO,TBL_TIPO_PERMISO.DESCRIPCION , TBL_TIPO_PERMISO.ESTADO,TBL_ROL_SUBMENU.SID_ROL, TBL_ROL_SUBMENU.SID_SUB_MENU from    TBL_TIPO_PERMISO INNER JOIN TBL_ROL_MENU_TIPO_PERMISO ON TBL_ROL_MENU_TIPO_PERMISO.SID_TIPO_PERMISO =  TBL_TIPO_PERMISO.SID INNER JOIN TBL_ROL_SUBMENU ON TBL_ROL_SUBMENU.SID=TBL_ROL_MENU_TIPO_PERMISO.SID_ROL_SUB_MENU WHERE   TBL_ROL_MENU_TIPO_PERMISO.ESTADO=?1 AND TBL_ROL_SUBMENU.SID_ROL=?2 and TBL_ROL_SUBMENU.SID_SUB_MENU=?3 " ;
    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     *   
     * @return
     * @see com.acl.dao.PermisoDAO#getAllPermiso()
     * @since 1.X
     */
    @Override
    public List<Permiso> getAllPermiso() {
	List<Permiso> permisoList = new ArrayList<Permiso>();

	List<Map<String, Object>> rows = getJdbcTemplate().queryForList(ALLPERMISO);
	for (Map row : rows) {
	    	Permiso permiso = new Permiso();
	    	BigDecimal bg=(BigDecimal) row.get("SID");
	    	permiso.setSid(bg.longValue());
	    	
	    	permiso.setPermiso((String)row.get("TIPO_PERMISO"));
	    	
	    	permiso.setDescripcion((String)row.get("DESCRIPCION"));
	  
	    	permisoList.add(permiso);
	}

	return permisoList;
    }
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    public Permiso getIdPermiso(String nombre){
	String sql=IDPERMISO;
	sql=sql.replace("?","'"+nombre+"'");
	return  (Permiso) this.jdbcTemplate.queryForObject(sql, new RowMapper<Permiso>() {
            @Override
	    public Permiso mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Permiso permiso = new Permiso();
        	permiso.setSid(rs.getLong("SID"));
        	permiso.setPermiso(rs.getString("TIPO_PERMISO"));
        	permiso.setDescripcion(rs.getString("DESCRIPCION"));
                return permiso;
	    }
        });
    }
    
    public List<Permiso> getAllPermisoForRol(RolSubMenu rolSubMenu,String estado){
	
	 
	String ALLPERMISOFORROL2=ALLPERMISOFORROL;
	List<Permiso> permisoList = new ArrayList<Permiso>();
	ALLPERMISOFORROL2=ALLPERMISOFORROL2.replace("?1", estado);
	ALLPERMISOFORROL2=ALLPERMISOFORROL2.replace("?2", rolSubMenu.getRol().getSid().toString());
	ALLPERMISOFORROL2=ALLPERMISOFORROL2.replace("?3", rolSubMenu.getSubmenu().getSid().toString());

	List<Map<String, Object>> rows = getJdbcTemplate().queryForList(ALLPERMISOFORROL2);
	for (Map row : rows) {
	    	Permiso permiso = new Permiso();
	    	BigDecimal bg=(BigDecimal) row.get("SID");
	    	permiso.setSid(bg.longValue());
	    	
	    	permiso.setPermiso((String)row.get("TIPO_PERMISO"));
	    	
	    	permiso.setDescripcion((String)row.get("DESCRIPCION"));
	  
	    	permisoList.add(permiso);
	}

	return permisoList;

    }

}
