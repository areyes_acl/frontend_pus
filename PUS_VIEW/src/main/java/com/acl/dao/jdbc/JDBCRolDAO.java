/**
 * 
 */
package com.acl.dao.jdbc;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.acl.dao.RolDAO;
import com.acl.dao.jdbc.mappers.RolMapper;
import com.acl.model.Rol;
import com.acl.model.commons.DatabaseConstant;
import com.acl.model.commons.GlobalConstants;
import com.acl.model.exception.RolServiceException;
import com.acl.model.filter.FiltroRoles;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX</YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Repository(value = "rolDAO")
public class JDBCRolDAO implements RolDAO, Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(JDBCRolDAO.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

    private static final String SQL_ALL_ROLS = "SELECT SID, NOMBRE_ROL, DESCRIPCION, FECHA_ING, SID_USR_ING, FECHA_MOD, SID_USR_MOD, ESTADO FROM SIG."+DatabaseConstant.TABLA_ROL+" WHERE 1=1";
    private static final String SP_SIG_GESTIONAR_ROL = "SP_SIG_GESTIONAR_ROL";
    private static final String SCHEMA = "SIG";

   

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see com.acl.dao.GenericDAO#obtenerTodos()
     * @since 1.X
     */
    @Override
    public List<Rol> buscarRoles(FiltroRoles filtro) {
	String sql = SQL_ALL_ROLS;

	if (filtro != null && filtro.getSid() != null) {
	    sql = sql.concat(" AND  SID = :sid");
	    sql = sql.replace(":sid", String.valueOf(filtro.getSid()));
	}

	if (filtro != null
		&& filtro.getNombreRol() != null
		&& !(GlobalConstants.EMPTY.equalsIgnoreCase(filtro
			.getNombreRol().trim()))) {
	    sql = sql.concat(" AND UPPER(translate(NOMBRE_ROL, 'áéíóúÁÉÍÓÚ', 'aeiouAEIOU')) LIKE UPPER(translate('%:nombre:%', 'áéíóúÁÉÍÓÚ', 'aeiouAEIOU'))");
	    sql = sql.replace(":nombre:", filtro.getNombreRol().toUpperCase());
	    
	}

	logger.info(sql);
	return jdbcTemplate.query(sql, new RolMapper());

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rol
     * @throws RolServiceException
     * @see com.acl.dao.RolDAO#guardar(com.acl.model.Rol)
     * @since 1.X
     */
    @Override
    public void guardar(Rol rol) throws RolServiceException {

	SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
	simpleJdbcCall.withSchemaName(SCHEMA);
	simpleJdbcCall.withProcedureName(SP_SIG_GESTIONAR_ROL);

	Map<String, Object> inParamMap = new HashMap<String, Object>();
	inParamMap.put("p_sidRol", rol.getSid());
	inParamMap.put("p_nombre_rol", rol.getNombre());
	inParamMap.put("p_descripcion", rol.getDescripcion());
	inParamMap.put("p_sid_usr_mod", rol.getLog().getSidModificacion());
	inParamMap.put("p_estado", rol.getEstado().getValue());

	SqlParameterSource in = new MapSqlParameterSource(inParamMap);

	Map<String, Object> simpleJdbcCallResult = simpleJdbcCall.execute(in);
	Integer codigoError = ((BigDecimal) simpleJdbcCallResult
		.get("COD_ERROR")).intValue();
	String warning = (String) simpleJdbcCallResult.get("WARNING");

	logger.info(simpleJdbcCallResult);

	if (codigoError != 0) {
	    throw new RolServiceException(warning);
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see com.acl.dao.RolDAO#getAllRol()
     * @since 1.X
     */
    @Override
    public List<Rol> getAllRol() {
	String sql = SQL_ALL_ROLS;

	try {
	    logger.debug(sql);
	    return jdbcTemplate.query(sql, new RolMapper());
	} catch (EmptyResultDataAccessException e) {
	    return null;
	}
    }

}
