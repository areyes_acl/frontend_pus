package com.acl.dao.jdbc.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.acl.model.Parametros;
import com.acl.model.types.EstadoParametroType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ParametrosMapper implements RowMapper<Parametros> {

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rs
     * @param rowNum
     * @return
     * @throws SQLException
     * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet,
     *      int)
     * @since 1.X SELECT SID, NOMBRE_PARAMETRO, VALOR, FECHA_INGRESO,
     *        REF_SID_USR_ING, FECHA_MODIFICACION, REF_SID_USR_MOD, ESTADO FROM
     *        SIG.TBL_POLITICAS_USUARIO_PRTS
     */
    @Override
    public Parametros mapRow(ResultSet rs, int rowNum) throws SQLException {
	Parametros politica = new Parametros();
	politica.setSid(rs.getLong("SID"));
	politica.setNombre(rs.getString("NOMBRE_PARAMETRO"));
	politica.setValor(rs.getString("VALOR"));
	politica.setNewValor(rs.getString("VALOR"));
	politica.getLog().setFechaCreacion(rs.getDate("FECHA_INGRESO"));
	politica.getLog().setSidCreacion(rs.getLong("REF_SID_USR_ING"));
	politica.getLog()
		.setFechaModificacion(rs.getDate("FECHA_MODIFICACION"));
	politica.getLog().setSidModificacion(rs.getLong("REF_SID_USR_MOD"));
	politica.setEstado(EstadoParametroType.fromValue(rs.getInt("ESTADO")));

	return politica;
    }

}