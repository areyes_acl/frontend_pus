/**
 * 
 */
package com.acl.dao;

import java.util.List;

import com.acl.model.Rol;
import com.acl.model.RolSubMenu;
import com.acl.model.SubMenu;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface RolSubMenuDAO {
    	public List<RolSubMenu> searchRolSubMenu(Rol rol, SubMenu subMenu);
    	public int updateSubmenuEstado(RolSubMenu idRolSubMenu, String estado);
    	public Long createRolSubMenu(RolSubMenu rolSubmenu);

    	
}
