/**
 * 
 */
package com.acl.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acl.dao.ParametrosDAO;
import com.acl.model.Parametros;
import com.acl.model.filter.FiltroParametro;
import com.acl.service.ParametrosService;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * Servicio disponible para gestionar usuarios
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Service(value = "parametrosService")
public class ParametrosServiceImpl implements ParametrosService {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    @Autowired
    private ParametrosDAO parametrosDao;

    public ParametrosDAO getParametrosDao() {
	return parametrosDao;
    }

    public void setParametrosDao(ParametrosDAO parametrosDao) {
	this.parametrosDao = parametrosDao;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param objeto
     * @see com.acl.service.ParametrosService#guardar(com.acl.model.Parametro)
     * @since 1.X
     */
    @Override
    public void guardar(Parametros objeto) {
	// TODO Auto-generated method stub

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param idUsuario
     * @see com.acl.service.ParametrosService#eliminar(java.lang.String)
     * @since 1.X
     */
    @Override
    public void eliminar(String idUsuario) {
	// TODO Auto-generated method stub

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param filtro
     * @return
     * @see com.acl.service.ParametrosService#buscarPorFiltro(com.acl.model.filter.FiltroParametro)
     * @since 1.X
     */
    @Override
    public List<Parametros> buscarPorFiltro(FiltroParametro filtro) {
	return parametrosDao.buscarPorFiltro(filtro);
    }

    public int update(Parametros parametros) {
	return parametrosDao.update(parametros);
    }
}
