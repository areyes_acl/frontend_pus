/**
 * 
 */
package com.acl.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.acl.dao.ContrasenaByUsuarioDAO;
import com.acl.dao.ParametrosDAO;
import com.acl.dao.RolByUsuarioDAO;
import com.acl.dao.UsuarioDAO;
import com.acl.model.Contrasena;
import com.acl.model.Rol;
import com.acl.model.Usuario;
import com.acl.model.commons.GlobalConstants;
import com.acl.model.exception.RolByUserException;
import com.acl.model.exception.ServiceException;
import com.acl.model.exception.UsuarioServiceException;
import com.acl.model.filter.FiltroUsuario;
import com.acl.model.types.EstadoCambioType;
import com.acl.model.types.EstadoContrasenaType;
import com.acl.model.utils.PasswordUtils;
import com.acl.service.UsuarioService;
import com.acl.service.logic.IContrasena;
import com.acl.service.logic.IUsuario;
import com.acl.service.logic.impl.ContrasenaLogic;
import com.acl.service.logic.impl.UsuarioLogic;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * Servicio disponible para gestionar usuarios
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Service(value = "usuarioService")
public class UsuarioServiceImpl implements UsuarioService {

    /**
     * Constante de logger
     */
    private static final Logger logger = Logger
	    .getLogger(UsuarioServiceImpl.class);

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    @Autowired
    private UsuarioDAO usuarioDAO;
    @Autowired
    private RolByUsuarioDAO rolByUsuario;
    @Autowired
    private ParametrosDAO politicaDAO;
    @Autowired
    private ContrasenaByUsuarioDAO contrasenaByUsuarioDAO;

    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
	this.usuarioDAO = usuarioDAO;
    }

    public RolByUsuarioDAO getRolByUsuario() {
	return rolByUsuario;
    }

    public void setRolByUsuario(RolByUsuarioDAO rolByUsuario) {
	this.rolByUsuario = rolByUsuario;
    }

    public void setPoliticaDAO(ParametrosDAO politicaDAO) {
	this.politicaDAO = politicaDAO;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param objeto
     * @throws ServiceException
     * @see com.acl.service.UsuarioService#insertar(com.acl.model.Usuario)
     * @since 1.X
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public void guardar(Usuario usuario) throws ServiceException {

	try {
	    // Nuevo Usuario
	    if (usuario.getSid() == null) {
		doBussinesPasswordValidation(usuario);

		// CONVERSION DE CONTRASENA A SHA-256
		String passSha256 = PasswordUtils
			.convertirPasswordSha256(usuario.getContrasena()
				.getPassword());
		usuario.getContrasena().setPassword(passSha256);

		// GUARDA USUARIO Y CONTRASENA
		this.usuarioDAO.guardar(usuario);

		// Guardar los roles
		this.rolByUsuario.agregarRolesUsuarioByRut(usuario.getRut(),
			usuario.getListaRoles());
			
	    } else {
	    System.out.println("prueba de integracion");
		// SOLO SI CAMBIA LA CONTRASEÑA SE MANDA A VALIDAR ESTA DENUEVO
		if (usuario.getContrasena().getPassword() != null) {
		    doBussinesPasswordValidation(usuario);
		}
	    }
	} catch (UsuarioServiceException e) {
	    throw new ServiceException(e.getMessage(), e);
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que realiza la validacion de si un usuario es valido o no
     * 
     * @param usuario
     * @throws UserServiceException
     * @since 1.X
     */
    private void doBussinesPasswordValidation(Usuario usuario)
	    throws UsuarioServiceException {

	IContrasena passwordValidator = new ContrasenaLogic();
	passwordValidator.setPoliticaDAO(politicaDAO);
	passwordValidator.setContrasenaByUsuarioDAO(contrasenaByUsuarioDAO);
	passwordValidator.validarContrasena(usuario);

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param password
     * @param rut
     * @return
     * @throws UsuarioServiceException
     * @see com.acl.service.UsuarioService#login(java.lang.String,
     *      java.lang.String)
     * @since 1.X
     */
    @Override
    public Usuario login(String password, Integer rut, String dv)
	    throws UsuarioServiceException {
	Usuario usuarioEncontrado = null;
	List<Usuario> usuarios;

	if (password != null && rut != null) {
	    FiltroUsuario filtro = new FiltroUsuario(rut, dv);
	    usuarios = buscarUsuariosPorFiltro(filtro);
	    if (usuarios != null && !usuarios.isEmpty()) {
		usuarioEncontrado = usuarios.get(0);

		if (usuarioEncontrado != null) {
		    IUsuario usuarioProxyLogic = new UsuarioLogic(this);
		    usuarioProxyLogic.doLoginBussinessValidation(password,
			    usuarioEncontrado);
		}
	    } else {
		throw new UsuarioServiceException(
			"La autenticación no ha sido correcta, usuario ingresado no existe en el sistema.");
	    }
	}

	return usuarioEncontrado;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param filtroUsuario
     * @return
     * @see com.acl.service.UsuarioService#buscarUsuariosPorFiltro(com.acl.model.filter.UserFilter)
     * @since 1.X
     */
    @Override
    public List<Usuario> buscarUsuariosPorFiltro(FiltroUsuario filtroUsuario) {
	List<Usuario> listaUsuarios = this.usuarioDAO
		.buscarUsuariosPorFiltro(filtroUsuario);

	for (Usuario usuario : listaUsuarios) {
	    List<Contrasena> contrasenas = contrasenaByUsuarioDAO
		    .obtenerContrasenasAnterioresPorUsuario(
			    EstadoContrasenaType.ACTIVA, usuario.getSid(), 1);

	    if (contrasenas != null && !contrasenas.isEmpty()) {
		usuario.setContrasena(contrasenas.get(0));
	    }

	    List<Rol> roles = rolByUsuario.obtenerRolesPorRutUsuario(usuario
		    .getRut());

	    // Setea los roles como base
	    for (Rol rol : roles) {
		rol.setEstadoCambio(EstadoCambioType.BASE);
	    }

	    usuario.setListaRoles(roles);

	}

	return listaUsuarios;

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param usuario
     * @throws ServiceException
     * @see com.acl.service.UsuarioService#editarUsuario(com.acl.model.Usuario)
     * @since 1.X
     */
    @Override
    public void actualizarUsuario(Usuario usuario) throws ServiceException {

	try {

	    logger.info("password en servicio  : "
		    + usuario.getContrasena().getPassword());
	    // Si la contraseña cambia se debe realizar la validacion de negocio
	    // de esta
	    if (usuario.getContrasena().getPassword() != null
		    && !GlobalConstants.EMPTY.equalsIgnoreCase(usuario
			    .getContrasena().getPassword())) {
		logger.info("Se valida contrasena");
		doBussinesPasswordValidation(usuario);
		String passwordSha256 = PasswordUtils
			.convertirPasswordSha256(usuario.getContrasena()
				.getPassword());
		usuario.getContrasena().setPassword(passwordSha256);

	    }

	    logger.info(usuario);
	    // Se actualiza usuario
	    usuarioDAO.actualizar(usuario);

	    // Logica para insertar/desactivar roles
	    for (Rol item : usuario.getListaRoles()) {

		// Si es un nuevo rol y no esta la relacion creada en Base de
		// datos
		if (EstadoCambioType.NEW.equals(item.getEstadoCambio())) {
		    logger.info("SE DEBE INSERTAR NUEVO ROL " + item);
		    rolByUsuario.agregarRolUsuarioBySid(usuario.getSid(), item);

		    // Se debe actualizar el rol de usuario
		} else if (EstadoCambioType.UPDATE.equals(item
			.getEstadoCambio())
			|| EstadoCambioType.DELETE.equals(item
				.getEstadoCambio())) {
		    logger.info("SE DEBE REALIZAR " + item.getEstadoCambio()
			    + " DEL ROL " + item.getNombre());
		    rolByUsuario.actualizarRolUsuario(usuario.getSid(), item);
		}
	    }
	} catch (RolByUserException e) {
	    throw new ServiceException(e.getMessage(), e);

	} catch (UsuarioServiceException e) {
	    throw new ServiceException(e.getMessage(), e);
	}
    }
}
