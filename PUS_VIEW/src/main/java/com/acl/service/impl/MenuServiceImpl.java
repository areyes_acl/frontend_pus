/**
 * 
 */
package com.acl.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acl.dao.MenuDAO;
import com.acl.model.Menu;
import com.acl.service.MenuService;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * Servicio disponible para gestionar usuarios
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Service(value = "menuService")
public class MenuServiceImpl implements MenuService {

    private static final long serialVersionUID = 1L;
    @Autowired
    private MenuDAO menuDAO;

    public MenuDAO getMenuDAO() {
	return menuDAO;
    }

    public void setMenuDAO(MenuDAO menuDAO) {
	this.menuDAO = menuDAO;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see com.acl.service.MenuService#obtenerMenus()
     * @since 1.X
     */
    @Override
    public List<Menu> obtenerMenus() {

	List<Menu> listaMenu = menuDAO.obtenerTodosLosMenus();

	return listaMenu;
    }

}
