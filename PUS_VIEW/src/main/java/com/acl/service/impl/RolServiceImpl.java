/**
 * 
 */
package com.acl.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acl.dao.RolDAO;
import com.acl.model.Rol;
import com.acl.model.exception.RolServiceException;
import com.acl.model.exception.ServiceException;
import com.acl.model.filter.FiltroRoles;
import com.acl.model.types.EstadoCambioType;
import com.acl.service.RolService;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * Servicio disponible para gestionar usuarios
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Service(value = "rolService")
public class RolServiceImpl implements RolService {

    private static final Logger logger = Logger.getLogger(RolServiceImpl.class);

    private static final long serialVersionUID = 1L;
    @Autowired
    private RolDAO rolDAO;

    public RolDAO getRolDAO() {
	return rolDAO;
    }

    public void setRolDAO(RolDAO rolDAO) {
	this.rolDAO = rolDAO;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rol
     * @throws ServiceException
     * @see com.acl.service.RolService#guardar(com.acl.model.Rol)
     * @since 1.X
     */
    @Override
    public void guardar(Rol rol) throws ServiceException {
	try {
	    rolDAO.guardar(rol);
	} catch (RolServiceException e) {
	    throw new ServiceException(e.getMessage(), e);
	}
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see com.acl.service.RolService#listaRoles()
     * @since 1.X
     */
    @Override
    public List<Rol> buscarRoles(FiltroRoles filtro) {
	List<Rol> roles = rolDAO.buscarRoles(filtro);
	for (Rol rol : roles) {
	    rol.setEstadoCambio(EstadoCambioType.NEW);
	}
	return roles;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rol
     * @throws RolServiceException
     * @since 1.X
     */
    @Override
    public void actualizarRol(Rol rol) throws RolServiceException {
	logger.info("Rol a guardar : " + rol);
	rolDAO.guardar(rol);
    }
}
