/**
 * 
 */
package com.acl.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import oracle.net.aso.e;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acl.dao.PermisoDAO;
import com.acl.dao.RolDAO;
import com.acl.dao.RolSubMenuDAO;
import com.acl.dao.RolSubMenuPermisoDAO;
import com.acl.dao.SubMenuDAO;
import com.acl.model.Permiso;
import com.acl.model.Rol;
import com.acl.model.RolSubMenu;
import com.acl.model.RolSubMenuPermiso;
import com.acl.model.SubMenu;
import com.acl.service.PermisoRolService;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Service(value = "permisoRolService")
public class PermisoRolServiceImpl implements PermisoRolService {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Autowired
    private RolDAO rolDAO;
    @Autowired
    private SubMenuDAO subMenuDAO;
    @Autowired
    private PermisoDAO permisoDAO;
    @Autowired
    private RolSubMenuDAO rolSubMenuDAO;
    @Autowired
    private RolSubMenuPermisoDAO rolSubMenuPermisoDAO;
    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     *   
     * @return
     * @see com.acl.service.PermisoRolService#getAllSubMenu()
     * @since 1.X
     */
    
 
    @Override
    public List<SubMenu> getAllSubMenu() {
	// TODO Auto-generated method stub
	
	return subMenuDAO.getAllSubMenu();
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     *   
     * @return
     * @see com.acl.service.PermisoRolService#getAllPermisos()
     * @since 1.X
     */
    @Override
    public List<Permiso> getAllPermisos() {
	// TODO Auto-generated method stub
	return permisoDAO.getAllPermiso();
    }

    public List<Permiso> getAllPermisoForRol(RolSubMenu rolSubMenu,String estado){
	
	
	return permisoDAO.getAllPermisoForRol(rolSubMenu, estado);
	
    }
    
    
    public RolDAO getRolDAO() {
        return rolDAO;
    }

    public void setRolDAO(RolDAO rolDAO) {
        this.rolDAO = rolDAO;
    }

    public SubMenuDAO getSubMenuDAO() {
        return subMenuDAO;
    }

    public void setSubMenuDAO(SubMenuDAO subMenuDAO) {
        this.subMenuDAO = subMenuDAO;
    }

    public PermisoDAO getPermisoDAO() {
        return permisoDAO;
    }

    public void setPermisoDAO(PermisoDAO permisoDAO) {
        this.permisoDAO = permisoDAO;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     *   
     * @param rolSubmenu
     * @return
     * @see com.acl.service.PermisoRolService#updateEstate(com.acl.model.RolSubMenu)
     * @since 1.X
     */
    @Override
    public boolean updateEstate(RolSubMenu rolSubmenu) {
	// TODO Auto-generated method stub
	//todos los rolsubmenu 
	List<RolSubMenu> rolSubMenuBD =rolSubMenuDAO.searchRolSubMenu(rolSubmenu.getRol(), rolSubmenu.getSubmenu());
	
	if (rolSubMenuBD!=null & rolSubMenuBD.size()==0){
	    //Agregando nuevos permisos Noexisten en bd Primer ingreso
        	    if(rolSubmenu!=null & rolSubmenu.getPermisionsListNameSelection().length>0){	
        		rolSubmenu.setSid((Long)rolSubMenuDAO.createRolSubMenu(rolSubmenu));
        		for (String f:rolSubmenu.getPermisionsListNameSelection()){ 
        		    for(Permiso p :rolSubmenu.getPermisoList()){	
        			if (p.getPermiso().equals(f)){	    
                		    RolSubMenuPermiso rolPermiso= new RolSubMenuPermiso();
                        	    rolPermiso.setEstado(1L);
                        	    rolPermiso.setPermiso(p);
                        	    rolPermiso.setRolSubMenu(rolSubmenu);
                        	    rolSubMenuPermisoDAO.createRolSubMenuPermiso(rolPermiso);
        			}
        		    }
        		}
        		
        	    }
        	    

	}else if (rolSubMenuBD!=null & rolSubMenuBD.size()>0){
	    for (RolSubMenu submenu:rolSubMenuBD){
		List<Permiso> permisoBD=permisoDAO.getAllPermisoForRol(submenu,"1");
		    HashMap<String,Boolean> permisosActualesenBD = new HashMap<String,Boolean>();
		    HashMap<String,Boolean> permisosdeVista = new HashMap<String,Boolean>();
		    HashMap<String,Integer> comparacion = new HashMap<String,Integer>();
		    for (Permiso p :permisoBD){
			permisosActualesenBD.put(p.getPermiso(), true);
		    }
		    for (String x:rolSubmenu.getPermisionsListNameSelection()){
			permisosdeVista.put(x, true);
		    }
		    Iterator<String> it =permisosdeVista.keySet().iterator();
		    while(it.hasNext()){
			String x =it.next();
			if (permisosActualesenBD.containsKey(x)){
			    comparacion.put("vista-bd-iguales", 1);
			   
			}else{
			    comparacion.put("existe-vista-no-bd", 1);
			  //Agregando nuevos permisos existen en bd 
			    List<Permiso> permisoBDDesactivados=permisoDAO.getAllPermisoForRol(submenu,"1");
			    if (permisoBDDesactivados.size()==0){
				//Agregando nuevos permisos existe el rol menu con otros permisos pero se agregan nuevos
				    Permiso nuevoPermiso= permisoDAO.getIdPermiso(x);
				   
				    if (nuevoPermiso.getPermiso().equals(x)){
	        			    RolSubMenuPermiso rolPermiso= new RolSubMenuPermiso();
	                        	    rolPermiso.setEstado(1L);
	                        	    rolPermiso.setPermiso(nuevoPermiso);
	                        	    rolPermiso.setRolSubMenu(submenu);
	                        	    rolSubMenuPermisoDAO.createRolSubMenuPermiso(rolPermiso);
	                        	    
				    }
			    }else if (permisoBDDesactivados.size()>0){
				//Actualizando  permisos existe el rol menu con Estado desactivado y se activa
				List<Permiso> permisoBDDesactivados2=permisoDAO.getAllPermisoForRol(submenu,"0");
				
				for (Permiso p :permisoBDDesactivados2){
				   
				    if (p.getPermiso().equals(x)){
					
					 RolSubMenuPermiso rolsubmenu2=rolSubMenuPermisoDAO.searchRolSubMenuPermiso(submenu, p,"0");
	                        	 rolSubMenuPermisoDAO.updateRolSubMenuPermisoEstado(rolsubmenu2, "1");
				    }
				}
			    }
			}
			
		    }
		    Iterator<String> it2 =permisosActualesenBD.keySet().iterator();
		    while(it2.hasNext()){
			String x =it2.next();
			if (permisosdeVista.containsKey(x)){
			    comparacion.put("vista-bd-iguales", 1);
			   
			}else{
			  //Actualizando  permisos existe el rol menu con Estado activado y se desactiva
			    comparacion.put("existe-bd-no-vista", 1);
			   
			    Permiso nuevoPermiso= permisoDAO.getIdPermiso(x);
			    RolSubMenuPermiso rolsubmenu=rolSubMenuPermisoDAO.searchRolSubMenuPermiso(submenu, nuevoPermiso,"1");
			    rolSubMenuPermisoDAO.updateRolSubMenuPermisoEstado(rolsubmenu, "0");
			    
			}
			
		    }
	    }
	}
	return true;
    }

    public RolSubMenuDAO getRolSubMenuDAO() {
        return rolSubMenuDAO;
    }

    public void setRolSubMenuDAO(RolSubMenuDAO rolSubMenuDAO) {
        this.rolSubMenuDAO = rolSubMenuDAO;
    }

    public RolSubMenuPermisoDAO getRolSubMenuPermisoDAO() {
        return rolSubMenuPermisoDAO;
    }

    public void setRolSubMenuPermisoDAO(RolSubMenuPermisoDAO rolSubMenuPermisoDAO) {
        this.rolSubMenuPermisoDAO = rolSubMenuPermisoDAO;
    }
    
    
}
