/**
 * 
 */
package com.acl.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acl.dao.PreguntaDAO;
import com.acl.model.Pregunta;
import com.acl.model.exception.PreguntaServiceException;
import com.acl.model.filter.FiltroPregunta;
import com.acl.service.PreguntaService;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * Servicio disponible para gestionar usuarios
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
@Service(value = "preguntaService")
public class PreguntaServiceImpl implements PreguntaService {

    private static final long serialVersionUID = 1L;
    @Autowired
    private PreguntaDAO preguntaDAO;

    public PreguntaDAO getPreguntaDAO() {
	return preguntaDAO;
    }

    public void setPreguntaDAO(PreguntaDAO preguntaDAO) {
	this.preguntaDAO = preguntaDAO;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @return
     * @see com.acl.service.PreguntaService#buscarPreguntas()
     * @since 1.X
     */
    @Override
    public List<Pregunta> buscarPreguntas(FiltroPregunta filtro) {
	return preguntaDAO.buscarPreguntas(filtro);
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param pregunta
     * @throws PreguntaServiceException
     * @see com.acl.service.PreguntaService#guardar(com.acl.model.Pregunta)
     * @since 1.X
     */
    @Override
    public void guardar(Pregunta pregunta) throws PreguntaServiceException {
	preguntaDAO.guardar(pregunta);

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param pregunta
     * @throws PreguntaServiceException
     * @see com.acl.service.PreguntaService#actualizar(com.acl.model.Pregunta)
     * @since 1.X
     */
    @Override
    public void actualizar(Pregunta pregunta) throws PreguntaServiceException {
	preguntaDAO.actualizar(pregunta);
    }

}
