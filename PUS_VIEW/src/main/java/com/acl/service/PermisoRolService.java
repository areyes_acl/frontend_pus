/**
 * 
 */
package com.acl.service;

import java.io.Serializable;
import java.util.List;

import com.acl.model.Permiso;
import com.acl.model.Rol;
import com.acl.model.RolSubMenu;
import com.acl.model.RolSubMenuPermiso;
import com.acl.model.SubMenu;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface PermisoRolService extends Serializable {
    	public List<SubMenu> getAllSubMenu();
    	public List<Permiso> getAllPermisos();
    	public List<Permiso> getAllPermisoForRol(RolSubMenu rolSubmenu,String estado);
    	public boolean updateEstate(RolSubMenu rolSubmenu);

}
