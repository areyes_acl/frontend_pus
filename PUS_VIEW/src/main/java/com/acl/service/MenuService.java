/**
 * 
 */
package com.acl.service;

import java.io.Serializable;
import java.util.List;

import com.acl.model.Menu;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface MenuService extends Serializable {

    public List<Menu> obtenerMenus();

}
