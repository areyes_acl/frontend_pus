/**
 * 
 */
package com.acl.service;

import java.io.Serializable;
import java.util.List;

import com.acl.model.Parametros;
import com.acl.model.filter.FiltroParametro;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface ParametrosService extends Serializable {

    public List<Parametros> buscarPorFiltro(FiltroParametro filtro);

    public void guardar(Parametros objeto);

    public void eliminar(String idUsuario);
    
    public int update(Parametros oarametros);

}
