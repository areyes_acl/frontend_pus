/**
 * 
 */
package com.acl.service;

import java.io.Serializable;
import java.util.List;

import com.acl.model.Usuario;
import com.acl.model.exception.ServiceException;
import com.acl.model.exception.UsuarioServiceException;
import com.acl.model.filter.FiltroUsuario;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface UsuarioService extends Serializable {

    public Usuario login(String password, Integer rut, String dv)
	    throws UsuarioServiceException;

    public List<Usuario> buscarUsuariosPorFiltro(FiltroUsuario filtroUsuario);

    public void guardar(Usuario usuario) throws ServiceException;

    public void actualizarUsuario(Usuario usuario) throws ServiceException;

}
