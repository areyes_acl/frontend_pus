/**
 * 
 */
package com.acl.service.logic.impl;

import org.apache.log4j.Logger;

import com.acl.model.Usuario;
import com.acl.model.exception.UsuarioServiceException;
import com.acl.model.utils.PasswordUtils;
import com.acl.service.UsuarioService;
import com.acl.service.logic.IUsuario;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class UsuarioLogic implements IUsuario {

    private UsuarioService usuarioService;

    private static final Logger logger = Logger.getLogger(UsuarioLogic.class);

    public void setUsuarioService(UsuarioService usuarioService) {
	this.usuarioService = usuarioService;
    }

    public UsuarioLogic() {

    }

    public UsuarioLogic(UsuarioService usuarioService) {
	this.usuarioService = usuarioService;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param password
     * @param usuarioEncontrado
     * @throws UsuarioServiceException
     * @see com.acl.service.logic.IUsuario#doBussinessValidation(java.lang.String,
     *      com.acl.model.Usuario)
     * @since 1.X
     */
    @Override
    public void doLoginBussinessValidation(String password,
	    Usuario usuarioEncontrado) throws UsuarioServiceException {

	// Valida que sea superUsuario
	if (usuarioEncontrado != null && usuarioEncontrado.getEsSuperUsuario()) {

	    String passwordInEncriptado = PasswordUtils
		    .convertirPasswordSha256(password);

	    logger.info("PW sin encriptar:" + password +"  (largo = "+password.length()+")" );
	    logger.info("PW Encriptado: " + passwordInEncriptado  +"  (largo = "+passwordInEncriptado.length()+")");
	    logger.info("PW usuario BD : "+ usuarioEncontrado.getContrasena().getPassword());

	    // Password incorrecto
	    if (!passwordInEncriptado.equals(usuarioEncontrado.getContrasena()
		    .getPassword())) {
		logger.warn("Contraseñas no coinciden");
		throw new UsuarioServiceException(
			"La autenticación no ha sido correcta");
	    }
	} else {
	    logger.warn("Usuario no es administrador");
	    throw new UsuarioServiceException(
		    "El usuario que se está intentando ingresar al sistema no es Administrador.");
	}

    }
}
