/**
 * 
 */
package com.acl.service.logic;

import com.acl.model.Usuario;
import com.acl.model.exception.UsuarioServiceException;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * ssssssssssssssss
 * <p>
 * 
 * 
 * <p>
 */
public interface IUsuario {

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param password
     * @param usuarioEncontrado
     * @throws UsuarioServiceException
     * @since 1.X
     */
    public void doLoginBussinessValidation(String password, Usuario usuarioEncontrado)
	    throws UsuarioServiceException;

}
