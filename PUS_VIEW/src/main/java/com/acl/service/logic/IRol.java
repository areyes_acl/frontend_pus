/**
 * 
 */
package com.acl.service.logic;

import java.util.List;

import com.acl.model.Rol;
import com.acl.model.exception.ServiceException;
import com.acl.model.filter.FiltroRoles;
import com.acl.service.RolService;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class IRol {

    private RolService rolService;

    public void setRolService(RolService rolService) {
	this.rolService = rolService;
    }

    public IRol() {

    }

    public IRol(RolService rolService) {
	this.rolService = rolService;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param aRol
     * @since 1.X
     */
    public Rol doBussinessValidation(Rol aRol) throws ServiceException {
	FiltroRoles filtro = new FiltroRoles();
	filtro.setNombreRol(aRol.getNombre());
	List<Rol> auxRol = rolService.buscarRoles(filtro);

	if (auxRol != null) {
	    throw new ServiceException(
		    "Existe un rol con el mismo nombre en Base de datos.");
	}

	return aRol;

    }

}
