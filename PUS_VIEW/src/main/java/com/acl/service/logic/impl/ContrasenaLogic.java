/**
 * 
 */
package com.acl.service.logic.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.acl.dao.ContrasenaByUsuarioDAO;
import com.acl.dao.ParametrosDAO;
import com.acl.model.Contrasena;
import com.acl.model.Parametros;
import com.acl.model.Usuario;
import com.acl.model.exception.UsuarioServiceException;
import com.acl.model.filter.FiltroParametro;
import com.acl.model.types.AccionType;
import com.acl.model.types.ContrasenaType;
import com.acl.model.types.EstadoParametroType;
import com.acl.model.types.ParametrosType;
import com.acl.model.utils.PasswordUtils;
import com.acl.service.logic.IContrasena;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * CLASE QUE SE ENCARGA DE VALIDAR LA CONTRASEÑA • Leading (Comiencen con
 * números) • Trailing (Que terminen con números) • Largo – (Mínimo y máximo) •
 * Numérica, alfabética, alfanumérica • Uso de caracteres especiales • Vigencia
 * de contraseña • Permite caracteres consecutivos • Permite caracteres iguales
 * seguidos • Sensibilidad a mayúsculas y minúsculas • Obliga uso mayúscula
 * 
 * 
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ContrasenaLogic implements IContrasena {

    /**
     * Constante de logger
     */
    private static final Logger logger = Logger
	    .getLogger(ContrasenaLogic.class);

    /**
     * VARIABLES
     */
    private String noPermite = "0";
    private List<Parametros> listaPoliticasActivas;
    private ParametrosDAO politicaDAO;
    private ContrasenaByUsuarioDAO contrasenaByUsuarioDAO;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param usuario
     * @throws UsuarioServiceException
     * @see com.acl.service.logic.IContrasena#validarContrasena(com.acl.model.Usuario)
     * @since 1.X
     */
    @Override
    public void validarContrasena(Usuario usuario)
	    throws UsuarioServiceException {

	Contrasena contrasena = usuario.getContrasena();
	AccionType accion = (usuario.getSid() == null) ? AccionType.NUEVO
		: AccionType.ACTUALIZACION;

	// Obtiene las politicas activas
	FiltroParametro filtro = new FiltroParametro(EstadoParametroType.ACTIVO);
	listaPoliticasActivas = politicaDAO.buscarPorFiltro(filtro);

	// Validación común independiente del tipo de contraseña utilizado
	PasswordUtils.validarContrasenaIgualARut(usuario.getRut(),
		usuario.getDv(), contrasena);

	if (listaPoliticasActivas != null && !listaPoliticasActivas.isEmpty()) {

	    // Buscar parametro del tipo de contrasena
	    Parametros parametro = buscarTipoContrasena();

	    if (parametro != null
		    && ContrasenaType.NUMERICA.getLabel().equalsIgnoreCase(
			    parametro.getValor())) {
		validacionesTipoNumerica(contrasena, accion, usuario.getSid());

	    } else if (parametro != null
		    && ContrasenaType.ALFABETICA.getLabel().equalsIgnoreCase(
			    parametro.getValor())) {
		validacionesTipoAlfabetica(contrasena, accion, usuario.getSid());

	    } else if (parametro != null
		    && ContrasenaType.ALFANUMERICA.getLabel().equalsIgnoreCase(
			    parametro.getValor())) {
		validacionesTipoAlfanumerica(contrasena, accion,
			usuario.getSid());

	    }

	}
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param listaPoliticas
     * @throws UsuarioServiceException
     * @throws
     * @since 1.X
     */
    private void validacionesTipoAlfanumerica(Contrasena contrasena,
	    AccionType accion, Long sidUsuario) throws UsuarioServiceException {

	for (Parametros parametro : listaPoliticasActivas) {

	    // VALIDAR EL TIPO DE CONTRASEÑA

	    validacionesGenericas(contrasena, parametro);

	    // 1- Comience con numeros
	    if (parametro.getParametro().equals(ParametrosType.OBLIGA_LEADING)
		    && parametro.getValor().equalsIgnoreCase(noPermite)) {
		PasswordUtils.validarLeading(contrasena.getPassword());
	    }

	    // 2- Termine con numeros
	    if (parametro.getParametro().equals(ParametrosType.OBLIGA_TRAILING)
		    && parametro.getValor().equalsIgnoreCase(noPermite)) {
		PasswordUtils.validarTrailing(contrasena.getPassword());
	    }

	    // 7- Validar cantidad de veces de rehuso (CONTRASENA NO NUEVA)
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_RECHAZO_RE_USO)
		    && accion.equals(AccionType.ACTUALIZACION)) {
		validarNroRechazoRehuso(contrasena,
			Integer.valueOf(parametro.getValor()), sidUsuario);
	    }

	    // 11- Obliga uso de mayuscula
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_DE_MAYUSCULAS_OBLIGATORIAS)) {
		PasswordUtils.validaObligatoriedadMayuscula(
			contrasena.getPassword(),
			Integer.valueOf(parametro.getValor()));
	    }
	}
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param listaPoliticasActivas2
     * @throws UsuarioServiceException
     * @since 1.X
     */
    private void validacionesTipoAlfabetica(Contrasena contrasena,
	    AccionType accion, Long sidUsuario) throws UsuarioServiceException {

	for (Parametros parametro : listaPoliticasActivas) {

	    validacionesGenericas(contrasena, parametro);

	    // 7- Validar cantidad de veces de rehuso (CONTRASENA NO NUEVA)
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_RECHAZO_RE_USO)
		    && accion.equals(AccionType.ACTUALIZACION)) {
		validarNroRechazoRehuso(contrasena,
			Integer.valueOf(parametro.getValor()), sidUsuario);
	    }


	    // 11- Obliga uso de mayuscula
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_DE_MAYUSCULAS_OBLIGATORIAS)) {
		PasswordUtils.validaObligatoriedadMayuscula(
			contrasena.getPassword(),
			Integer.valueOf(parametro.getValor()));
	    }

	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param listaPoliticasActivas2
     * @throws UsuarioServiceException
     * @since 1.X
     */
    private void validacionesTipoNumerica(Contrasena contrasena,
	    AccionType accion, Long sidUsuario) throws UsuarioServiceException {

	for (Parametros parametro : listaPoliticasActivas) {

	    validacionesGenericas(contrasena, parametro);

	    // 7- Validar cantidad de veces de rehuso (CONTRASENA NO NUEVA)
	    if (parametro.getParametro().equals(
		    ParametrosType.NRO_RECHAZO_RE_USO)
		    && accion.equals(AccionType.ACTUALIZACION)) {
		validarNroRechazoRehuso(contrasena,
			Integer.valueOf(parametro.getValor()), sidUsuario);
	    }
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param contrasena
     * @param parametro
     * @throws UsuarioServiceException
     * @since 1.X
     */
    private void validacionesGenericas(Contrasena contrasena,
	    Parametros parametro) throws UsuarioServiceException {

	if (parametro.getParametro().equals(
		ParametrosType.PERMITE_ESPACIOS_BLANCOS)
		&& parametro.getValor().equalsIgnoreCase(noPermite)) {
	    PasswordUtils.validarEspaciosEnBlanco(contrasena.getPassword());
	}

	// validar largo minimo de contrasena
	if (parametro.getParametro().equals(ParametrosType.LARGO_MINIMO)) {
	    PasswordUtils.validarLargoMinimo(contrasena.getPassword(),
		    Integer.parseInt(parametro.getValor()));
	}

	// validar largo max de contrasena
	if (parametro.getParametro().equals(ParametrosType.LARGO_MAXIMO)) {
	    PasswordUtils.validarLargoMaximo(contrasena.getPassword(),
		    Integer.parseInt(parametro.getValor()));
	}

	// 4- Validar tipo de clave
	if (parametro.getParametro().equals(ParametrosType.TIPO_CLAVE)) {
	    PasswordUtils.validarTipo(contrasena.getPassword(),
		    parametro.getValor());
	}

	// 5- Validar caracteres especiales
	if (parametro.getParametro().equals(
		ParametrosType.OBLIGA_CARACT_ESPECIALES)
		&& parametro.getValor().equalsIgnoreCase(noPermite)) {
	    PasswordUtils.validarCaracteresEspeciales(contrasena.getPassword());
	}
	

	// 8- Permite Caracteres consecutivos
	if (parametro.getParametro().equals(
		ParametrosType.NRO_CARACTERES_CONSECUTIVOS)) {
	    PasswordUtils.validarCaracteresConsecutivos(
		    contrasena.getPassword(),
		    Integer.valueOf(parametro.getValor()));
	}

	// 9- Permite Caracteres iguales seguidos
	if (parametro.getParametro().equals(
		ParametrosType.NRO_CARACT_IGUALES_CONSECUTIVOS)) {
	    PasswordUtils.validarCaracteresRepetidosConsecutivos(
		    contrasena.getPassword(),
		    Integer.valueOf(parametro.getValor()));
	    }

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param listaPoliticasActivas2
     * @return
     * @since 1.X
     */
    private Parametros buscarTipoContrasena() {

	for (Parametros politica : listaPoliticasActivas) {
	    if (ParametrosType.TIPO_CLAVE.equals(politica.getParametro())) {
		return politica;
	    }
	}
	return null;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Consultar a Bd de datos la cantidad de veces que la contrasena ha sido
     * utilizada
     * 
     * @param contrasena
     * @throws UserServiceException
     * @since 1.X
     */
    private void validarNroRechazoRehuso(Contrasena contrasenaOriginal,
	    Integer cantidadAnteriores, Long sidUsuario)
	    throws UsuarioServiceException {

	logger.debug("cantidadAnteriores :" + cantidadAnteriores);
	logger.debug("sidUsuario :" + sidUsuario);

	List<Contrasena> listaUltimasContrasenas = contrasenaByUsuarioDAO
		.obtenerContrasenasAnterioresPorUsuario(null, sidUsuario,
			cantidadAnteriores);

	logger.debug("listaUltimasContrasenas +" + listaUltimasContrasenas);

	PasswordUtils.validarNroRechazoRehuso(listaUltimasContrasenas,
		cantidadAnteriores, contrasenaOriginal);
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que comprara si la fecha de hoy es mayor a la de vencimiento de la
     * contrasena para validar que no haya expirado
     * 
     * @param contrasena
     * @param valor
     * @return
     * @since 1.X
     */
    public Boolean validarVigenciaContrasena(Date fechaVencimiento) {
	Date hoy = Calendar.getInstance().getTime();

	if (fechaVencimiento.after(hoy)) {
	    return Boolean.FALSE;
	} else {
	    return Boolean.TRUE;
	}

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param politicaDAO
     * @see com.acl.service.logic.IContrasena#setPoliticaDAO(com.acl.dao.ParametrosDAO)
     * @since 1.X
     */
    @Override
    public void setPoliticaDAO(ParametrosDAO politicaDAO) {
	this.politicaDAO = politicaDAO;

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param contrasenaByUsuarioDAO
     * @see com.acl.service.logic.IContrasena#setContrasenaByUsuarioDAO(com.acl.dao.ContrasenaByUsuarioDAO)
     * @since 1.X
     */
    @Override
    public void setContrasenaByUsuarioDAO(
	    ContrasenaByUsuarioDAO contrasenaByUsuarioDAO) {
	this.contrasenaByUsuarioDAO = contrasenaByUsuarioDAO;

    }

}
