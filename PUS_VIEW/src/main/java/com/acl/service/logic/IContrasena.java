/**
 * 
 */
package com.acl.service.logic;

import com.acl.dao.ContrasenaByUsuarioDAO;
import com.acl.dao.ParametrosDAO;
import com.acl.model.Usuario;
import com.acl.model.exception.UsuarioServiceException;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public interface IContrasena {

    public void validarContrasena(Usuario usuario)
	    throws UsuarioServiceException;

    public void setPoliticaDAO(ParametrosDAO politicaDAO);

    public void setContrasenaByUsuarioDAO(
	    ContrasenaByUsuarioDAO contrasenaByUsuarioDAO);
}
