package com.acl.model.types;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public enum EstadoPreguntaType {

    TODOS("TODOS", -1), INACTIVO("INACTIVO", 0), ACTIVO("ACTIVO", 1);

    EstadoPreguntaType(String label, Integer value) {
	this.label = label;
	this.value = value;
    }

    private final String label;
    private final Integer value;

    public String getLabel() {
	return label;
    }

    public int getPosition() {
	return ordinal();
    }

    public int getValue() {
	return value;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param ordinal
     * @return
     * @since 1.X
     */
    public static EstadoPreguntaType fromOrdinal(int ordinal) {

	EstadoPreguntaType response = null;
	for (EstadoPreguntaType type : EstadoPreguntaType.values()) {
	    if (type.ordinal() == ordinal)
		response = type;
	}
	return response;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param value
     * @return
     * @since 1.X
     */
    public static EstadoPreguntaType fromValue(int value) {

	EstadoPreguntaType response = null;
	for (EstadoPreguntaType type : EstadoPreguntaType.values()) {
	    if (type.value == value) {
		response = type;
		break;
	    }
	}
	return response;
    }

}