package com.acl.model.types;


/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public enum ParametrosType{
    
    OBLIGA_LEADING ("LEADING","Leading","inputSwitch",null,null),
    OBLIGA_TRAILING  ("TRAILING","Trailing","inputSwitch",null,null),
    LARGO_MINIMO  ("LARGO_MINIMO","Largo Mínimo","inputText",1,50),
    LARGO_MAXIMO ("LARGO_MAXIMO","Largo Máximo","inputText",1,50), // el valor minimo debe comenzar en el maximo del largo minimo
    TIPO_CLAVE ("TIPO_CLAVE","Tipo de texto","selectOneMenu",null,null),
    OBLIGA_CARACT_ESPECIALES ("OBLIGA_CARACT_ESPECIALES","Caracteres especiales","inputSwitch",null,null),
    VIGENCIA_CONTRASENA ("VIGENCIA_CONTRASENA","Vigencia de contraseña","inputText",1,3650),
    NRO_RECHAZO_RE_USO ("NRO_RECHAZO_RE_USO","Rechaza re uso de contraseña","inputText",1,100),
    NRO_CARACTERES_CONSECUTIVOS ("NRO_CARACT_CONSECUTIVOS","Permite caracteres consecutivos","inputText",1,50),
    NRO_CARACT_IGUALES_CONSECUTIVOS ("NRO_CARACT_IGUALES_CONSECUTIVOS","Permite caracteres iguales seguidos","inputText",1,50),
    SENSIBILIDAD_MAYUSCULAS  ("SENSIBILIDAD_MAYUSCULAS","Sensibilidad a mayúscula","inputSwitch",null,null),
    NRO_DE_MAYUSCULAS_OBLIGATORIAS  ("NRO_DE_MAYUSCULAS_OBLIGATORIAS","Obliga uso de mayúscula","inputText",0,50),
    NRO_REINTENTOS_BLOQUEAR_CUENTA ("NRO_REINTENTOS_BLOQUEAR_CUENTA","Numero de Reintentos ","inputText",1,100),
    PREGUNTAS_SEGURIDAD ("PREGUNTAS_SEGURIDAD","Preguntas de seguridad ante olvido de contraseña","inputSwitch",null,null),
    PERMITE_ESPACIOS_BLANCOS("PERMITE_ESPACIOS_BLANCOS","Permite espacios en Blanco","inputText",0,0),
    DIAS_ALERTA_VENCIMIENTO("DIAS_ALERTA_VENCIMIENTO","Nro de dias de aviso antes del vencimiento de contraseña","inputText",0,4),
    OTHER("OTHER","","inputText",0,100);
    
    private String label;
    private String view;
    private String obje;
    private Integer valorMinimo;
    private Integer valorMaximo;
    
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) -  versión inicial 
     * </ul>
     * <p>
     * 
     * @param label
     * @param view
     * @param obje
     * @param valorMinimo
     * @param valorMaximo
     * @since 1.X
     */
    private ParametrosType(String label, String view, String obje,Integer valorMinimo,Integer valorMaximo) {
	this.label = label;
	this.view = view;
	this.obje = obje;
	this.valorMinimo = valorMinimo;
	this.valorMaximo = valorMaximo;
    }
    
    public String getLabel() {
        return label;
    }
    public String getView() {
        return view;
    }

    public String getObje() {
        return obje;
    }

    public Integer getValorMaximo() {
        return valorMaximo;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setView(String view) {
        this.view = view;
    }

    public void setObje(String obje) {
        this.obje = obje;
    }

    public void setValorMaximo(Integer valorMaximo) {
        this.valorMaximo = valorMaximo;
    }

    public Integer getValorMinimo() {
        return valorMinimo;
    }

    public void setValorMinimo(Integer valorMinimo) {
        this.valorMinimo = valorMinimo;
    }
    
    
    

}