package com.acl.model.types;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public enum ContrasenaType {

    NUMERICA("Numerica"), ALFABETICA("Alfabetica"), ALFANUMERICA("Alfanumerica");

    private final String label;

    private ContrasenaType(String label) {
	this.label = label;
    }

    public String getLabel() {
	return label;
    }

    public int getPosition() {
	return ordinal();
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param ordinal
     * @return
     * @since 1.X
     */
    public static ContrasenaType fromOrdinal(int ordinal) {

	ContrasenaType response = null;
	for (ContrasenaType type : ContrasenaType.values()) {
	    if (type.ordinal() == ordinal)
		response = type;
	}
	return response;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param value
     * @return
     * @since 1.X
     */
    public static ContrasenaType fromLabel(String label) {

	ContrasenaType response = null;
	for (ContrasenaType type : ContrasenaType.values()) {
	    if (type.label.equalsIgnoreCase(label)) {
		response = type;
		break;
	    }
	}
	return response;
    }

}