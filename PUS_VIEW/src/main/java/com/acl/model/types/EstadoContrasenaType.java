package com.acl.model.types;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public enum EstadoContrasenaType {

    INACTIVA("ESTADO INACTIVO", 1), ACTIVA("ESTADO ACTIVO", 2), PENDIENTE_ACTIVACION(
	    "ESTADO PENDIENTE ACTIVACION", 3);

    private final String label;
    private final Integer sid;

    private EstadoContrasenaType(String label, Integer sid) {
	this.label = label;
	this.sid = sid;

    }

    public String getLabel() {
	return label;
    }

    public Integer getSid() {
	return sid;
    }

    public int getValue() {
	return ordinal();
    }

    public static EstadoContrasenaType fromOrdinal(int ordinal) {

	EstadoContrasenaType response = null;
	for (EstadoContrasenaType type : EstadoContrasenaType.values()) {
	    if (type.ordinal() == ordinal)
		response = type;
	}
	return response;
    }

}