package com.acl.model;

import java.io.Serializable;

import com.acl.model.types.EstadoCambioType;
import com.acl.model.types.EstadoRolType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class Rol implements Serializable, Comparable<Rol> {

    /**
     * 
     */
    private static final long serialVersionUID = 2771603065551264772L;
    private Long sid;
    private String nombre;
    private String descripcion;
    private Log log = new Log();
    private EstadoRolType estado;
    private EstadoCambioType estadoCambio;

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

    public Log getLog() {
	return log;
    }

    public void setLog(Log log) {
	this.log = log;
    }

    public EstadoRolType getEstado() {
	return estado;
    }

    public void setEstado(EstadoRolType estado) {
	this.estado = estado;
    }

    public EstadoCambioType getEstadoCambio() {
	return estadoCambio;
    }

    public void setEstadoCambio(EstadoCambioType estadoCambio) {
	this.estadoCambio = estadoCambio;
    }

    @Override
    public String toString() {
	return "Rol [sid=" + sid + ", nombre=" + nombre + ", estado=" + estado
		+ ", estadoCambio=" + estadoCambio + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((sid == null) ? 0 : sid.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Rol other = (Rol) obj;
	if (sid == null) {
	    if (other.sid != null)
		return false;
	} else if (!sid.equals(other.sid))
	    return false;
	return true;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param o
     * @return
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     * @since 1.X
     */
    @Override
    public int compareTo(Rol o) {
	return this.getSid().compareTo(o.getSid());
    }

}