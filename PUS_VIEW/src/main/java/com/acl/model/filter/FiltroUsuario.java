/**
 * 
 */
package com.acl.model.filter;

import java.io.Serializable;

import com.acl.model.types.EstadoUsuarioType;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class FiltroUsuario implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Integer rut;
    private String dv;
    private EstadoUsuarioType estado;

    public FiltroUsuario() {
	super();
    }

    public FiltroUsuario(Integer rut, String dv) {
	super();
	this.rut = rut;
	this.dv = dv;
    }

    public FiltroUsuario(Integer rut, String dv, EstadoUsuarioType estado) {
	super();
	this.rut = rut;
	this.dv = dv;
	this.estado = estado;
    }

    public Integer getRut() {
	return rut;
    }

    public void setRut(Integer rut) {
	this.rut = rut;
    }

    public String getDv() {
	return dv;
    }

    public void setDv(String dv) {
	this.dv = dv;
    }

    public EstadoUsuarioType getEstado() {
	return estado;
    }

    public void setEstado(EstadoUsuarioType estado) {
	this.estado = estado;
    }

    @Override
    public String toString() {
	return "FiltroUsuario [rut=" + rut + ", dv=" + dv + ", estado="
		+ estado + "]";
    }

}
