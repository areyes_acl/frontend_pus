/**
 * 
 */
package com.acl.model.filter;

import java.io.Serializable;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class FiltroRoles implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1846112892033109873L;
    private Long sid;
    private String nombreRol;

    public String getNombreRol() {
	return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
	this.nombreRol = nombreRol;
    }

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    @Override
    public String toString() {
	return "FiltroRoles [sid=" + sid + ", nombreRol=" + nombreRol + "]";
    }

}
