/**
 * 
 */
package com.acl.model.filter;

import java.io.Serializable;

import com.acl.model.types.EstadoParametroType;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class FiltroParametro implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2751731454264624591L;
    private String nombre;
    private EstadoParametroType estado;

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public EstadoParametroType getEstado() {
	return estado;
    }

    public void setEstado(EstadoParametroType estado) {
	this.estado = estado;
    }

    public FiltroParametro(EstadoParametroType estado) {
	super();
	this.estado = estado;
    }

    public FiltroParametro(String nombre, EstadoParametroType estado) {
	super();
	this.nombre = nombre;
	this.estado = estado;
    }

    @Override
    public String toString() {
	return "FiltroParametro [nombre=" + nombre + ", estado=" + estado + "]";
    }

}
