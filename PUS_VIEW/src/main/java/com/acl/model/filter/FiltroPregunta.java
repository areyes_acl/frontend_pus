/**
 * 
 */
package com.acl.model.filter;

import java.io.Serializable;

import com.acl.model.types.EstadoPreguntaType;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class FiltroPregunta implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1814709552405851509L;
    private Long sid;
    private String pregunta;
    private EstadoPreguntaType estado;

    public String getPregunta() {
	return pregunta;
    }

    public void setPregunta(String pregunta) {
	this.pregunta = pregunta;
    }

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public EstadoPreguntaType getEstado() {
	return estado;
    }

    public void setEstado(EstadoPreguntaType estado) {
	this.estado = estado;
    }

    @Override
    public String toString() {
	return "FiltroPregunta [sid=" + sid + ", pregunta=" + pregunta
		+ ", estado=" + estado + "]";
    }

}
