package com.acl.model;

import java.io.Serializable;
import java.util.List;

import com.acl.model.types.EstadoUsuarioType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class Usuario implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8109472420457810282L;
    private Long sid;
    private Integer rut;
    private String dv;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String email;
    private String fono;
    private Boolean esSuperUsuario;
    private EstadoUsuarioType estado;	
    private Contrasena contrasena = new Contrasena();
    private List<Rol> listaRoles;
    private List<Menu> listaMenu;
    private Log log;

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public Integer getRut() {
	return rut;
    }

    public void setRut(Integer rut) {
	this.rut = rut;
    }

    public String getDv() {
	return dv;
    }

    public void setDv(String dv) {
	this.dv = dv;
    }

    public String getNombres() {
	return nombres;
    }

    public void setNombres(String nombres) {
	this.nombres = nombres;
    }

    public String getApellidoPaterno() {
	return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
	this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
	return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
	this.apellidoMaterno = apellidoMaterno;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getFono() {
	return fono;
    }

    public void setFono(String fono) {
	this.fono = fono;
    }

    public Boolean getEsSuperUsuario() {
	return esSuperUsuario;
    }

    public void setEsSuperUsuario(Boolean esSuperUsuario) {
	this.esSuperUsuario = esSuperUsuario;
    }

    public EstadoUsuarioType getEstado() {
	return estado;
    }

    public void setEstado(EstadoUsuarioType estado) {
	this.estado = estado;
    }

    public Contrasena getContrasena() {
	return contrasena;
    }

    public void setContrasena(Contrasena contrasena) {
	this.contrasena = contrasena;
    }

    public List<Rol> getListaRoles() {
	return listaRoles;
    }

    public void setListaRoles(List<Rol> listaRoles) {
	this.listaRoles = listaRoles;
    }

    public List<Menu> getListaMenu() {
	return listaMenu;
    }

    public void setListaMenu(List<Menu> listaMenu) {
	this.listaMenu = listaMenu;
    }

    public Log getLog() {
	return log;
    }

    public void setLog(Log log) {
	this.log = log;
    }

    public String getNombreCompleto() {

	String fullName = "";
	if (this != null) {
	    if (this.nombres != null) {
		fullName = this.nombres;
	    }
	    if (this.apellidoPaterno != null) {
		fullName = fullName.concat(" ").concat(this.apellidoPaterno != null ? this.apellidoPaterno : "");
	    }
	    if (this.apellidoPaterno != null) {
		fullName = fullName.concat(" ").concat(this.apellidoMaterno != null ? this.apellidoMaterno : "");
	    }
	}

	return fullName;
    }

    @Override
    public String toString() {
	return "Usuario [sid=" + sid + ", rut=" + rut + ", dv=" + dv
		+ ", nombres=" + nombres + ", apellidoPaterno="
		+ apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno
		+ ", email=" + email + ", fono=" + fono + ", esSuperUsuario="
		+ esSuperUsuario + ", estado=" + estado + ", contrasena="
		+ contrasena + ", listaRoles=" + listaRoles + ", listaMenu="
		+ listaMenu + ", log=" + log + "]";
    }

}