package com.acl.model;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class SubMenu implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Long sid;
    private String nombre;
    private String descripcion;
    private Integer posicion;
    private Menu menu;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param sid
     * @param nombre
     * @param descripcion
     * @param posicion
     * @since 1.X
     */
    public SubMenu(Long sid, String nombre, String descripcion, Integer posicion) {
	super();
	this.sid = sid;
	this.nombre = nombre;
	this.descripcion = descripcion;
	this.posicion = posicion;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    public SubMenu() {
    }

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

    public Integer getPosicion() {
	return posicion;
    }

    public void setPosicion(Integer posicion) {
	this.posicion = posicion;
    }

    @Override
    public String toString() {
	return "Menu [sid=" + sid + ", nombre=" + nombre + ", descripcion="
		+ descripcion + ", posicion=" + posicion + "]";
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

}