package com.acl.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class Log implements Serializable {
    private static final long serialVersionUID = -508822372715344437L;

    private Long sidCreacion;
    private Date fechaCreacion;
    private Long sidModificacion;
    private Date fechaModificacion;

    public Long getSidCreacion() {
	return sidCreacion;
    }

    public void setSidCreacion(Long sidCreacion) {
	this.sidCreacion = sidCreacion;
    }

    public Date getFechaCreacion() {
	return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
	this.fechaCreacion = fechaCreacion;
    }

    public Long getSidModificacion() {
	return sidModificacion;
    }

    public void setSidModificacion(Long sidModificacion) {
	this.sidModificacion = sidModificacion;
    }

    public Date getFechaModificacion() {
	return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
	this.fechaModificacion = fechaModificacion;
    }

    @Override
    public String toString() {
	return "Log [sidCreacion=" + sidCreacion + ", fechaCreacion="
		+ fechaCreacion + ", sidModificacion=" + sidModificacion
		+ ", fechaModificacion=" + fechaModificacion + "]";
    }

}
