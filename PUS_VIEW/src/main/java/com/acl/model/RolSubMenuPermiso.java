/**
 * 
 */
package com.acl.model;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class RolSubMenuPermiso {
    	private Long sid;
	private RolSubMenu rolSubMenu;
	private Permiso permiso;
	private Long estado;
	public Long getSid() {
	    return sid;
	}
	public void setSid(Long sid) {
	    this.sid = sid;
	}
	public RolSubMenu getRolSubMenu() {
	    return rolSubMenu;
	}
	public void setRolSubMenu(RolSubMenu rolSubMenu) {
	    this.rolSubMenu = rolSubMenu;
	}
	public Permiso getPermiso() {
	    return permiso;
	}
	public void setPermiso(Permiso permiso) {
	    this.permiso = permiso;
	}
	public Long getEstado() {
	    return estado;
	}
	public void setEstado(Long estado) {
	    this.estado = estado;
	}
	@Override
	public String toString() {
	    return "RolSubMenuPermiso [sid=" + sid + ", rolSubMenu="
		    + rolSubMenu.getSid() + ", permiso=" + permiso.getSid() + ", estado="
		    + estado + "]";
	}
	
	
	
}
