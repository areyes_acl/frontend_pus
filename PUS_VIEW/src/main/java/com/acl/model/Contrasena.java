package com.acl.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class Contrasena implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6463613780860826686L;

    private Long sid;
    private EstadoContrasena estado = new EstadoContrasena();
    private String password;
    private Date fechaVencimiento;

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public EstadoContrasena getEstado() {
	return estado;
    }

    public void setEstado(EstadoContrasena estado) {
	this.estado = estado;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public Date getFechaVencimiento() {
	return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
	this.fechaVencimiento = fechaVencimiento;
    }

    @Override
    public String toString() {
	return "Contrasena [sid=" + sid + ", estado=" + estado + ", password="
		+ password + ", fechaVencimiento=" + fechaVencimiento + "]";
    }

}