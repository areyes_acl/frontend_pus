package com.acl.model;

import java.io.Serializable;

import com.acl.model.types.EstadoPreguntaType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class Pregunta implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Long sid;
    private String pregunta;
    private Log log = new Log();
    private EstadoPreguntaType estado;

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public String getPregunta() {
	return pregunta;
    }

    public void setPregunta(String pregunta) {
	this.pregunta = pregunta;
    }

    public Log getLog() {
	return log;
    }

    public void setLog(Log log) {
	this.log = log;
    }

    public EstadoPreguntaType getEstado() {
	return estado;
    }

    public void setEstado(EstadoPreguntaType estado) {
	this.estado = estado;
    }

    @Override
    public String toString() {
	return "Pregunta [sid=" + sid + ", pregunta=" + pregunta + ", log="
		+ log + ", estado=" + estado + "]";
    }

}