package com.acl.model;

import java.io.Serializable;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class EstadoContrasena implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4125612073665856418L;
    private Long sid;
    private Boolean estado;
    private String descripcion;

    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public Boolean getEstado() {
	return estado;
    }

    public void setEstado(Boolean estado) {
	this.estado = estado;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

    @Override
    public String toString() {
	return "EstadoContrasena [sid=" + sid + ", estado=" + estado
		+ ", descripcion=" + descripcion + "]";
    }

}