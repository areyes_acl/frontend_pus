/**
 * 
 */
package com.acl.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) -  versión inicial 
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class RolSubMenu {
	private Long sid;
    	private SubMenu submenu= new SubMenu();
    	private Rol rol= new Rol();
    	private List<Permiso> permisoList=new ArrayList();
    	private Long estado;
    	
    	
    	private List<String> permisionsListName;
    	private String[] permisionsListNameSelection;
    	
    	public void addPermiso(Permiso p){
    	    if (permisoList!=null){
    		permisoList.add(p);
    	    }
    	}
    	public void addpermisionsListNameSelection(String z){
    	    ArrayList<String> y = new ArrayList<String>();
    	if (permisionsListNameSelection!=null){
    	    for (String x :permisionsListNameSelection ){
    		y.add(x);
    	    }
    	}
    	    y.add(z);
    	Object[] objectList = y.toArray();
    	String[] stringArray =  Arrays.copyOf(objectList,objectList.length,String[].class);
    	    permisionsListNameSelection=stringArray;
    	}
    	public List<String> getPermisionsListName(){
    	    permisionsListName= new ArrayList();
    	    for (Permiso p :permisoList){
    		permisionsListName.add(p.getPermiso());
    	    }
    	    return permisionsListName;
    	}
    	
	public SubMenu getSubmenu() {
	    return submenu;
	}
	public void setSubmenu(SubMenu submenu) {
	    this.submenu = submenu;
	}
	public Rol getRol() {
	    return rol;
	}
	public void setRol(Rol rol) {
	    this.rol = rol;
	}
	public List<Permiso> getPermisoList() {
	    return permisoList;
	}
	public void setPermisoList(List<Permiso> permisoList) {
	    this.permisoList = permisoList;
	}
	public Long getSid() {
	    return sid;
	}
	public void setSid(Long sid) {
	    this.sid = sid;
	}

	public void setPermisionsListName(List<String> permisionsListName) {
	    this.permisionsListName = permisionsListName;
	}

	public String[] getPermisionsListNameSelection() {
	    return permisionsListNameSelection;
	}

	public void setPermisionsListNameSelection(
		String[] permisionsListNameSelection) {
	    this.permisionsListNameSelection = permisionsListNameSelection;
	}

	public Long getEstado() {
	    return estado;
	}

	public void setEstado(Long estado) {
	    this.estado = estado;
	}

	@Override
	public String toString() {
	    return "RolSubMenu [sid=" + sid + ", submenu=" + submenu.getDescripcion() + ", rol="
		    + rol.getNombre() + ", permisoList=" + permisoList.size() + ", estado="
		    + estado + ", permisionsListName=" + permisionsListName
		    + ", permisionsListNameSelection="
		    + Arrays.toString(permisionsListNameSelection) + "]";
	}
	
}
