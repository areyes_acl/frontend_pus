/**
 * 
 */
package com.acl.model.exception;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class PreguntaServiceException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 2440276286562255352L;
    private String descripcion;

    public PreguntaServiceException() {
    }

    public PreguntaServiceException(String message) {
	super(message);
    }

    public PreguntaServiceException(Throwable cause) {
	super(cause);
    }

    public PreguntaServiceException(String message, Throwable cause) {
	super(message, cause);
    }
    
    public PreguntaServiceException(String message, String descripcion, Throwable cause) {
	super(message, cause);
	this.descripcion = descripcion;
    }

    public PreguntaServiceException(String mensaje, String descripcion) {
	super(mensaje);
	this.descripcion = descripcion;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

}
