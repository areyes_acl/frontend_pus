/**
 * 
 */
package com.acl.model.exception;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ServiceException extends Exception implements Serializable {

    /**
     * serial.
     */
    private static final long serialVersionUID = 1L;
    private String stackTraceDescripcion;

    /**
     * Constructor.
     * 
     * @param message
     *            .
     */
    public ServiceException(final String message) {
	super(message);
    }

    public String getStackTraceDescripcion() {
	return stackTraceDescripcion;
    }

    public void setStackTraceDescripcion(String stackTraceDescripcion) {
	this.stackTraceDescripcion = stackTraceDescripcion;
    }

    /**
     * Contructor.
     * 
     * @param message
     *            .
     * @param cause
     *            .
     */
    public ServiceException(final String message, final Throwable cause) {
	super(message, cause);
	this.stackTraceDescripcion = getStackTrace(cause);

    }

    /**
     * Contructor.
     * 
     * @param message
     *            .
     * @param cause
     *            .
     */
    public ServiceException(final Throwable cause) {
	super(cause);
	this.stackTraceDescripcion = getStackTrace(cause);

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 3/12/2015, (ACL-abcdin) - versión inicial
     * </ul>
     * <p>
     * Metodo que dada una excepcion retorna su traza completa
     * 
     * @param t
     * @return
     * @since 1.X
     */
    public static String getStackTrace(Throwable t) {
	StringWriter sw = new StringWriter();
	t.printStackTrace(new PrintWriter(sw));
	return sw.toString();
    }
}
