/**
 * 
 */
package com.acl.model.exception;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class UsuarioServiceException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 2440276286562255352L;

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Constructor de la clase
     * 
     * @since 1.X
     */
    public UsuarioServiceException() {
	// No contiene nada por ser un constructor de la excepcio
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param message
     * @since 1.X
     */
    public UsuarioServiceException(String message) {
	super(message);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param cause
     * @since 1.X
     */
    public UsuarioServiceException(Throwable cause) {
	super(cause);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param message
     * @param cause
     * @since 1.X
     */
    public UsuarioServiceException(String message, Throwable cause) {
	super(message, cause);
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param mensaje
     * @param descripcion
     * @since 1.X
     */
    public UsuarioServiceException(String mensaje, String descripcion) {
	super(mensaje);
    }

}
