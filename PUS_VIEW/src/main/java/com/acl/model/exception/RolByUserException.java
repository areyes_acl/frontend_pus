/**
 * 
 */
package com.acl.model.exception;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class RolByUserException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 2440276286562255352L;
    private String descripcion;

    public RolByUserException() {
    }

    public RolByUserException(String message) {
	super(message);
    }

    public RolByUserException(Throwable cause) {
	super(cause);
    }

    public RolByUserException(String message, Throwable cause) {
	super(message, cause);
    }

    public RolByUserException(String mensaje, String descripcion) {
	super(mensaje);
	this.descripcion = descripcion;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

}
