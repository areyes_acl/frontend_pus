package com.acl.model;

import java.io.Serializable;

import com.acl.model.types.EstadoParametroType;
import com.acl.model.types.ParametrosType;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class Parametros implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private Long sid;
    private String nombre;
    private String valor;
    private Log log = new Log();
    private EstadoParametroType estado;
    private ParametrosType parametro;
    private boolean changedValor=false;
    private String newValor;
    private boolean testValidatorFail=false; 
	    
    public Long getSid() {
	return sid;
    }

    public void setSid(Long sid) {
	this.sid = sid;
    }

    public String getNombre() {
	
	return nombre;
    }

    public void setNombre(String nombre) {
	boolean st=false;
	for (ParametrosType x:ParametrosType.values()){
	    if (x.name().equals(nombre)){
		parametro=x;
		st=true;
	    }
	}
	if (!st) {
	    parametro=ParametrosType.OTHER;
	    parametro.setView(nombre);
	}
	this.nombre = nombre;
    }

    public String getValor() {
	return valor;
    }

    public void setValor(String valor) {
	this.valor = valor;
    }

    public Log getLog() {
	return log;
    }

    public void setLog(Log log) {
	this.log = log;
    }

    public EstadoParametroType getEstado() {
	return estado;
    }

    public void setEstado(EstadoParametroType estado) {
	this.estado = estado;
    }

    public ParametrosType getParametro() {
	return parametro;
    }

    public void setParametro(ParametrosType parametro) {
	this.parametro = parametro;
    }

  

    @Override
    public String toString() {
	return "Parametros [sid=" + sid + ", nombre=" + nombre + ", valor="
		+ valor + ", log=" + log + ", estado=" + estado
		+ ", parametro=" + parametro + ", changedValor=" + changedValor
		+ ", newValor=" + newValor + "]";
    }

    public boolean isChangedValor() {
        return changedValor;
    }
  
    public void changedValor() {
	String[] misValores={"0","1","false","true","inputText","selectOneMenu","OTHER","inputSwitch"};
		if (this.parametro.getObje().equals(misValores[7])){
	      	if (this.newValor.equals(misValores[1]) && this.valor.equals(misValores[3])  ){
        	    
        	    this.changedValor=false;
        	}else if (this.newValor.equals(misValores[0]) && this.valor.equals(misValores[2]) ){
        	    
        	    this.changedValor=false;
        	}else if (this.newValor.equals(misValores[1]) && this.valor.equals(misValores[2]) ){
        	   
        	    this.changedValor=true;
        	}else if (this.newValor.equals(misValores[0]) && this.valor.equals("true") ){
        	   
        	    this.changedValor=true;
        	}else if (this.newValor.equals(misValores[0]) && this.valor.equals(misValores[0])){
        	    this.changedValor=false;
        	}else if (this.newValor.equals(misValores[1]) && this.valor.equals(misValores[1])){
        	    this.changedValor=false;
        	}else if (this.newValor.equals(misValores[0]) && this.valor.equals(misValores[1])){
        	    this.changedValor=true;
        	}
	}
	if ( this.parametro.getObje().equals(misValores[4]) || this.parametro.getObje().equals(misValores[5]) || this.parametro.getObje().equals(misValores[6])){
        	if (this.valor.equals(newValor)  ){
        	   
        	    this.changedValor=false;
        	}else{
        	   
        	    this.changedValor=true;
        	}
	}
	
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getNewValor() {
        return newValor;
    }

    public void setNewValor(String newValor) {
        this.newValor = newValor;
    }

    public boolean isTestValidatorFail() {
        return testValidatorFail;
    }

    public void setTestValidatorFail(boolean testValidatorFail) {
        this.testValidatorFail = testValidatorFail;
    }

    public void setChangedValor(boolean changedValor) {
        this.changedValor = changedValor;
    }

}