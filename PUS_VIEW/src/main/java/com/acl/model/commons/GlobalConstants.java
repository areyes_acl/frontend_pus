/**
 * 
 */
package com.acl.model.commons;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class GlobalConstants {

    public static final String APLICATION_NAME = "POLITICAS-USUARIO";
    public static final String ERROR = "ERROR";
    public static final String EMPTY = "";
    public static final String BLANK = " ";

    private GlobalConstants() {
    }

}
