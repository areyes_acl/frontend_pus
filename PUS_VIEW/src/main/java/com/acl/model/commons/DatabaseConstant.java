/**
 * 
 */
package com.acl.model.commons;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class DatabaseConstant {

    // ESQUEMA
    public static final String ESQUEMA = "SIG";

    // TABLAS
    public static final String TABLA_USUARIO = "TBL_USUARIO_IG";
    public static final String TABLA_PREGUNTA = "TBL_PREGUNTAS";
    public static final String TABLA_ROL = "TBL_ROL";
    public static final String TABLA_POLITICAS = "TBL_POLITICAS_USUARIO_PRTS";
    public static final String TABLA_ROL_USUARIO = "TBL_ROL_USUARIO";
    public static final String TABLA_MENU = "TBL_MENU";

    // SECUENCIAS
    public static final String SECUENCIA_TBL_PREGUNTAS = "SEQ_TBL_PREGUNTAS";
    public static final String SECUENCIA_TBL_ROL_USUARIO = "SEQ_TBL_ROL_USUARIO";

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private DatabaseConstant() {
    }

}
