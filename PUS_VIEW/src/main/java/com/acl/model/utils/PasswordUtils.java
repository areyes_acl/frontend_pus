/**
 * 
 */
package com.acl.model.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.acl.model.Contrasena;
import com.acl.model.exception.UsuarioServiceException;
import com.acl.model.types.ContrasenaType;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class PasswordUtils {
    /**
     * Constante de logger
     */
    private static final Logger logger = Logger.getLogger(PasswordUtils.class);
    private static final String PATTERN_CARACTERES_ESPECIALES = "[^A-Za-z0-9 ]";
    private static final String CONTAINS_ALMOST_ONE_LETTER = ".*[áéíóúña-zÑÁÉÍÓÚA-Z]+.*";
    private static final String CONTAINS_ALMOST_ONE_NUMBER = ".*\\d.*";
    private static final String MAYUSCULAS_EXCEPTION = "La contraseña ingresada no cumple con la cantidad de mayúsculas obligatorias (:max) indicadas en las politicas de contraseñas de usuarios.";
    private static final String MAX_CONSECUTIVAS_EXCEPTION = "La contraseña ingresada no puede tener mas de (:max) caracteres consecutivos de acuerdo a las politicas de contraseñas de usuarios.";
    private static final String MAX_REPETIDAS_CONSECUTIVAS_EXCEPTION = "La contraseña ingresada sobrepasa la cantidad máxima (:max) de caracteres repetidos consecutivamente indicadas en las politicas de contraseñas de usuarios.";
    private static final String MIN_LARGO_EXCEPTION = "La contraseña ingresada no cumple el largo minimo (:min) exigido en las politicas de contraseñas de usuarios.";
    private static final String MAX_LARGO_EXCEPTION = "La contraseña ingresada sobrepasa el largo maximo (:max) exigido en las politicas de contraseñas de usuarios.";
    private static final String LEADING_EXCEPTION = "La contraseña no debe comenzar con números.";
    private static final String TRAILING_EXCEPTION = "La contraseña no debe terminar con números.";
    private static final String ALFANUMERICA_EXCEPTION = "La contraseña debe contener al menos una letra y un numero de acuerdo a lo que exige en las politicas de contraseñas de usuarios.";
    private static final String ALFABETICA_EXCEPTION = "La contraseña debe contener solo letras de acuerdo a lo que exige en las politicas de contraseñas de usuarios.";
    private static final String NUMERICA_EXCEPTION = "La contraseña debe contener solo numeros de acuerdo a lo que se exige en las politicas de contraseñas de usuarios.";
    private static final String MAXIMO_USO_EXCEPTION = "La contraseña ingresada no puede ser igual a alguna de las ultimas (:max) contraseñas utilizadas.";
    private static final String WHITESPACES_EXCEPTION = "La contraseña no debe contener espacios en blanco.";
    private static final String CARACTERES_ESPECIALES_EXCEPTION = "La contraseña ingresada no debe contener caracteres especiales.";
    private static final String RUT_IGUAL_CONTRASENA_EXCEPTION = "La contraseña ingresada no debe ser igual al rut registrado para el usuario.";

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private PasswordUtils() {

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param args
     * @throws NoSuchAlgorithmException
     * @throws Exception
     * @since 1.X
     */
    public static String convertirPasswordSha256(String contrasena) {

	MessageDigest md;
	try {
	    md = MessageDigest.getInstance("SHA-256");

	    StringBuilder hexString = new StringBuilder();
	    md.update(contrasena.getBytes());

	    byte[] byteData = md.digest();

	    // convert the byte to hex format method 2

	    for (int i = 0; i < byteData.length; i++) {
		String hex = Integer.toHexString(0xff & byteData[i]);
		if (hex.length() == 1)
		    hexString.append('0');
		hexString.append(hex);
	    }
	    return hexString.toString();
	} catch (NoSuchAlgorithmException e) {
	    logger.info(e.getMessage(), e);
	}
	return null;
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Si esta activado debe permitir caracteres especiales. Si no esta activado
     * no debe permitir el ingreso de caracteres especiales.
     * 
     * @param contrasena
     * @return
     * @throws UsuarioServiceException
     * @since 1.X
     */
    public static void validarCaracteresEspeciales(String contrasena)
	    throws UsuarioServiceException {

	Pattern pattern = Pattern.compile(PATTERN_CARACTERES_ESPECIALES);
	Matcher matcher = pattern.matcher(contrasena);

	if (matcher.find()) {
	    throw new UsuarioServiceException(CARACTERES_ESPECIALES_EXCEPTION);
	}
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 1: Numèrica 2: alfabetica 3: alfanumerica
     * 
     * @param contrasena
     * @throws UsuarioServiceException
     * @since 1.X
     */
    public static void validarTipo(String contrasena, String tipo)
	    throws UsuarioServiceException {
	Boolean retorno;

	logger.debug("tipo:" + tipo);

	// SE REEMPLAZAN LOS CARACTERES ESPECIALES PARA QUE NO ARROJE EXCEPCION
	// SI ES ALFANUMERICO
	String contrasenaAux = eliminarCaracteresEspeciales(contrasena);

	if (ContrasenaType.NUMERICA.getLabel().contentEquals(tipo)) {

	    retorno = org.apache.commons.lang.math.NumberUtils
		    .isNumber(contrasenaAux);

	    if (!retorno) {
		throw new UsuarioServiceException(NUMERICA_EXCEPTION);
	    }

	} else if (ContrasenaType.ALFABETICA.getLabel().contentEquals(tipo)) {
	    retorno = org.apache.commons.lang.StringUtils
		    .isAlpha(contrasenaAux);
	    logger.debug("retorno:" + retorno);
	    if (!retorno) {
		throw new UsuarioServiceException(ALFABETICA_EXCEPTION);
	    }
	} else if (ContrasenaType.ALFANUMERICA.getLabel().contentEquals(tipo)) {

	    retorno = isAlphaNumeric(contrasenaAux);
	    if (!retorno) {
		throw new UsuarioServiceException(ALFANUMERICA_EXCEPTION);
	    }
	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Contiene almenos un caracter y un numero segun definicion de abcdin
     * 
     * @param s
     * @return
     * @since 1.X
     */
    public static boolean isAlphaNumeric(String s) {

	return s.matches(CONTAINS_ALMOST_ONE_LETTER)
		&& s.matches(CONTAINS_ALMOST_ONE_NUMBER);
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Si esta politica esta activa se permite que termine con numeros la
     * contraseña Si esta desactivada no se permitirà que termine con numeros.
     * 
     * @param contrasena
     * @throws UsuarioServiceException
     * @since 1.X
     */
    public static void validarTrailing(String contrasena)
	    throws UsuarioServiceException {
	if (contrasena != null
		&& Character
			.isDigit(contrasena.charAt(contrasena.length() - 1))) {
	    throw new UsuarioServiceException(TRAILING_EXCEPTION);
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Si esta politica se encuentra activa se pemite que comience con numero Si
     * esta desactivada no se permite que comience con numeros
     * 
     * @param contrasena
     * @throws UsuarioServiceException
     * @since 1.X
     */
    public static void validarLeading(String contrasena)
	    throws UsuarioServiceException {

	if (contrasena != null && Character.isDigit(contrasena.charAt(0))) {
	    throw new UsuarioServiceException(LEADING_EXCEPTION);

	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param contrasena
     * @param maximo
     * @throws UsuarioServiceException
     * @since 1.X
     */
    public static void validarLargoMaximo(String contrasena, Integer maximo)
	    throws UsuarioServiceException {

	if (contrasena.length() > maximo) {
	    throw new UsuarioServiceException(MAX_LARGO_EXCEPTION.replace(
		    ":max", String.valueOf(maximo)));
	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param contrasena
     * @param minimo
     * @throws UsuarioServiceException
     * @since 1.X
     */
    public static void validarLargoMinimo(String contrasena, Integer minimo)
	    throws UsuarioServiceException {
	if (contrasena.length() < minimo) {
	    throw new UsuarioServiceException(MIN_LARGO_EXCEPTION.replace(
		    ":min", String.valueOf(minimo)));

	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que valida caracteres consecutivos dada una cantidad maxima. Si
     * sobrepasa la cantidad maxima se arroja una exepcion
     * 
     * 
     * 
     * @param cadena
     * @param cantMaxConsecutivo
     * @return
     * @throws UsuarioServiceException
     * @since 1.Xs
     */
    public static void validarCaracteresConsecutivos(String cadena,
	    Integer cantMaxConsecutivo) throws UsuarioServiceException {
	char[] charArray = cadena.toCharArray();
	int asciiCode;
	boolean isConSeq = false;
	int previousAsciiCode = (charArray != null && charArray.length > 0 && charArray[0] == '0') ? (int) charArray[0] - 1
		: 0;
	int numSeqcount = 0;

	if (charArray != null) {
	    for (int i = 0; i < charArray.length; i++) {
		asciiCode = (int) charArray[i];
		if ((previousAsciiCode + 1) == asciiCode) {
		    numSeqcount++;
		    if (numSeqcount >= cantMaxConsecutivo + 1) {
			isConSeq = true;
			break;
		    }
		} else {
		    numSeqcount = 0;
		}
		previousAsciiCode = asciiCode;
	    }
	}

	if (isConSeq) {
	    throw new UsuarioServiceException(
		    MAX_CONSECUTIVAS_EXCEPTION.replace(":max",
			    String.valueOf(cantMaxConsecutivo)));
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que valida si una cadena contiene un caracter repetido
     * consecutivamente mas de la cantidad permitida
     * 
     * @param cadena
     * @param cantMaxRepetidos
     * @throws UsuarioServiceException
     * @since 1.X
     */
    public static void validarCaracteresRepetidosConsecutivos(String cadena,
	    Integer cantMaxRepetidos) throws UsuarioServiceException {
	char[] charArray = cadena.toCharArray();
	int asciiCode;
	boolean isConSeq = false;
	int previousAsciiCode = (charArray != null && charArray.length > 0 && charArray[0] == '0') ? (int) charArray[0]
		: 0;
	int numSeqcount = 0;

	if (charArray != null) {
	    for (int i = 0; i < charArray.length; i++) {
		asciiCode = (int) charArray[i];

		if ((previousAsciiCode) == asciiCode) {
		    numSeqcount++;
		    if (numSeqcount >= cantMaxRepetidos + 1) {
			isConSeq = true;
			break;
		    }
		} else {
		    numSeqcount = 0;
		}
		previousAsciiCode = asciiCode;
	    }

	    if (isConSeq) {
		throw new UsuarioServiceException(
			MAX_REPETIDAS_CONSECUTIVAS_EXCEPTION.replace(":max",
				String.valueOf(cantMaxRepetidos)));
	    }

	}
    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Metodo que valida que una cadena dada posea una cantidad de mayusculas.
     * Si no se cumple con esto, se lanza una execpion personalizada
     * 
     * @param password
     * @throws UsuarioServiceException
     * @since 1.X
     */
    public static void validaObligatoriedadMayuscula(String contrasena,
	    Integer cantidadMayusculasObligatorias)
	    throws UsuarioServiceException {

	int countMayusculas = 0;

	for (int i = 0; i < contrasena.length(); i++) {
	    if (Character.isUpperCase(contrasena.charAt(i))) {
		countMayusculas++;
		if (countMayusculas >= cantidadMayusculasObligatorias) {
		    break;
		}
	    }
	}

	if (countMayusculas < cantidadMayusculasObligatorias) {
	    throw new UsuarioServiceException(MAYUSCULAS_EXCEPTION.replace(
		    ":max", String.valueOf(cantidadMayusculasObligatorias)));
	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @throws UsuarioServiceException
     * 
     * @since 1.X
     */
    public static void validarNroRechazoRehuso(
	    List<Contrasena> contrasenasHistoricas, Integer cantidadAnteriores,
	    Contrasena nueva) throws UsuarioServiceException {

	String newPasswordEncript = convertirPasswordSha256(nueva.getPassword());

	for (Contrasena contrasenaHistorica : contrasenasHistoricas) {
	    logger.info("Comparacion Pass : "
		    + contrasenaHistorica.getPassword() + "-"
		    + newPasswordEncript);
	    logger.info(contrasenaHistorica.getPassword().equalsIgnoreCase(
		    newPasswordEncript));

	    if (contrasenaHistorica.getPassword().equalsIgnoreCase(
		    newPasswordEncript)) {
		throw new UsuarioServiceException(MAXIMO_USO_EXCEPTION.replace(
			":max", String.valueOf(cantidadAnteriores)));
	    }

	}

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param contrasena
     * @throws UsuarioServiceException
     * @since 1.X
     */
    public static void validarEspaciosEnBlanco(String contrasena)
	    throws UsuarioServiceException {
	Pattern pattern = Pattern.compile("\\s");
	Matcher matcher = pattern.matcher(contrasena);

	if (matcher.find()) {
	    throw new UsuarioServiceException(WHITESPACES_EXCEPTION);
	}
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @since 1.X
     */
    private static String eliminarCaracteresEspeciales(String input) {
	return input.replaceAll("[^a-zA-Z0-9]+", "");

    }

    /**
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2017, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param rut
     * @param dv
     * @throws UsuarioServiceException
     * @since 1.X
     */
    public static void validarContrasenaIgualARut(Integer rut, String dv,
	    Contrasena contrasena) throws UsuarioServiceException {
	String contrasenaRut = contrasena.getPassword().replace(".", "")
		.replace("-", "");
	String rutCompuesto = String.valueOf(rut).concat(dv);

	if (contrasenaRut.equalsIgnoreCase(rutCompuesto)) {
	    throw new UsuarioServiceException(RUT_IGUAL_CONTRASENA_EXCEPTION);
	}

    }
}
