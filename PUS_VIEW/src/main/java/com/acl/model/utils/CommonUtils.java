/**
 * 
 */
package com.acl.model.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;

import com.acl.model.Log;
import com.acl.model.Usuario;
import com.acl.model.commons.GlobalConstants;
import com.acl.model.types.IType;

/**
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class CommonUtils {
    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param p_type
     * @return
     * @since 1.X
     */
    public static List<IType> getGenericType(String p_type) {
	List<IType> lstType = new ArrayList<IType>();

	try {
	    @SuppressWarnings({ "unchecked" })
	    Class<Enum> enumType = (Class<Enum>) Class.forName(p_type);
	    for (Enum e : enumType.getEnumConstants()) {
		lstType.add((IType) e);
	    }

	} catch (ClassNotFoundException e) {
	    lstType = new ArrayList<IType>();
	    e.printStackTrace();

	}

	return lstType;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param pSiduser
     * @return
     * @since 1.X
     */
    public static Log createLog() {
	Usuario user = ((Usuario) FacesContext.getCurrentInstance()
		.getExternalContext().getSessionMap().get("usuario"));
	Log log = new Log();
	Date logDate = new Date();
	log.setFechaCreacion(logDate);
	log.setFechaModificacion(logDate);
	log.setSidCreacion(user.getSid());
	log.setSidModificacion(user.getSid());
	return log;

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * 
     * @param aValue
     * @return
     * @since 1.X
     */
    public static Boolean isEmpty(Object aValue) {
	if (aValue == null) {
	    return true;
	}

	return GlobalConstants.EMPTY.equals(aValue.toString().trim());

    }

}
