package com.acl.model.utils;

import org.apache.log4j.Logger;

/**
 * 
 * 
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>Todos los derechos reservados por ACL SPA .</B>
 */
public class ValidationUtils {
    private static Logger logger = Logger.getLogger(ValidationUtils.class);

    private ValidationUtils() {

    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Funcion que evalua si un rut es valido o no
     * 
     * @param rut
     * @return
     * @since 1.X
     */
    public static boolean validarRut(String rut) {
	String rutAuxiliar = rut;
	boolean validacion = false;
	try {
	    rutAuxiliar = rutAuxiliar.toUpperCase();
	    rutAuxiliar = rutAuxiliar.replace(".", "");
	    rutAuxiliar = rutAuxiliar.replace("-", "");
	    int rutAux = Integer.parseInt(rutAuxiliar.substring(0, rutAuxiliar.length() - 1));

	    char dv = rutAuxiliar.charAt(rutAuxiliar.length() - 1);

	    int m = 0;
	    int s = 1;
	    for (; rutAux != 0; rutAux /= 10) {
		s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
	    }
	    if (dv == (char) (s != 0 ? s + 47 : 75)) {
		validacion = true;
	    }

	} catch (java.lang.NumberFormatException e) {
	    logger.error(e.getMessage(), e);
	}
	return validacion;
    }

    /**
     * 
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 XX/YY/2016, (ACL SPA) - versión inicial
     * </ul>
     * <p>
     * Funcion que valida si un rut es valido o no
     * 
     * @param rut
     * @param dv
     * @return
     * @since 1.X
     */
    public static boolean validarRut(String rut, String dv) {
	String rutAuxiliar = rut.concat(dv);
	boolean validacion = false;
	try {
	    rutAuxiliar = rutAuxiliar.toUpperCase();
	    rutAuxiliar = rutAuxiliar.replace(".", "");
	    rutAuxiliar = rutAuxiliar.replace("-", "");
	    int rutAux = Integer.parseInt(rutAuxiliar.substring(0, rutAuxiliar.length() - 1));
	    char dvAux = rutAuxiliar.charAt(rutAuxiliar.length() - 1);
	    int m = 0;
	    int s = 1;
	    for (; rutAux != 0; rutAux /= 10) {
		s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
	    }
	    if (dvAux == (char) (s != 0 ? s + 47 : 75)) {
		validacion = true;
	    }

	} catch (java.lang.NumberFormatException e) {
	    logger.error(e.getMessage(), e);
	}
	return validacion;
    }

}